#include <iostream>
#include "../NorrServer/NorrServer.h"

int main()
{
   RPC_STATUS status;
   RPC_WSTR szStringBinding = NULL;

	// Create a string binding handle.
	status = RpcStringBindingCompose(
		NULL,										// UUID to bind to.
		reinterpret_cast<RPC_WSTR>(L"ncacn_ip_tcp"),// Use TCP/IP protocol.
		reinterpret_cast<RPC_WSTR>(L"localhost"),	// TCP/IP network address to use.
		reinterpret_cast<RPC_WSTR>(L"4747"),		// TCP/IP port to use.
		NULL,										// Protocol dependent network options to use.
		&szStringBinding);							// String binding output.
	if (status) exit(status);

	// Validate the format of the string binding handle and convert it to a binding handle.
	status = RpcBindingFromStringBinding(szStringBinding, &hServerBinding);
	if (status) exit(status);

	RpcTryExcept
	{
		Output("Hello Implicit RPC World!");
	}
	RpcExcept(1)
	{
		std::cerr << "Runtime reported exception " << RpcExceptionCode() << std::endl;
	}
	RpcEndExcept

	status = RpcStringFree(&szStringBinding);
	if (status) exit(status);

	// Release binding handle resources and disconnect from the server.
	status = RpcBindingFree(&hServerBinding);
	if (status) exit(status);
}

// Memory allocation function for RPC.
void* __RPC_USER midl_user_allocate(size_t size)
{
    return malloc(size);
}

// Memory deallocation function for RPC.
void __RPC_USER midl_user_free(void* p)
{
    free(p);
}
