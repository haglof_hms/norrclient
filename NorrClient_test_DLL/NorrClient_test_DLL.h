
// NorrClient_test_DLL.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CNorrClient_test_DLLApp:
// See NorrClient_test_DLL.cpp for the implementation of this class
//

class CNorrClient_test_DLLApp : public CWinAppEx
{
public:
	CNorrClient_test_DLLApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CNorrClient_test_DLLApp theApp;