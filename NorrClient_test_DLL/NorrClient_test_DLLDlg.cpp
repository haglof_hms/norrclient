
// NorrClient_test_DLLDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NorrClient_test_DLL.h"
#include "NorrClient_test_DLLDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CNorrClient_test_DLLDlg dialog




CNorrClient_test_DLLDlg::CNorrClient_test_DLLDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNorrClient_test_DLLDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CNorrClient_test_DLLDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CNorrClient_test_DLLDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CNorrClient_test_DLLDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON3, &CNorrClient_test_DLLDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON2, &CNorrClient_test_DLLDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON4, &CNorrClient_test_DLLDlg::OnBnClickedButton4)
END_MESSAGE_MAP()


// CNorrClient_test_DLLDlg message handlers

BOOL CNorrClient_test_DLLDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CNorrClient_test_DLLDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CNorrClient_test_DLLDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CNorrClient_test_DLLDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


int (__stdcall *SkapaBestand)(void);
int (__stdcall *RaderaBestand)(int);
int (__stdcall *RaknaomBestand)(int);
int (__stdcall *KompletteraBestand)(int);

void CNorrClient_test_DLLDlg::OnBnClickedButton1()
{
	HINSTANCE hInst = AfxLoadLibrary(_T("NorrClient.dll"));
	if(hInst == NULL) return;

	SkapaBestand = (int (__stdcall *)(void)) GetProcAddress(hInst, "SkapaBestand");
	ASSERT(SkapaBestand != NULL);

	SkapaBestand();

	AfxFreeLibrary(hInst);
}

void CNorrClient_test_DLLDlg::OnBnClickedButton3()
{
	HINSTANCE hInst = AfxLoadLibrary(_T("NorrClient.dll"));
	if(hInst == NULL) return;

	RaderaBestand = (int (__stdcall *)(int)) GetProcAddress(hInst, "RaderaBestand");
	ASSERT(RaderaBestand != NULL);

	RaderaBestand(2);

	AfxFreeLibrary(hInst);
}

void CNorrClient_test_DLLDlg::OnBnClickedButton2()
{
	HINSTANCE hInst = AfxLoadLibrary(_T("NorrClient.dll"));
	if(hInst == NULL) return;

	RaknaomBestand = (int (__stdcall *)(int)) GetProcAddress(hInst, "RaknaomBestand");
	ASSERT(RaknaomBestand != NULL);

	RaknaomBestand(2);

	AfxFreeLibrary(hInst);
}

void CNorrClient_test_DLLDlg::OnBnClickedButton4()
{
	HINSTANCE hInst = AfxLoadLibrary(_T("NorrClient.dll"));
	if(hInst == NULL) return;

	KompletteraBestand = (int (__stdcall *)(int)) GetProcAddress(hInst, "KompletteraBestand");
	ASSERT(KompletteraBestand != NULL);

	KompletteraBestand(2);

	AfxFreeLibrary(hInst);
}
