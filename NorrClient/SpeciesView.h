#pragma once

#include "Resource.h"
#include "afxcmn.h"

// CSpeciesView form view

class CSpeciesView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSpeciesView)

	BOOL m_bPopulated;
	CString m_sAbrevLangSet;
	CString m_sLangFN;
	BOOL m_bDataNotChanged;
	CString m_csAddStr;
	CString m_csDelMsg;
	CString m_csMaxValue;

public:

	CXTPReportControl m_wndReport;

protected:
	CSpeciesView();           // protected constructor used by dynamic creation
	virtual ~CSpeciesView();
	
	void setColumnsFocusState(void);
	void addInsertRow(void);

public:
	enum { IDD = IDD_SPECIESVIEW };
	void setupDclsTable(double dcls);
	void deleteFromStandTable(void);
	int getNumOfTrees(void);
	void updateRecord(void);

	BOOL isOkToClose(void) {return m_bDataNotChanged;}
	void setDataChanged(void) { m_bDataNotChanged = FALSE;}
	void resetDataChanged(void) {m_bDataNotChanged = TRUE;}

	void removeAllItems();

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnInitialUpdate();
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	afx_msg void OnClickAddDcls(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKeyDownAddDcls(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
};


