#pragma once

#include "../NorrServer/Intermediate/NorrServer_h.h"


void DisplayException(int errcode);


// RPC wrapper functions. These functions include exception handling for convenience (to be used when we don't need to distinguish an exception from a failed procedure).

boolean _CreateStand(const char* pData, const wchar_t* pTemplateXml, const wchar_t* pUsername, int* nStandId);

boolean _CalculateStand(int nStandId);

boolean _ChangeTemplate(const int nStandId, const char *pTemplateXml);

boolean _GetStandArray(int nDesiredSize, int* nActualSize, struct STAND* pData);

boolean _GetStandCount(int* nCount);

boolean _GetTemplateArray(int nDesiredSize, int* nActualSize, struct TEMPLATE* pData);

boolean _GetTemplateCount(int* nCount);

boolean _GetTemplateXml(int nTemplateId, int nDesiredSize, int* nActualSize, wchar_t* pData);

boolean _GetPricelistXml(int nPricelistId, int nDesiredSize, int* nActualSize, wchar_t* pData);

boolean _GetStandData(int nStandId, struct STANDDATA* pData);

boolean _RemoveStand(int nStandId);

wchar_t* _GetLastMessage();

boolean _CheckLicense();
