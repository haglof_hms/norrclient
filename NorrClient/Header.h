#pragma once

#include "HXLParser.h"

// CHeader dialog

class CHeader : public CPropertyPage
{
	DECLARE_DYNAMIC(CHeader)

	HXLHEADER m_hxlHeader;
	BOOL m_bDontChangePage;

public:
	CHeader();
	virtual ~CHeader();
	virtual BOOL OnSetActive();

// Dialog Data
	enum { IDD = IDD_WIZARD_HEADER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual LRESULT OnWizardBack();
	virtual LRESULT OnWizardNext();
	virtual BOOL OnInitDialog();
	virtual BOOL OnKillActive();

	DECLARE_MESSAGE_MAP()
public:
	// Best�ndsnamn
	CString m_csBestand;
	// Delbest�nd
	CString m_csDelbestand;
	// Datum
	CTime m_ctDatum;
	// Altitud (h�jd �ver havet)
	int m_nAltitud;
	// Areal
	float m_fAreal;
	// �lder
	int m_nAlder;
	CString m_csSI;
	int m_nBreddgrad;
};
