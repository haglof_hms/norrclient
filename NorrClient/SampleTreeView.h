#pragma once

#include "stdafx.h"
#include "Resource.h"

// CSampleTreeView form view

//class CSampleTreeView : public CXTPReportView
class CSampleTreeView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSampleTreeView)

	BOOL m_bPopulated;
	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString csDefaultType;

	CString m_csErrMsgSampleTree;
	CString m_csErrMsgRandTree;
	CString m_csMenuCopy1;
	CString m_csMenuCopy2;

	BOOL m_bDataNotChanged;

public:
	CXTPReportControl m_wndReport;
	CStringArray m_sarrTreeTypes;

protected:
	CSampleTreeView();           // protected constructor used by dynamic creation
	virtual ~CSampleTreeView();

	BOOL isFocusedRowEmpty(void);
	int findTypeIndexAsInt(LPCTSTR name);
	void copyRowOnce(void);
	void copyRowMultiple(void);
	

public:
	void setNumOfTreeHeader(void);
	enum { IDD = IDD_SAMPLETREEVIEW };
	void setupDclsTable(int dcls);
	void deleteSampleTree(void);
	BOOL getSampleTree(vecMySampleTrees &vecST, int id);
	void updateRecord(void);
	BOOL isOkToClose(void) {return m_bDataNotChanged;}
	void setDataChanged(void) { m_bDataNotChanged = FALSE;}
	void resetDataChanged(void) {m_bDataNotChanged = TRUE;}
	int getNumOfTrees(void);
	void addTree(CMyTransactionSampleTree *tree);
	void removeAllItems();
	void addEmptyRow();

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnInitialUpdate();
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemRClick(NMHDR *pNotifyStruct, LRESULT* /*result*/);
};


