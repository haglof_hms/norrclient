#include "StdAfx.h"
#include "ResLangFileReader.h"

RLFReader::RLFReader(void)
{
}

RLFReader::~RLFReader()
{
	clean();
	m_doc.Clear();
}

void RLFReader::clean(void)
{
	m_strings.clear();
}

BOOL RLFReader::Load(LPCTSTR xml_fn)
{
	char szFilename[1024];
	TiXmlNode* pRootNode;
	TiXmlElement* pElement;
	TiXmlAttribute* pAttrib;
	int nId = 0;
	CString csBuf;

	// load the XML
	m_doc.Clear();
	sprintf(szFilename, "%S", xml_fn);
	if(!m_doc.LoadFile(szFilename))
	{
		AfxMessageBox(_T("Kunde inte �ppna spr�kfil!"), MB_ICONERROR);
//		MessageBox(NULL, _T("Kunde inte �ppna spr�kfil!"), _T("Fel!"), MB_ICONERROR);
		return -1;
	}

	if(m_doc.Error())
	{
//		AfxMessageBox( CString(m_doc.ErrorDesc()) );
		return FALSE;
	}
	

	// parse it
	pRootNode = m_doc.FirstChild("resource");
	pElement = pRootNode->FirstChildElement("string");
	while( pElement )
	{
		nId = atoi( pElement->Attribute("id") );
		if(nId != 0)
		{
//			csBuf = pElement->Attribute("value");
//			MultiByteToWideChar(CP_UTF8, 0, pElement->Attribute("value"), -1, csBuf.GetBuffer(), 1000);
			m_strings[nId] = pElement->Attribute("value");
//			csBuf.ReleaseBuffer();
//			csBuf.Empty();
		}

		nId = 0;
		pElement = pElement->NextSiblingElement("string");
	}

	return TRUE;
}

CString RLFReader::str(DWORD resid)
{
	return m_strings[resid];
}
