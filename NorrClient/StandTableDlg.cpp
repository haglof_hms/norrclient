// StandTableDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NorrClient.h"
#include "StandTableDlg.h"
#include "TabTreeTypes.h"
#include "TabPageManager.h"
#include "SampleTreeTabPageManager.h"
#include "SelectSpeciesDlg.h"
#include "SpeciesView.h"
#include "SampleTreeView.h"
#include "SpeciesViewReportRec.h"
#include <math.h>
#include "RpcProcedures.h"


// CStandTableDlg dialog

IMPLEMENT_DYNAMIC(CStandTableDlg, CXTResizeDialog)

CStandTableDlg::CStandTableDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CStandTableDlg::IDD, pParent)
{
	m_bDataNotChanged = TRUE;
	m_nInventeringsMetod = 0;	// provytetaxering
	m_pWnd = NULL;
	m_nDefInventeringsMetod = -1;
}

CStandTableDlg::CStandTableDlg(CString csFilename, int nInventoryType)
	: CXTResizeDialog(CStandTableDlg::IDD)
{
	m_csFilename = csFilename;

	if(nInventoryType == 1)	// provytor
		m_nDefInventeringsMetod = 0;
	else if(nInventoryType == 2)	// total
		m_nDefInventeringsMetod = 1;
	else
		m_nDefInventeringsMetod = nInventoryType;

	m_bDontLoadSave = FALSE;
}


CStandTableDlg::~CStandTableDlg()
{
}

void CStandTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRP1, m_wndGrp1);

	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL7, m_wndLbl7);
	DDX_Control(pDX, IDC_LBL8, m_wndLbl8);
	DDX_Control(pDX, IDC_LBL10, m_wndLbl10);
	DDX_Control(pDX, IDC_LBL_INVMETOD, m_wndLblInvmetod);
	DDX_Control(pDX, IDC_LBL_YTAREAL, m_wndLblYtareal);

	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT6, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit7);
	DDX_Control(pDX, IDC_EDIT8, m_wndEdit8);
	DDX_Control(pDX, IDC_EDIT10, m_wndEdit10);
	DDX_Control(pDX, IDC_COMBO_INVMETOD, m_cbInvmetod);
	DDX_Control(pDX, IDC_EDIT_YTAREAL, m_wndEditYtareal);
	DDX_CBIndex(pDX, IDC_COMBO_INVMETOD, m_nInventeringsMetod);
}


BEGIN_MESSAGE_MAP(CStandTableDlg, CXTResizeDialog)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_EN_CHANGE(IDC_EDIT3, OnEnChangeEdit3)
	ON_EN_CHANGE(IDC_EDIT4, OnEnChangeEdit4)
	ON_EN_CHANGE(IDC_EDIT5, OnEnChangeEdit5)
	ON_EN_CHANGE(IDC_EDIT6, OnEnChangeEdit6)
	ON_EN_CHANGE(IDC_EDIT7, OnEnChangeEdit7)
	ON_EN_CHANGE(IDC_EDIT8, OnEnChangeEdit8)
	ON_EN_CHANGE(IDC_EDIT10, OnEnChangeEdit10)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CStandTableDlg::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, &CStandTableDlg::OnBnClickedButtonCancel)
	ON_CBN_SELCHANGE(IDC_COMBO_INVMETOD, &CStandTableDlg::OnCbnSelchangeInvmetod)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TABCONTROL_PLOTS, OnSelChangeTab)
	ON_NOTIFY(TCN_SELCHANGING, IDC_TABCONTROL_PLOTS, OnSelChangingTab)
	ON_BN_CLICKED(IDC_BUTTON_ADD, &CStandTableDlg::OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, &CStandTableDlg::OnBnClickedButtonDel)
	ON_MESSAGE(WM_TIMER, OnTimer)
END_MESSAGE_MAP()

int CStandTableDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CXTPPaintManager::SetTheme(xtpThemeOffice2003);

	return 0;
}

BOOL CStandTableDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTResizeDialog::PreCreateWindow(cs))
		return FALSE;

//	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
//	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CStandTableDlg::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeDialog::OnSize(nType, cx, cy);
	if(m_wndTabControl.GetSafeHwnd())
	{
		RECT rect;
		GetWindowRect(&rect);
		ScreenToClient(&rect);

		GetDlgItem(IDC_TABCONTROL_PLOTS)->SetWindowPos(NULL, 0, 0, rect.right-29, rect.bottom-182, SWP_NOMOVE|SWP_NOOWNERZORDER|SWP_NOZORDER);
	}
}

// CStandTableDlg message handlers
BOOL CStandTableDlg::OnInitDialog()
{
	// tab control for sample plots
	CRect rc;
	GetDlgItem(IDC_PLACEHOLDER)->GetWindowRect(&rc);
	ScreenToClient( &rc );
//	m_wndTabControl = new CXTPTabControl;
	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP, rc, this, IDC_TABCONTROL_PLOTS);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = FALSE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);
	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS,NULL,0,CSize(16, 16), xtpImageNormal);

	CreateView(RUNTIME_CLASS(CTabTreeTypes));	// pre-create the view that will be reused on every plot tab

	m_csFileMsg = theApp.m_XML->str(164);	//file msg
	m_csErrCreateInvFile = theApp.m_XML->str(138);
	m_csErrMsgNoTree = theApp.m_XML->str(181);

	CXTResizeDialog::OnInitDialog();

	setLanguage();

	// �ndra egenskaper f�r editeringsf�lt
	m_wndEdit3.SetEnabledColor(BLACK, WHITE);
	m_wndEdit3.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit4.SetEnabledColor(BLACK, WHITE);
	m_wndEdit4.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit5.SetEnabledColor(BLACK, WHITE);
	m_wndEdit5.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit6.SetEnabledColor(BLACK, WHITE);
	m_wndEdit6.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit7.SetEnabledColor(BLACK, WHITE);
	m_wndEdit7.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit8.SetMask(_T(">##"));
	m_wndEdit8.SetPromptSymbol('_');
	m_wndEdit10.SetEnabledColor(BLACK, WHITE);
	m_wndEdit10.SetDisabledColor(BLACK, INFOBK);

	m_wndEdit4.SetAsNumeric();
	m_wndEdit5.SetAsNumeric();	//breddgrad
	m_wndEdit5.SetLimitText(3);
	m_wndEdit6.SetAsNumeric();	//h�jd �ver hav
	m_wndEdit6.SetLimitText(6);
	m_wndEdit7.SetAsNumeric();	//age
	m_wndEdit7.SetLimitText(4);
	m_wndEdit10.SetAsNumeric();	//Delnr


	// populera dialogen med data fr�n inventeringsfil
	if(m_csFilename != _T(""))
	{
		m_hxl.Open(m_csFilename);

		CString csBestand = m_hxl.getBestand();
		CString csDelbestand;

		int nPos = csBestand.Find('_');
		if( nPos != -1 )
		{
			csDelbestand = csBestand.Right( csBestand.GetLength() - nPos - 1 );
			csBestand = csBestand.Left( nPos );
		}

		setName( csBestand );
		setDelnr( csDelbestand );
		setArea( m_hxl.getAreal() / 10000 );
		setLatitude( m_hxl.getLatitud() );
		setHeightOverSea( m_hxl.getAltitud() );
		setAge( m_hxl.getBestAlder() );
		setH100( m_hxl.getSI() );
		setInventeringsmetod( m_hxl.getInventeringsmetod() );
		
		// skapa provytor
		CString csBuf;
		m_hxl.getPlots(&m_vecPlots);

		// set which species to use
		vecTransactionSpecies vecSpecies;
		getSpecies(vecSpecies);

		for(int nLoop=0; nLoop<vecSpecies.size(); nLoop++)
		{
			for(int nLoop2=0; nLoop2<m_vecPlots[0].vecTreeType[0].vecSpecies.size(); nLoop2++)
			{
				if(m_vecPlots[0].vecTreeType[0].vecSpecies[nLoop2].nSpeciesNum == vecSpecies[nLoop].getID())
				{
					m_vecSpecies.push_back(vecSpecies[nLoop]);
					break;
				}
			}
		}

		m_bDontLoadSave = TRUE;	// prevent saving to vectors

		for(int nLoop=0; nLoop<m_hxl.getNumPlots(); nLoop++)
		{
			// insert plot tab in the tab control
			csBuf.Format(_T("%s %d"), theApp.m_XML->str(34), nLoop+1);
			m_wndTabControl.InsertItem(nLoop, csBuf, m_pWnd->GetSafeHwnd(), 3);
		}
		AddSpecies(0);

		m_bDontLoadSave = FALSE;	// allow saving when changing tabs again
		m_bDataNotChanged = TRUE;

		if(m_hxl.getNumPlots() > 0)
		{
			GetDlgItem(IDC_COMBO_INVMETOD)->EnableWindow(FALSE);	// we can not change inventory method if we have plots
			if( m_wndTabControl.GetItemCount() > 1 ) m_wndTabControl.SetCurSel(1); // HACKHACK: force a refresh
			m_wndTabControl.SetCurSel(0);
		}
	}

	m_cbInvmetod.SetCurSel(m_nDefInventeringsMetod);

	// do not allow user to change inventory method if we have a default one
	if(m_nDefInventeringsMetod != -1)
		GetDlgItem(IDC_COMBO_INVMETOD)->EnableWindow(FALSE);

	// anchor buttons to the lower part of the window
	SetResize(IDC_BUTTON_ADD, SZ_BOTTOM_CENTER, SZ_BOTTOM_CENTER);
	SetResize(IDC_BUTTON_DEL, SZ_BOTTOM_CENTER, SZ_BOTTOM_CENTER);
	SetResize(IDC_BUTTON_SAVE, SZ_BOTTOM_CENTER, SZ_BOTTOM_CENTER);
	SetResize(IDC_BUTTON_CANCEL, SZ_BOTTOM_CENTER, SZ_BOTTOM_CENTER);

	// set a license-poker-timer on 10 minutes
	SetTimer(2, 10*60*1000, 0);

	return TRUE;
}

//BOOL CStandTableDlg::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
BOOL CStandTableDlg::CreateView(CRuntimeClass* pViewClass)
{
	CCreateContext contextT;
	contextT.m_pNewViewClass = pViewClass;

//	CWnd* m_pWnd;
	TRY
	{
		m_pWnd = (CWnd*)pViewClass->CreateObject();
		if (m_pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!m_pWnd->Create(NULL, NULL, dwStyle, rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		// pWnd will be cleaned up by PostNcDestroy
		AfxMessageBox(_T("Failed to create Window!"));
		return NULL;
	}
//	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	m_pWnd->SetOwner(this);
	m_pWnd->SendMessage(WM_INITIALUPDATE);

	return TRUE;
}

void CStandTableDlg::OnCancel()
{
	// kolla om vi har osparat data
	if(!isOkToClose())
	{
		CString csClose;
		csClose.Format(_T("%s\r\n\r\n%s\r\n\r\n%s"), theApp.m_XML->str(156), theApp.m_XML->str(157), theApp.m_XML->str(158));

		if(AfxMessageBox(csClose, MB_ICONEXCLAMATION | MB_YESNO) != IDYES)
		{
			//don't close window
			return;
		}
	}

	// st�ng av licenstimer
	KillTimer(2);

	CXTResizeDialog::OnCancel();
}

void CStandTableDlg::OnBnClickedButtonSpeciesAdd()
{
	// check if we have any sample plots
	if(m_wndTabControl.GetItemCount() == 0)
		return;

	CSelectSpeciesDlg *dlg = new CSelectSpeciesDlg();
	CXTPDockingPaneManager *pPaneManager = NULL;
	CXTPDockingPane *pPane = NULL;
	 
	//get current species from first tab (cut)
	CXTPTabManagerItem *pTabItem = m_wndTabControl.GetItem(0);	// sample plots
	CTabTreeTypes *pTabTreeTypes = DYNAMIC_DOWNCAST(CTabTreeTypes, CWnd::FromHandle(pTabItem->GetHandle()));	// tree types
	CXTPTabManagerItem *pCut = pTabTreeTypes->m_wndTabControl.GetItem(0);	// cut
	CTabPageManager* pTabPage = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pCut->GetHandle()));	// species

	int nSpeciesUsed = pTabPage->m_wndTabControl.GetItemCount();
	for(int i=0; i<nSpeciesUsed; i++)
	{
		dlg->setSpeciesUsed((int)(pTabPage->m_wndTabControl.GetItem(i))->GetData());
	}

	if(dlg->DoModal() == IDOK)
	{
		m_vecSpecies = dlg->getSpeciesSelected();

		if(m_vecSpecies.size() <= 0)
			return;	// FALSE;

		// we only need to add species to the first plot, since it is reused
		AddSpecies(0);
	}

	delete dlg;
}

int CStandTableDlg::AddSpecies(int nPlot)
{
	CXTPTabManagerItem *pTabItem = m_wndTabControl.GetItem(nPlot);	// sample plots
	CTabTreeTypes *pTabTreeTypes = DYNAMIC_DOWNCAST(CTabTreeTypes, CWnd::FromHandle(pTabItem->GetHandle()));	// tree types

	int nTabPages = pTabTreeTypes->m_wndTabControl.GetItemCount();

	// go through all tree type pages (cut, leave, sampleplots)
	for(int i = 0; i<nTabPages; i++)
	{
		CXTPTabManagerItem *pItem = pTabTreeTypes->m_wndTabControl.GetItem(i);

		if(pItem)
		{
			if(i != TAB_SAMPLETREE)
			{
				CTabPageManager *pTabManager = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pItem->GetHandle()));

				if(pTabManager)
				{
					if(m_vecSpecies.size() > 0)
					{
						for(UINT j=0; j<m_vecSpecies.size(); j++)
						{
							if(!pTabManager->AddView(RUNTIME_CLASS(CSpeciesView), m_vecSpecies[j].getSpcName(), m_vecSpecies[j].getSpcID(), 3))
								return FALSE;
						}
					}//if(m_vecSpecies.size() > 0)
				}//if(pManager)
			}
			else if(i == TAB_SAMPLETREE)
			{
				CSampleTreeTabPageManager *pSampleTreeTabManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pItem->GetHandle()));

				if(pSampleTreeTabManager)
				{
					if(m_vecSpecies.size() > 0)
					{
						for(UINT j=0; j<m_vecSpecies.size(); j++)
						{
							if(!pSampleTreeTabManager->AddView(RUNTIME_CLASS(CSampleTreeView), m_vecSpecies[j].getSpcName(), m_vecSpecies[j].getSpcID(), 3))
								return FALSE;
						}
					}//if(m_vecSpecies.size() > 0)
				}//if(pSampleTreeManager)
			}
		}//if(pItem)
	}

	//create dclstables
	double dcls = 2.0;
	setupDclsTables(nPlot, dcls);

	return TRUE;
}

void CStandTableDlg::OnBnClickedButtonSpeciesDel()
{
	// check if we have any sample plots
	if(m_wndTabControl.GetItemCount() == 0)
		return;

	int nId;

	CXTPTabManagerItem *pTabItem = m_wndTabControl.GetItem( m_wndTabControl.GetCurSel() );	// sample plots
	CTabTreeTypes *pTabTreeTypes = DYNAMIC_DOWNCAST(CTabTreeTypes, CWnd::FromHandle(pTabItem->GetHandle()));	// tree types
	CXTPTabManagerItem *pActTab = pTabTreeTypes->m_wndTabControl.GetItem( pTabTreeTypes->m_wndTabControl.GetCurSel() );

	if(pActTab)
	{
		if(pActTab->GetIndex() != TAB_SAMPLETREE)
		{
			CTabPageManager *pActManager = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pActTab->GetHandle()));
			if(pActManager)
			{
				if(pActManager->m_wndTabControl.GetItemCount() <= 0)
					return;	// 0;//TRUE;
				nId = (int)(pActManager->m_wndTabControl.GetItem( pActManager->m_wndTabControl.GetCurSel() ))->GetData();
			}//if(pActManager)
			
		}
		else if(pActTab->GetIndex() == TAB_SAMPLETREE)
		{
			CSampleTreeTabPageManager *pSampleTreeActManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pActTab->GetHandle()));
			if(pSampleTreeActManager)
			{
				if(pSampleTreeActManager->m_wndTabControl.GetItemCount() <= 0)
					return;	// 0;//TRUE;
				nId = (int)(pSampleTreeActManager->m_wndTabControl.GetItem( pSampleTreeActManager->m_wndTabControl.GetCurSel() ))->GetData();
			}//if(pSampleTreeActManager)
		}
	}

	//ask if selected species should be deleted
	if(AfxMessageBox(theApp.m_XML->str(154), MB_YESNO|MB_ICONEXCLAMATION) != IDYES)
	{
		return;
	}

	// remove the species from the species vector
	for(int nLoop=0; nLoop<m_vecSpecies.size(); nLoop++)
	{
		if(m_vecSpecies[nLoop].getSpcID() == nId)
		{
			m_vecSpecies.erase( m_vecSpecies.begin() + nLoop );
			break;
		}
	}

	int nPages = 0;
	int nPlot = 0;

	pTabItem = m_wndTabControl.GetItem(nPlot);	// sample plots
	pTabTreeTypes = DYNAMIC_DOWNCAST(CTabTreeTypes, CWnd::FromHandle(pTabItem->GetHandle()));	// tree types

	// tree type tabs
	int nTabPages = pTabTreeTypes->m_wndTabControl.GetItemCount();
	for(int i=0; i<nTabPages; i++)
	{
		CXTPTabManagerItem *pItem = pTabTreeTypes->m_wndTabControl.GetItem(i);
		if(pItem)
		{
			if(i != TAB_SAMPLETREE)
			{
				CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
				if(pManager)
				{
					nPages = pManager->m_wndTabControl.GetItemCount();
					for(int j=0; j<nPages;j++)
					{
						CXTPTabManagerItem *pSelectedItem = pManager->m_wndTabControl.GetItem(j);
						if(pSelectedItem)
						{
							if(pSelectedItem->GetData() == nId)
							{
								pSelectedItem->Remove();
							}
						}//if(pSelectedItem)
					}//for(int j=0; j<nPages;j++)
				}//if(pManager)
			}
			else if(i == TAB_SAMPLETREE)
			{
				CSampleTreeTabPageManager *pSampleTreeManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
				if(pSampleTreeManager)
				{
					nPages = pSampleTreeManager->m_wndTabControl.GetItemCount();
					for(int j=0; j<nPages;j++)
					{
						CXTPTabManagerItem *pSampleTreeSelectedItem = pSampleTreeManager->m_wndTabControl.GetItem(j);
						if(pSampleTreeSelectedItem)
						{
							if(pSampleTreeSelectedItem->GetData() == nId)
							{
								pSampleTreeSelectedItem->Remove();
							}
						}//if(pSampleTreeSelectedItem)
					}//for(int j=0; j<nPages;j++)
				}//if(pSampleTreeManager)
			}
		}//if(pItem)
	}//for(int i=0; i<nTabPages; i++)
}

void CStandTableDlg::setupDclsTables(int nPlot, double dcls)
{
	// setup diameter class tables for a sample plot
	CXTPTabManagerItem *pTabItem = m_wndTabControl.GetItem(nPlot);	// sample plots
	CTabTreeTypes *pTabTreeTypes = DYNAMIC_DOWNCAST(CTabTreeTypes, CWnd::FromHandle(pTabItem->GetHandle()));	// tree types

	int nTabPages = pTabTreeTypes->m_wndTabControl.GetItemCount();
	for(int i=0; i<nTabPages; i++)
	{
		CXTPTabManagerItem *pItem = pTabTreeTypes->m_wndTabControl.GetItem(i);
		if(pItem)
		{
			if(i != TAB_SAMPLETREE)
			{
				CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
				if(pManager)
				{
					pManager->setupDclsTables(dcls);
				}//if(pManager)
			}
			else if(i == TAB_SAMPLETREE)
			{
				//creates empty row when initialized
				CSampleTreeTabPageManager *pSampleTreeManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
				if(pSampleTreeManager)
				{
					pSampleTreeManager->setupDclsTables(dcls);
				}//if(pSampleTreeManager)
			}
		}//if(pItem)
	}//for(int i=0; i<nTabPages; i++)
}

BOOL CStandTableDlg::saveDataToHxlFile(CString csFilename)
{
	CStringA csStr, csPart, csTmp; // ANSI output
	CString csFilePath;
	CString csErr1,csErr2, csErr3;

	//update data in records to get changes
//	updateRecords();
	
	csErr1 = theApp.m_XML->str(192);
	csErr2 = theApp.m_XML->str(193);
	csErr3 = theApp.m_XML->str(194);

	//Kontroll av block, enhet, delnr och sida
	//koll att delnummer ej �r 0
	int nNum;
	if(getDelnr() != _T(""))
	{
		nNum = _tstoi(getDelnr());
		if(nNum == 0)
		{
			setDelnr(_T(""));
			setUpdate(FALSE);
			AfxMessageBox(csErr3);
			return FALSE;
		}
	}


	AfxGetApp()->BeginWaitCursor();

	//header
	csStr += "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";

	//HXL
	csStr += "<HXL>\n";

	//Document
	csStr += "<Document>\n";

	struct tm T;
	time_t t;
	time(&t);
	localtime_s(&T, &t);
	csPart.Format("<FileDate>%04d%02d%02d</FileDate>\n",T.tm_year+1900,T.tm_mon+1,T.tm_mday);
	csStr += csPart;

	csStr += "<Type>HXL HMS</Type>\n";
	csStr += "<Ver>107</Ver>\n";
	csStr += "</Document>\n";

	//Object
	csStr += "<Object>\n";
	//objectData
	csStr += "<objectData>\n";
	//1 2 program version
	csStr += "<ProgramVersion Name=\"PROGRAMVERSION\" Var=\"1\" Type=\"2\">HXL HMS 1.0.7</ProgramVersion>\n";
	//1 3 charset
	csStr += "<Tecken Name=\"CHARSET\" Var=\"1\" Type=\"3\">ISO 8859-1</Tecken>\n";
	//2 1 stand name
	csTmp = CheckStringNotForbidden(getName()) + _T(" ");	// + CheckStringNotForbidden(getNumber());
	if(getDelnr() != _T(""))
	{
		//Egentligen on�digt: Delnr should always be a int, in that case no check with CheckStringNotForbidden() is required.
		csTmp += _T("_") + CheckStringNotForbidden(getDelnr());
	}
	csPart.Format("<Best Name=\"STANDID\" Var=\"2\" Type=\"1\">%s</Best>\n", csTmp);
	csStr += csPart;
	//12 4 date
	csPart.Format("<Date Name=\"DATE\" Var=\"12\" Type=\"4\">%04d%02d%02d%02d%02d%02d</Date>\n",T.tm_year+1900,T.tm_mon+1,T.tm_mday,T.tm_hour,T.tm_min,T.tm_sec);
	csStr += csPart;
	//111 1 number of species used
	int nNumOfSpc = getNumOfSpcUsed();
	if(nNumOfSpc > 0)
	{
		csPart.Format("<Numsp Name=\"NUMSPEC\" Var=\"111\" Type=\"1\">%d</Numsp>\n", nNumOfSpc);
		csStr += csPart;

		//SpecieSet
		csStr += "<SpecieSet>\n";
		csStr += "<SpecieVar Name=\"SPECIENAME\" Var=\"120\" Type=\"1\"/>\n";
		csStr += "<SpecieVar Name=\"SPECIENUMBER\" Var=\"120\" Type=\"3\"/>\n";
		csStr += "<Species>";
		//120 1 och 120 3
		csStr += getSpcNamesUsedHxl();
		csStr += "</Species>\n";
		csStr += "</SpecieSet>\n";

	}
	else
	{
		AfxMessageBox(m_csErrMsgNoTree, MB_ICONERROR);
		return FALSE;
	}

	//651 1 number of plots
	csPart.Format("<Numplots Name=\"NUMPLOT\" Var=\"651\" Type=\"1\">%d</Numplots>\n", m_vecPlots.size());
	csStr += csPart;

	//660 1 age on stand
	if(getAge() != _T(""))
	{
		int age = _tstoi(getAge());
		if(age >= 0)
		{
			csPart.Format("<BestAge Name=\"BESTAGE\" Var=\"660\" Type=\"1\">%d</BestAge>\n", age);
			csStr += csPart;
		}
	}

	//670 1 areal in m2
	int area = 0;
	if(getArea() != _T(""))
	{
		double tarea = _tstof(getArea());
		area = (int)(tarea * 10000.0);	//from ha to m2
		if(area > 0)
		{
			csPart.Format("<Areal Name=\"AREAL\" Var=\"670\" Type=\"1\">%d</Areal>\n", area);
			csStr += csPart;
		}
	}
		
	//2011 2 latitud
	if(getLatitude() != _T(""))
	{
		int lat = _tstoi(getLatitude());
		lat *= 10;
		if(lat >= 0)
		{
			csPart.Format("<Latitude Name=\"LATITUDE\" Var=\"2011\" Type=\"2\">%d</Latitude>\n", lat);
			csStr += csPart;
		}
	}

	//2012 1 height above sea level
	if(getHeightOverSea() != _T(""))
	{
		int seahgt = _tstoi(getHeightOverSea());
		if(seahgt >= 0)
		{
			csPart.Format("<HgtAboveSea Name=\"HGTABOVESEA\" Var=\"2012\" Type=\"1\">%d</HgtAboveSea>\n", seahgt);
			csStr += csPart;
		}
	}

	//2030 1 inventory method, 1 = sample plots, 2 = total inventory
	if(m_nInventeringsMetod == 0)	// sample plots
		csStr += "<Metod Name=\"TAXMETHOD\" Var=\"2030\" Type=\"1\">1</Metod>\n";
	else if(m_nInventeringsMetod == 1)	// total inventory
		csStr += "<Metod Name=\"TAXMETHOD\" Var=\"2030\" Type=\"1\">2</Metod>\n";


	//2095 2 bonitet
	csTmp.Format("%s", CW2A(getH100()));
	if(csTmp != _T("___"))
	{
		csTmp += _T("0");	//dm
		csPart.Format("<Bonitet2 Name=\"BONITET2\" Var=\"2095\" Type=\"2\">%s</Bonitet2>\n", csTmp);
		csStr += csPart;
	}

	csStr += "</objectData>\n";
	//PlotSet
	csStr += "<PlotSet>\n";

	for(int nPlot=0; nPlot<m_vecPlots.size(); nPlot++)
	{
		//Plot
		csStr += "<Plot>\n";
		//2052 1 plot id
		csPart.Format("<Id Name=\"ID\" Var=\"2052\" Type=\"1\">%d</Id>\n", m_vecPlots[nPlot].nPlotNum);
		csStr += csPart;

		//671 1 areal
		if(m_vecPlots[nPlot].nAreal > 0)
		{
			csPart.Format("<Areal Name=\"AREAL\" Var=\"671\" Type=\"1\">%d</Areal>\n", m_vecPlots[nPlot].nAreal);	// m2
			csStr += csPart;
		}
		
		//End of Plot
		csStr += "</Plot>\n";
		}

	csStr += "</PlotSet>\n";


	//TreeSet
	if(nNumOfSpc > 0)
	{
		csStr += "<TreeSet>\n";
		csStr += "<TreeVar Name=\"SPECNR\" Var=\"652\" Type=\"1\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"DBH\" Var=\"653\" Type=\"1\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"HGTNUM\" Var=\"654\" Type=\"1\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"HGT\" Var=\"656\" Type=\"1\" Def=\"50\"/>\n";
		csStr += "<TreeVar Name=\"HGT2\" Var=\"656\" Type=\"1\" Def=\"51\"/>\n";
		csStr += "<TreeVar Name=\"BARK\" Var=\"2003\" Type=\"2\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"AGE\" Var=\"2001\" Type=\"2\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"TYPE\" Var=\"2004\" Type=\"2\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"BONITET\" Var=\"2006\" Type=\"1\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"PLOT\" Var=\"2052\" Type=\"1\" Def=\"\"/>\n";

		//Tree
		csStr += "<Tree>";
		//652 1, 653 1, 654 1, 656 1 50, 656 1 51, 2003 2, 2001 2, 2004 2, 2006 1, 2052 1
//		csPart = getTreeDataHxl();
//		if(csPart == _T("FALSE"))
//			return FALSE;

		DCLS dc;
		CMyTransactionSampleTree st;
		SPECIES sp;
		TREETYPE tt;
		PLOT pl;
		int nNumHeights = 0, nHgt = 0, nHgt2 = 0;
		float fGcrown = 0.0;

		for(int nPlot=0; nPlot<m_vecPlots.size(); nPlot++)
		{
			pl = m_vecPlots[nPlot];

			for(int nTreeType=0; nTreeType<pl.vecTreeType.size(); nTreeType++)
			{
				tt = pl.vecTreeType[nTreeType];

				for(int nSpecies=0; nSpecies<tt.vecSpecies.size(); nSpecies++)
				{
					sp = tt.vecSpecies[nSpecies];

					for(int nTree=0; nTree<sp.vecDcls.size(); nTree++)
					{
						dc = sp.vecDcls[nTree];

						for(int nDia=0; nDia<dc.nAmount; nDia++)
						{
							csPart.Format("%d,%d,0,0,0,0,0,%d,0,%d\n", sp.nSpeciesNum, (dc.nDclsNum * 20) + 10, tt.nTreeTypeNum, pl.nPlotNum);
							csStr += csPart;
						}
					}
 
//unsigned short int type:2;   //AVV_IN_FOREST=0,REMAINED=1,AVK_IN_ROAD=2}
//enum CALCMETHODS  {AVV_IN_FOREST,REMAINED,AVK_IN_ROAD,AVK_ALL};//0=avv 1=kvar 2=istk  3=allt
// 0 = avverkat, 1 = kvarl�mnat, 2 = avverkat i v�g

					for(int nSampleTree=0; nSampleTree<sp.vecSampleTrees.size(); nSampleTree++)
					{
						st = sp.vecSampleTrees[nSampleTree];

						nNumHeights = 0;
						nHgt = 0;
						nHgt2 = 0;

						if(st.getHgt() > 0.0)
						{
							nNumHeights++;
							nHgt = (int)st.getHgt() * 10.0;
						}

						if(st.getGCrown() > 0.0)
						{
							nNumHeights++;
							nHgt2 = (int)st.getHgt() * 10.0 * (1.0 - st.getGCrown() / 100);
						}

						csPart.Format("%d,%.0f,%d,%d,%d,%.0f,%.0f,%d,%.0f,%d\n", sp.nSpeciesNum, st.getDbh(), nNumHeights, nHgt, nHgt2,
							st.getBrk(), st.getAge(), st.getType(), st.getBonitet()*10.0, pl.nPlotNum);
						csStr += csPart;
					}
				}
			}
		}

		csStr += "</Tree>\n";
		csStr += "</TreeSet>\n";
	}

	csStr += "</Object>\n";
	csStr += "</HXL>\n";

//	m_csOutputHXL = csStr;

	//create file
	CFile cfFile;
	if(cfFile.Open(csFilename, CFile::modeReadWrite|CFile::modeCreate) == 0)
	{
		//can't create file
		AfxMessageBox(m_csErrCreateInvFile);
		return FALSE;
	}

	cfFile.Write(csStr.GetBuffer(), csStr.GetLength());
	cfFile.Close();
	m_csFilename = csFilename;


	resetDataChanged();

	AfxGetApp()->EndWaitCursor();

	return TRUE;
}

void CStandTableDlg::setLanguage(void)
{
	SetWindowText(theApp.m_XML->str(38));	// Skapa/redigera inventeringsfil

	m_wndLbl3.SetWindowText(theApp.m_XML->str(103));
	m_wndLbl4.SetWindowText(theApp.m_XML->str(104));
	m_wndLbl5.SetWindowText(theApp.m_XML->str(105));
	m_wndLbl6.SetWindowText(theApp.m_XML->str(106));
	m_wndLbl7.SetWindowText(theApp.m_XML->str(107));
	m_wndLbl8.SetWindowText(theApp.m_XML->str(108));
	m_wndLbl10.SetWindowText(theApp.m_XML->str(191));
	m_wndLblInvmetod.SetWindowText(theApp.m_XML->str(29));	// Inventeringsmetod
	m_wndLblYtareal.SetWindowText(theApp.m_XML->str(43));	// ytareal

	m_csMsgSide = theApp.m_XML->str(117);
	m_csMsgSILimit = theApp.m_XML->str(118);
	m_csMsgSISpec = theApp.m_XML->str(119);
	m_csMsgDelnr = theApp.m_XML->str(194);

	// 1 = Provytetax, 2 = Totaltax/Provtr�d, 3 = Snabbtaxering, 5 = Relaskop, 6 = V�rdering
	m_cbInvmetod.AddString(theApp.m_XML->str(30));	//_T("Provytetaxering"));
	m_cbInvmetod.AddString(theApp.m_XML->str(31));	//_T("Totaltaxering"));

}

void CStandTableDlg::OnEnChangeEdit3()
{
	//data changed, set flag
	setDataChanged();
}

void CStandTableDlg::OnEnChangeEdit4()
{
	//data changed, set flag
	setDataChanged();
}

void CStandTableDlg::OnEnChangeEdit5()
{
	//data changed, set flag
	setDataChanged();
}

void CStandTableDlg::OnEnChangeEdit6()
{
	//data changed, set flag
	setDataChanged();
}

void CStandTableDlg::OnEnChangeEdit7()
{
	//data changed, set flag
	setDataChanged();
}


void CStandTableDlg::OnEnChangeEdit8()
{
	CString csStr, csSpec, csValue;
	int value;

	if(m_wndEdit8.IsInputEmpty() == FALSE)
	{
		csStr.Format(_T("%s"), m_wndEdit8.GetInputData());
		csSpec = csStr.GetAt(0);
		

		if(csSpec != _T('_'))
		{
			if(csSpec == _T('T') || csSpec == _T('G') || csSpec == _T('B') || csSpec == _T('E') || csSpec == _T('F') || csSpec == _T('C'))
			{
				if(csStr.GetAt(1) != _T('_') && csStr.GetAt(2) != _T('_'))
				{
					csValue = csStr.Right(2);
					value = _tstoi(csValue);

					if((csSpec == _T('T') && (value < 10 || value > 32)) || (csSpec == _T('G') && (value < 10 || value > 40)) || ((csSpec != _T('T') && csSpec != _T('G')) && (value < 14 || value > 30)))
					{
						AfxMessageBox(m_csMsgSILimit);
						m_wndEdit8.EmptyData(TRUE);
						m_wndEdit8.SetFocus();
					}

					//data changed, set flag
					setDataChanged();
				}

			}
			else
			{
				AfxMessageBox(m_csMsgSISpec);
				m_wndEdit8.EmptyData(TRUE);
				m_wndEdit8.SetFocus();
			}
		}
	}	
}

void CStandTableDlg::OnEnChangeEdit10()
{
	//data changed, set flag
	setDataChanged();
}

int CStandTableDlg::getNumOfSpcUsed()
{
	int nNumOfSpc = 0;
	std::vector<int> vecSpeciesUsed;
	BOOL bExists;

	m_vecSpeciesUsed.clear();

	// get the amount of species in use
	for(int nPlot=0; nPlot<m_vecPlots.size(); nPlot++)
	{
		for(int nTreeType=0; nTreeType<m_vecPlots[nPlot].vecTreeType.size(); nTreeType++)
		{
			for(int nSpecies=0; nSpecies<m_vecPlots[nPlot].vecTreeType[0].vecSpecies.size(); nSpecies++)
			{
				// got any trees?
				if( m_vecPlots[nPlot].vecTreeType[0].vecSpecies[nSpecies].vecDcls.size() > 0 ||
					m_vecPlots[nPlot].vecTreeType[0].vecSpecies[nSpecies].vecSampleTrees.size() > 0)
				{
					bExists = FALSE;
					for(int nLoop=0; nLoop<vecSpeciesUsed.size(); nLoop++)
					{
						if( vecSpeciesUsed[nLoop] == m_vecPlots[nPlot].vecTreeType[0].vecSpecies[nSpecies].nSpeciesNum )
						{
							bExists = TRUE;
							break;
						}
					}

					if(bExists == FALSE)
						vecSpeciesUsed.push_back( m_vecPlots[nPlot].vecTreeType[0].vecSpecies[nSpecies].nSpeciesNum );

				}				
			}
		}
	}

	for(int nLoop=0; nLoop<vecSpeciesUsed.size(); nLoop++)
	{
		nNumOfSpc++;
		m_vecSpeciesUsed.push_back( vecSpeciesUsed[nLoop] );
	}


	return nNumOfSpc;
}

CString CStandTableDlg::getSpcNamesUsedHxl()
{
	CString csRes = _T(""), csPart = _T("");

	for(UINT i=0; i<m_vecSpeciesUsed.size(); i++)
	{
		csPart.Format(_T("%s,%d\n"),getSpcName(m_vecSpecies,m_vecSpeciesUsed[i]),m_vecSpeciesUsed[i]);
		csRes += csPart;
	}

	return csRes;
}

BOOL CStandTableDlg::getSampleTrees()
{
	m_vecSampleTrees.clear();

	CXTPTabManagerItem *pItem = m_wndTabControl.GetItem(TAB_SAMPLETREE);
	if(!pItem)
		return FALSE;
	
	CSampleTreeTabPageManager *pManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
	if(!pManager)
		return FALSE;

	return pManager->getSampleTrees(m_vecSampleTrees);
}

BOOL CStandTableDlg::isSampleTreeSet(double id, double start, double stop, int type, CMyTransactionSampleTree &stree)
{
	for(UINT i=0; i<m_vecSampleTrees.size(); i++)
	{
		if((m_vecSampleTrees[i].getDbh() >= start*10.0) && (m_vecSampleTrees[i].getDbh() < stop*10.0) &&
			(m_vecSampleTrees[i].getType() == type) && (m_vecSampleTrees[i].getSpcId() == id))
		{
			stree = m_vecSampleTrees[i];
			m_vecSampleTrees.erase(m_vecSampleTrees.begin() + i);
			return TRUE;
		}
	}

	return FALSE;
}

void CStandTableDlg::setName(LPCTSTR sName)
{
	if(sName != _T(""))
	{
		m_wndEdit3.SetWindowTextW(sName);
		setDataChanged();
	}
}

void CStandTableDlg::setArea(int nArea)
{
	double area;
	if(nArea >= 0)
	{
		area = nArea / 1.0;	//10000.0;	//m2 -> ha
		m_wndEdit4.setFloat(area,3);
		setDataChanged();
	}
}

void CStandTableDlg::setLatitude(int nLatitude)
{
	if(nLatitude >= 0)
	{
		m_wndEdit5.setInt(nLatitude);
		setDataChanged();
	}
}

void CStandTableDlg::setHeightOverSea(int nHgtSea)
{
	if(nHgtSea >= 0)
	{
		m_wndEdit6.setInt(nHgtSea);
		setDataChanged();
	}
}

void CStandTableDlg::setAge(int nAge)
{
	if(nAge >= 0)
	{
		m_wndEdit7.setInt(nAge);
		setDataChanged();
	}
}

void CStandTableDlg::setH100(LPCTSTR sSi)
{
	if(sSi != _T(""))
	{
		m_wndEdit8.SetWindowTextW(sSi);
		setDataChanged();
	}
}

void CStandTableDlg::setDelnr(LPCTSTR sNum)
{
	if(sNum != _T(""))
	{
		m_wndEdit10.SetWindowTextW(sNum);
		setDataChanged();
	}
}

void CStandTableDlg::OnBnClickedButtonSave()
{
	// kolla att anv�ndaren har matat in best�ndsnamn, areal, �lder samt att det finns registrerade tr�d.
	BOOL bMissing = FALSE;
	CString csMsg;

	if(getName() == _T(""))
	{
		bMissing = TRUE;
		csMsg += theApp.m_XML->str(165) + _T("\n");	//_T("Best�ndsnamn saknas!\n");
	}

	if(getArea() == _T(""))
	{
		bMissing = TRUE;
		csMsg += theApp.m_XML->str(167) + _T("\n");	//_T("Areal saknas!\n");
	}

	if(getAge() == _T(""))
	{
		bMissing = TRUE;
		csMsg += theApp.m_XML->str(168) + _T("\n");	//_T("Best�nds�lder saknas!\n");
	}

/*	if(getNumOfSpcUsed() == 0)
	{
		bMissing = TRUE;
		csMsg += theApp.m_XML->str(181) + _T("\n");	//_T("Det finns inga tr�d registrerade!\n");
	}*/


	if(bMissing == FALSE)
	{
		if(m_csFilename == _T(""))	// this file is created from scratch
		{
			// show a filsave-dialog
			CString csFilename, csDirPath;

			// suggest stand name as default file name
			csFilename = getName();
			if(getDelnr() != _T(""))
				csFilename += (_T("_") + getDelnr());

			TCHAR tzFilename[1024], tzInitDir[1024];
			_stprintf(tzFilename, csFilename);
			_stprintf(tzInitDir, csDirPath);
			
			CFileDialog dlgF(FALSE);
			dlgF.m_ofn.lpstrFilter = (LPTSTR)_T("HXL (*.hxl)\0*.hxl\0\0");
			dlgF.m_ofn.hwndOwner = AfxGetMainWnd()->GetSafeHwnd();
			dlgF.m_ofn.lpstrFile = tzFilename;
			dlgF.m_ofn.lpstrInitialDir = tzInitDir;
			dlgF.m_ofn.Flags = OFN_EXPLORER|OFN_ENABLEHOOK|OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT;
			dlgF.m_ofn.lpstrDefExt = _T(".hxl");

			if(dlgF.DoModal() != IDOK)
			{
				return;
			}

			// save current tab first
			int nTab = m_wndTabControl.GetCurSel();

			// save the data connected to the plot, replacing current data
			if(nTab != -1)
			{
				if(!SavePlotData(nTab))
					return;
			}

			csFilename = dlgF.GetPathName();
			if(saveDataToHxlFile(csFilename))
				CXTResizeDialog::OnOK();
		}
		else	// saving an edited file
		{
			// save current tab first
			int nTab = m_wndTabControl.GetCurSel();

			// save the data connected to the plot, replacing current data
			if(nTab != -1)
			{
				if(!SavePlotData(nTab))
					return;
			}

			// update header variables
			CString csBuf = getName();
			if(getDelnr() != _T(""))
				csBuf += (_T("_") + getDelnr());

			m_hxl.setBestand(csBuf);
			m_hxl.setAreal( _wtoi(getArea()) * 10000 );
			m_hxl.setLatitud( _wtoi(getLatitude()) * 10 );
			m_hxl.setAltitud( _wtoi(getHeightOverSea()) );
			m_hxl.setBestAlder( _wtoi(getAge()) );
			m_hxl.setSI( getH100() );
			m_hxl.setInventeringsmetod( getInventeringsmetod() );

			// update species
			m_hxl.setSpecies(&m_vecSpecies);

			// update plots
			m_hxl.setPlots(&m_vecPlots);

			// update trees
			m_hxl.setTrees(&m_vecPlots);

			// save the HXL file
			m_hxl.Save(m_csFilename);

			// close the dialog
			CXTResizeDialog::OnOK();
		}
	}
	else
	{
		AfxMessageBox(csMsg);
	}
}

void CStandTableDlg::OnOK()
{
//	OnCancel();
}

void CStandTableDlg::OnBnClickedButtonCancel()
{
	OnCancel();
}

void CStandTableDlg::OnCbnSelchangeInvmetod()
{
	m_nInventeringsMetod = m_cbInvmetod.GetCurSel();

	if(m_nInventeringsMetod == 1)	// Totaltaxering
	{
		m_wndLblYtareal.EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_YTAREAL)->EnableWindow(FALSE);

		OnBnClickedButtonPlotAdd();	// add a "plot"
	}
	else
	{
		m_wndLblYtareal.EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_YTAREAL)->EnableWindow(TRUE);
	}
}

void CStandTableDlg::OnBnClickedButtonPlotAdd()
{
	// add a new sample plot
	CString csBuf;
	int nNumPlots = 0;

	if( m_wndTabControl.GetItemCount() > 0)
	{
		if(m_nInventeringsMetod == 1)	// totaltaxering
			return;

		CXTPTabManagerItem *pTabItem = m_wndTabControl.GetItem( m_wndTabControl.GetItemCount()-1 );	// last sample plot
		csBuf = pTabItem->GetCaption();
		nNumPlots = _wtoi( csBuf.Right( csBuf.GetLength() - csBuf.ReverseFind(' ') ) );
	}

	// save current data first
	int nCurPlot = m_wndTabControl.GetCurSel();
	if(nCurPlot != -1)
	{
		if(!SavePlotData( nCurPlot ))
		{
			AfxMessageBox(_T("Kunde inte spara data!"));
			return;
		}
	}


	// create a new plot in the plot vector
	PLOT p;
	p.nPlotNum = nNumPlots+1;
	p.nAreal = _wtoi(getYtarea());
	m_vecPlots.push_back(p);

	// insert plot tab in the tab control
	if(m_nInventeringsMetod == 1)	// totaltaxering
		csBuf.Format(_T("%s"), theApp.m_XML->str(31));
	else	// provytor
		csBuf.Format(_T("%s %d"), theApp.m_XML->str(34), nNumPlots+1);

	m_wndTabControl.InsertItem(nNumPlots, csBuf, m_pWnd->GetSafeHwnd(), 3);

	// set focus on the newly created sample plot tab
	m_wndTabControl.SetCurSel( m_wndTabControl.GetItemCount()-1 );

	if(m_wndTabControl.GetItemCount() > 0)
	{
		GetDlgItem(IDC_COMBO_INVMETOD)->EnableWindow(FALSE);	// we can not change inventory method if we have plots
	}
}

void CStandTableDlg::OnBnClickedButtonPlotDel()
{
	// remove selected sample plot (after asking)
	if(AfxMessageBox(theApp.m_XML->str(37), MB_YESNO|MB_ICONEXCLAMATION) != IDYES)
	{
		return;
	}

	// remove associated data
	int nPlot = m_wndTabControl.GetCurSel();

	for(int nLoop=0; nLoop<m_vecPlots[nPlot].vecTreeType.size(); nLoop++)
	{
		for(int nLoop2=0; nLoop2<m_vecPlots[nPlot].vecTreeType[nLoop].vecSpecies.size(); nLoop2++)
		{
			m_vecPlots[nPlot].vecTreeType[nLoop].vecSpecies[nLoop2].vecDcls.clear();
			m_vecPlots[nPlot].vecTreeType[nLoop].vecSpecies[nLoop2].vecSampleTrees.clear();
		}
	}
	m_vecPlots.erase( m_vecPlots.begin() + nPlot );

	// will there be any plots left?
	if(m_wndTabControl.GetItemCount() == 1)
	{
		// we can change inventory method if we don't have plots
		// do not allow user to change inventory method if we have a default one
		if(m_nDefInventeringsMetod == -1)
			GetDlgItem(IDC_COMBO_INVMETOD)->EnableWindow(TRUE);

		// remove all species tabs
		int nPlot = 0;
		CXTPTabManagerItem *pTabItem = m_wndTabControl.GetItem(nPlot);	// sample plot
		if(!pTabItem) return;
		CTabTreeTypes *pTabTreeTypes = DYNAMIC_DOWNCAST(CTabTreeTypes, CWnd::FromHandle(pTabItem->GetHandle()));	// tree types
		if(!pTabTreeTypes) return;

		// tree type tabs
		int nTabPages = pTabTreeTypes->m_wndTabControl.GetItemCount();
		for(int i=0; i<nTabPages; i++)
		{
			CXTPTabManagerItem *pItem = pTabTreeTypes->m_wndTabControl.GetItem(i);
			if(pItem)
			{
				if(i != TAB_SAMPLETREE)
				{
					CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
					if(pManager)
					{
						int nPages = pManager->m_wndTabControl.GetItemCount();
						for(int j=0; j<nPages;j++)
						{
							CXTPTabManagerItem *pSelectedItem = pManager->m_wndTabControl.GetItem(0);
							if(pSelectedItem)
							{
								pSelectedItem->Remove();
							}//if(pSelectedItem)
						}//for(int j=0; j<nPages;j++)
					}//if(pManager)
				}
				else if(i == TAB_SAMPLETREE)
				{
					CSampleTreeTabPageManager *pSampleTreeManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
					if(pSampleTreeManager)
					{
						int nPages = pSampleTreeManager->m_wndTabControl.GetItemCount();
						for(int j=0; j<nPages;j++)
						{
							CXTPTabManagerItem *pSampleTreeSelectedItem = pSampleTreeManager->m_wndTabControl.GetItem(0);
							if(pSampleTreeSelectedItem)
							{
								pSampleTreeSelectedItem->Remove();
							}//if(pSampleTreeSelectedItem)
						}//for(int j=0; j<nPages;j++)
					}//if(pSampleTreeManager)
				}
			}//if(pItem)
		}//for(int i=0; i<nTabPages; i++)

		m_vecSpecies.clear();
	}

	// remove plot tab
	m_wndTabControl.GetItem(nPlot)->SetHandle(0);
	m_wndTabControl.DeleteItem( m_wndTabControl.GetCurSel() );
}

void CStandTableDlg::OnSelChangingTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if(m_bDontLoadSave == TRUE)
	{
		*pResult = 1;
		return;
	}

	// which tab (plot) were we on?
	int iOldTab = m_wndTabControl.GetCurSel();

	// save the data connected to the plot, replacing current data
	if(iOldTab != -1 && iOldTab >= 0 && iOldTab <= m_wndTabControl.GetItemCount())
	{
		if(!SavePlotData(iOldTab))
		{
			// prevent user from changing tab
			*pResult = 1;
			return;
		}
	}

	// save tab selections aswell

	*pResult = 0;
}

void CStandTableDlg::OnSelChangeTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if(m_bDontLoadSave == TRUE)
	{
		*pResult = 1;
		return;
	}

	// which tab (plot) gained focus?
	int iNewTab = m_wndTabControl.GetCurSel();
	
	// load data connected to the plot
	if(iNewTab != -1 && iNewTab >= 0 && iNewTab <= m_wndTabControl.GetItemCount())
		LoadPlotData(iNewTab);

	// load tab selections and set focus accordingly


	*pResult = 0;
}

int CStandTableDlg::SavePlotData(int nPlot)
{
	DCLS dc;
	SPECIES sp;
	TREETYPE tt;
	PLOT pl;
	int nId = 0;
	vecMySampleTrees vecST;


	// get data from the tabs
	CXTPTabManagerItem *pTabItem = m_wndTabControl.GetItem(nPlot);	// sample plot
	if(!pTabItem) return FALSE;
	CTabTreeTypes *pTabTreeTypes = DYNAMIC_DOWNCAST(CTabTreeTypes, CWnd::FromHandle(pTabItem->GetHandle()));	// tree types
	if(!pTabTreeTypes) return FALSE;

	
	// remove old data
	pl = m_vecPlots[nPlot];
	for(int nLoop=0; nLoop<pl.vecTreeType.size(); nLoop++)
	{
		tt = pl.vecTreeType[nLoop];

		for(int nLoop2=0; nLoop2<tt.vecSpecies.size(); nLoop2++)
		{
			sp = tt.vecSpecies[nLoop2];
			sp.vecDcls.clear();
			sp.vecSampleTrees.clear();
		}

		tt.vecSpecies.clear();
	}
	pl.vecTreeType.clear();
	pl.nPlotNum = nPlot+1;


	// tree type tabs
	int nTabPages = pTabTreeTypes->m_wndTabControl.GetItemCount();
	for(int i=0; i<nTabPages; i++)
	{
		tt.vecSpecies.clear();

		CXTPTabManagerItem *pItem = pTabTreeTypes->m_wndTabControl.GetItem(i);
		if(pItem)
		{
			if(i != TAB_SAMPLETREE)	// cut or leave tab
			{
				CTabPageManager *pTabCutLeave = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
				if(pTabCutLeave)
				{
					int nPages = pTabCutLeave->m_wndTabControl.GetItemCount();
					for(int j=0; j<nPages; j++)	// species
					{
						CXTPTabManagerItem *pSpeciesItem = pTabCutLeave->m_wndTabControl.GetItem(j);
						if(pSpeciesItem)
						{
							// get diameter class
							CSpeciesView *pSpcView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pSpeciesItem->GetHandle()));
							if(!pSpcView)
								return FALSE;

							CXTPReportRecords *pRecs = pSpcView->m_wndReport.GetRecords();
							if(!pRecs)
								return FALSE;

							// clear diameter class struct
							sp.vecDcls.clear();
							sp.nSpeciesNum = pSpeciesItem->GetData();

							for(int k=0; k<pRecs->GetCount(); k++)
							{
								CSpeciesViewReportRec *pSpcRec = (CSpeciesViewReportRec*)pRecs->GetAt(k);
								if(!pSpcRec)
									return FALSE;

								// amount of trees
								int nSize = (int)pSpcRec->getColumnFloat(2);

								// save diameter class into the vector of species
								dc.nDclsNum = k;	// diameter class
								dc.nAmount = nSize;	// amount of trees

								sp.vecDcls.push_back(dc);
							}

							// add diameter classes to species
							tt.nTreeTypeNum = i;
							tt.vecSpecies.push_back(sp);
						}
					}
				}
			}
			else if(i == TAB_SAMPLETREE)	// sample tree tab
			{
				// clear tree type struct
				tt.vecSpecies.clear();
				tt.nTreeTypeNum = i;

				CSampleTreeTabPageManager *pSampleTreeManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
				if(pSampleTreeManager)
				{
					int nPages = pSampleTreeManager->m_wndTabControl.GetItemCount();
					for(int j=0; j<nPages; j++)	// species
					{
						CXTPTabManagerItem *pSampleTreeSelectedItem = pSampleTreeManager->m_wndTabControl.GetItem(j);
						if(pSampleTreeSelectedItem)
						{
//							if(pSampleTreeSelectedItem->GetData() == j+1)
//							{
								// clear species struct
								sp.vecDcls.clear();
								sp.vecSampleTrees.clear();
								sp.nSpeciesNum = pSampleTreeSelectedItem->GetData();

								// get sample trees
								CSampleTreeView *pView = DYNAMIC_DOWNCAST(CSampleTreeView, CWnd::FromHandle(pSampleTreeSelectedItem->GetHandle()));
								if(!pView)
									return FALSE;

								vecST.clear();
								if( pView->getSampleTree(vecST, (int)pItem->GetData()) == FALSE )
									return FALSE;

								for(int nLoop=0; nLoop<vecST.size(); nLoop++)
								{
									// add sample trees to vector in species
									sp.vecSampleTrees.push_back( vecST[nLoop] );
								}
//							}
						}

						// add species into vector in tree type
						tt.vecSpecies.push_back(sp);

					}	// species
				}
			}	// TAB_SAMPLETREE
		}

		pl.vecTreeType.push_back(tt);
	}	// tree type tab


	// erase the old sample plot
	m_vecPlots.erase( m_vecPlots.begin() + nPlot );

	// save plot area
	pl.nAreal = _wtoi(getYtarea());

	// insert the plot data into the vector
	m_vecPlots.insert( m_vecPlots.begin() + nPlot, pl );


	return TRUE;
}

int CStandTableDlg::LoadPlotData(int nPlot)
{
	CXTPTabManagerItem *pTabItem = m_wndTabControl.GetItem(nPlot);	// sample plot
	if(!pTabItem) return FALSE;
	CTabTreeTypes *pTabTreeTypes = DYNAMIC_DOWNCAST(CTabTreeTypes, CWnd::FromHandle(pTabItem->GetHandle()));	// tree types
	if(!pTabTreeTypes) return FALSE;

	DCLS dc;
	CMyTransactionSampleTree st;
	SPECIES sp;
	TREETYPE tt;
	PLOT pl;

	if(m_vecPlots.size() == 0) return FALSE;
	pl = m_vecPlots.at( nPlot );

	
	// set plot area
	CString csBuf;
	csBuf.Format(_T("%d"), pl.nAreal);
	setYtarea(csBuf);

	// tree type tabs
	int nTabPages = pTabTreeTypes->m_wndTabControl.GetItemCount();
	for(int i=0; i<nTabPages; i++)
	{
		// get tree type struct from plot vector
		if(pl.vecTreeType.size() > 0)
			tt = pl.vecTreeType[i];

		CXTPTabManagerItem *pItem = pTabTreeTypes->m_wndTabControl.GetItem(i);
		if(pItem)
		{
			if(i != TAB_SAMPLETREE)	// cut or leave tab
			{
				CTabPageManager *pTabCutLeave = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
				if(pTabCutLeave)
				{
					int nPages = pTabCutLeave->m_wndTabControl.GetItemCount();
					for(int j=0; j<nPages; j++)	// species
					{
						// get species struct from tree type vector
						if(tt.vecSpecies.size() > 0)
							sp = tt.vecSpecies[j];

						CXTPTabManagerItem *pSpeciesItem = pTabCutLeave->m_wndTabControl.GetItem(j);
						if(pSpeciesItem)
						{
							// diameter class view
							CSpeciesView *pSpcView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pSpeciesItem->GetHandle()));
							if(!pSpcView)
								return FALSE;

							// remove all diameter classes in view
							pSpcView->removeAllItems();

							// setup a empty diameter class table
							pSpcView->setupDclsTable(2.0);

							CXTPReportRecords *pRecs = pSpcView->m_wndReport.GetRecords();
							if(!pRecs)
								return FALSE;

							if(sp.vecDcls.size() > 0)
							{
								for(int k=0; k<sp.vecDcls.size(); k++)
								{
									// get diameter class struct from species vector
									dc = sp.vecDcls[k];

									CSpeciesViewReportRec *pSpcRec = (CSpeciesViewReportRec*)pRecs->GetAt(k);
									if(!pSpcRec)	// no record, add a new row
									{
										pSpcView->OnClickAddDcls(0, 0);
										pSpcRec = (CSpeciesViewReportRec*)pRecs->GetAt(k);
									}

									// set amount of trees, can not set it to zero though, causes a crash when clearing the report control
									if(pSpcRec && dc.nAmount > 0)
										pSpcRec->setColumnFloat(2, (float)dc.nAmount);
								}
							}
						}
					}	// species
				}
			}	// i != TAB_SAMPLETREE
			else if(i == TAB_SAMPLETREE)	// sample tree tab
			{
				CSampleTreeTabPageManager *pSampleTreeManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
				if(pSampleTreeManager)
				{
					int nPages = pSampleTreeManager->m_wndTabControl.GetItemCount();
					for(int j=0; j<nPages; j++)	// species
					{
						// get species struct from tree type vector
						if(tt.vecSpecies.size() > 0)
							sp = tt.vecSpecies[j];

						CXTPTabManagerItem *pSampleTreeSelectedItem = pSampleTreeManager->m_wndTabControl.GetItem(j);
						if(pSampleTreeSelectedItem)
						{
							// sample tree view
							CSampleTreeView *pView = DYNAMIC_DOWNCAST(CSampleTreeView, CWnd::FromHandle(pSampleTreeSelectedItem->GetHandle()));
							if(!pView)
								return FALSE;

							// remove all sample trees first
							pView->removeAllItems();

							// add the trees in the vector
							for(int nLoop=0; nLoop<sp.vecSampleTrees.size(); nLoop++)
							{
								st = sp.vecSampleTrees[nLoop];
								pView->addTree(&st);
							}

							// add the last extra row for creating a new sample tree
							pView->addEmptyRow();
						}
					}	// species
				}
			}	// i == TAB_SAMPLETREE
		}
	}	// tree types tabs

	return TRUE;
}

void CStandTableDlg::OnBnClickedButtonAdd()
{
	UpdateData(TRUE);

	// display a popup-menu with the different alternatives
	POINT point;
	CMenu menuPopup;
	menuPopup.CreatePopupMenu();

	// add selections to the popup menu
	if(m_vecPlots.size() > 0)
		menuPopup.AppendMenu(MF_STRING, 1, (LPCTSTR)theApp.m_XML->str(124));	// add species
	else
		menuPopup.AppendMenu(MF_STRING|MF_DISABLED|MF_GRAYED, 1, (LPCTSTR)theApp.m_XML->str(124));	// add species

	if(m_nInventeringsMetod == 0 || ( m_vecPlots.size() == 0 && m_nInventeringsMetod != -1) )
		menuPopup.AppendMenu(MF_STRING, 2, (LPCTSTR)theApp.m_XML->str(35));	// add plot
	else
		menuPopup.AppendMenu(MF_STRING|MF_DISABLED|MF_GRAYED, 2, (LPCTSTR)theApp.m_XML->str(35));	// add plot
		
/*		if(isSampleTreesSelected() == TRUE)
		menuPopup.AppendMenu(MF_STRING, 3, (LPCTSTR)(LPCTSTR)theApp.m_XML->str(39));	// sample tree add
	else
		menuPopup.AppendMenu(MF_STRING|MF_DISABLED|MF_GRAYED, 3, (LPCTSTR)(LPCTSTR)theApp.m_XML->str(39));	// sample tree add
*/

	GetCursorPos(&point);
	int nRet = ::TrackPopupMenu(menuPopup.GetSafeHmenu(), TPM_NONOTIFY|TPM_RETURNCMD|TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, NULL, this->GetSafeHwnd(), NULL);
	menuPopup.DestroyMenu();

	if(nRet == 0)	// no selection
	{
		return;
	}
	else if(nRet == 1)	// species add
	{
		OnBnClickedButtonSpeciesAdd();
	}
	else if(nRet == 2)	// plot add
	{
		OnBnClickedButtonPlotAdd();
	}
	else if(nRet == 3)	// sample tree add
	{
	}
}

void CStandTableDlg::OnBnClickedButtonDel()
{
	UpdateData(TRUE);

	// display a popup-menu with the different alternatives
	POINT point;
	CMenu menuPopup;
	menuPopup.CreatePopupMenu();

	// add selections to the popup menu
	if(m_vecSpecies.size() > 0)
		menuPopup.AppendMenu(MF_STRING, 1, (LPCTSTR)theApp.m_XML->str(125));	// del species
	else
		menuPopup.AppendMenu(MF_STRING|MF_DISABLED|MF_GRAYED, 1, (LPCTSTR)theApp.m_XML->str(125));	// del species

	if(/*m_nInventeringsMetod == 0 &&*/ m_vecPlots.size() > 0)
		menuPopup.AppendMenu(MF_STRING, 2, (LPCTSTR)theApp.m_XML->str(36));	// del plot
	else
		menuPopup.AppendMenu(MF_STRING|MF_DISABLED|MF_GRAYED, 2, (LPCTSTR)theApp.m_XML->str(36));	// del plot

	if(isSampleTreesSelected() == TRUE)
		menuPopup.AppendMenu(MF_STRING, 3, (LPCTSTR)(LPCTSTR)theApp.m_XML->str(40));	// sample tree del
	else
		menuPopup.AppendMenu(MF_STRING|MF_DISABLED|MF_GRAYED, 3, (LPCTSTR)(LPCTSTR)theApp.m_XML->str(40));	// sample tree del


	GetCursorPos(&point);
	int nRet = ::TrackPopupMenu(menuPopup.GetSafeHmenu(), TPM_NONOTIFY|TPM_RETURNCMD|TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, NULL, this->GetSafeHwnd(), NULL);
	menuPopup.DestroyMenu();

	if(nRet == 0)	// no selection
	{
		return;
	}
	else if(nRet == 1)	// species del
	{
		OnBnClickedButtonSpeciesDel();
	}
	else if(nRet == 2)	// plot del
	{
		OnBnClickedButtonPlotDel();
	}
	else if(nRet == 3)	// sample tree del
	{
		DeleteSampleTree();
	}
}

int CStandTableDlg::isSampleTreesSelected()
{
	CXTPTabManagerItem *pTabItem = m_wndTabControl.GetItem( m_wndTabControl.GetCurSel() );	// sample plots
	if(!pTabItem) return FALSE;
	CTabTreeTypes *pTabTreeTypes = DYNAMIC_DOWNCAST(CTabTreeTypes, CWnd::FromHandle(pTabItem->GetHandle()));	// tree types
	if(!pTabTreeTypes) return FALSE;
	CXTPTabManagerItem *pActTab = pTabTreeTypes->m_wndTabControl.GetItem( pTabTreeTypes->m_wndTabControl.GetCurSel() );
	if(pActTab)
	{
		if(pActTab->GetIndex() == TAB_SAMPLETREE)
			return TRUE;
	}

	return FALSE;
}

int CStandTableDlg::DeleteSampleTree()
{
	CXTPTabManagerItem *pTabItem = m_wndTabControl.GetItem( m_wndTabControl.GetCurSel() );	// sample plots
	CTabTreeTypes *pTabTreeTypes = DYNAMIC_DOWNCAST(CTabTreeTypes, CWnd::FromHandle(pTabItem->GetHandle()));	// tree types
	CXTPTabManagerItem *pActTab = pTabTreeTypes->m_wndTabControl.GetItem( pTabTreeTypes->m_wndTabControl.GetCurSel() );

	if(pActTab)
	{
		if(pActTab->GetIndex() == TAB_SAMPLETREE)
		{
			CSampleTreeTabPageManager *pSampleTreeActManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pActTab->GetHandle()));
			if(pSampleTreeActManager)
			{
				if(pSampleTreeActManager->m_wndTabControl.GetItemCount() <= 0)
					return FALSE;

				pSampleTreeActManager->deleteSampleTree();
			}
		}
		else
		{
			return FALSE;
		}
	}

	return TRUE;
}

LRESULT CStandTableDlg::OnTimer(WPARAM wParam, LPARAM lParam)
{
	// do we have a valid license?
	if(!_CheckLicense())
	{
		// no valid license
		AfxMessageBox( GetLastMessage() );
	}

	return 0;
}
