// NorrClient.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NorrClient.h"
#include "StandTableDlg.h"
#include "WizardSheet.h"
#include "HXLParser.h"
#include "../NorrServer/Intermediate/NorrServer_h.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#pragma managed(push, off)

//
//TODO: If this DLL is dynamically linked against the MFC DLLs,
//		any functions exported from this DLL which call into
//		MFC must have the AFX_MANAGE_STATE macro added at the
//		very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

// CNorrClientApp

BEGIN_MESSAGE_MAP(CNorrClientApp, CWinApp)
END_MESSAGE_MAP()


// CNorrClientApp construction

CNorrClientApp::CNorrClientApp():
m_szStringBinding(NULL)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

CNorrClientApp::~CNorrClientApp()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(m_XML != NULL)
		delete m_XML;
}

// The one and only CNorrClientApp object

CNorrClientApp theApp;


// CNorrClientApp initialization

BOOL CNorrClientApp::InitInstance()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CWinApp::InitInstance();


	// ladda in spr�kfil
	m_XML = new RLFReader;
	TCHAR szPath[2048];
	if( GetModuleFileName( NULL, szPath, 2048 ) )
	{
		m_sLangFN = szPath;
		m_sLangFN = m_sLangFN.Left(m_sLangFN.ReverseFind('\\'));
		m_sLangFN += _T("\\NorrClientSVE.xml");
		//m_sLangFN = _T("C:\\Sources\\w32\\vC\\HMS\\NorrClient\\Release\\NorrClientSVE.xml");

		if(!m_XML->Load(m_sLangFN))
		{
			AfxMessageBox(_T("Kunde inte �ppna spr�kfil!"));
			return FALSE;
		}
	}

	
	// get service settings from registry
	TCHAR tzHostname[250], tzPort[20];
	CString csHostname = regGetStr_LM(_T("SOFTWARE"), _T("NorrClient"), _T("Hostname"), _T("localhost"));
	int nPort =  regGetInt_LM(_T("SOFTWARE"), _T("NorrClient"), _T("Port"), 4747);
	_stprintf(tzHostname, L"%s", csHostname);
	_stprintf(tzPort, L"%d", nPort);

	// Init RPC
	RPC_STATUS status;

	// Create a string binding handle.
	status = RpcStringBindingCompose(
		NULL,										// UUID to bind to.
		reinterpret_cast<RPC_WSTR>(L"ncacn_ip_tcp"),// Use TCP/IP protocol.
		reinterpret_cast<RPC_WSTR>(tzHostname),	// TCP/IP network address to use.
		reinterpret_cast<RPC_WSTR>(tzPort),		// TCP/IP port to use.
		NULL,										// Protocol dependent network options to use.
		&m_szStringBinding);						// String binding output.
	if( status )
	{
		AfxMessageBox(_T("RpcStringBindingCompose failed!"));
		return FALSE;
	}

	// Validate the format of the string binding handle and convert it to a binding handle.
	//	RPC_BINDING_HANDLE hServerBinding;
	status = RpcBindingFromStringBinding(m_szStringBinding, &hServerBinding);
	if( status )
	{
		AfxMessageBox(_T("RpcBindingFromStringBinding failed!"));
		RpcStringFree(&m_szStringBinding);
		return FALSE;
	}


	return TRUE;
}

int CNorrClientApp::ExitInstance()
{
 	// release the string allocated
	if( m_szStringBinding )
	{
		RpcStringFree(&m_szStringBinding);
		m_szStringBinding = NULL;
	}

	// Release binding handle resources and disconnect from the server.
	if( hServerBinding )
	{
		RpcBindingFree(&hServerBinding);
		hServerBinding = NULL;
	}

	return CWinApp::ExitInstance();
}


// Memory allocation function for RPC.
void* __RPC_USER midl_user_allocate(size_t size)
{
    return malloc(size);
}

// Memory deallocation function for RPC.
void __RPC_USER midl_user_free(void* p)
{
    free(p);
}

#pragma managed(pop)
