#include "stdafx.h"
#include "RpcProcedures.h"
#include "../NorrServer/Intermediate/NorrServer_h.h"


void DisplayException(int errcode)
{
	CString str; str.Format(_T("Ett RPC-anrop misslyckades!\nFelkod %d"), errcode);
	AfxMessageBox(str);
}


boolean _CreateStand(const char* pData, const wchar_t* pTemplateXml, const wchar_t* pUsername, int* nStandId)
{
	RpcTryExcept
	{
		return CreateStand(pData, pTemplateXml, pUsername, nStandId);
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return false;
	}
	RpcEndExcept
}

boolean _CalculateStand(int nStandId)
{
	RpcTryExcept
	{
		return CalculateStand(nStandId);
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return false;
	}
	RpcEndExcept
}

boolean _ChangeTemplate(const int nStandId, const char *pTemplateXml)
{
	RpcTryExcept
	{
		return ChangeTemplate(nStandId, pTemplateXml);
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return false;
	}
	RpcEndExcept
}

boolean _GetStandArray(int nDesiredSize, int* nActualSize, struct STAND* pData)
{
	RpcTryExcept
	{
		return GetStandArray(nDesiredSize, nActualSize, pData);
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return false;
	}
	RpcEndExcept
}

boolean _GetStandCount(int* nCount)
{
	RpcTryExcept
	{
		return GetStandCount(nCount);
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return false;
	}
	RpcEndExcept
}

boolean _GetTemplateArray(int nDesiredSize, int* nActualSize, struct TEMPLATE* pData)
{
	RpcTryExcept
	{
		return GetTemplateArray(nDesiredSize, nActualSize, pData);
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return false;
	}
	RpcEndExcept
}

boolean _GetTemplateCount(int* nCount)
{
	RpcTryExcept
	{
		return GetTemplateCount(nCount);
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return false;
	}
	RpcEndExcept
}

boolean _GetTemplateXml(int nTemplateId, int nDesiredSize, int* nActualSize, wchar_t* pData)
{
	RpcTryExcept
	{
		return GetTemplateXml(nTemplateId, nDesiredSize, nActualSize, pData);
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return false;
	}
	RpcEndExcept
}

boolean _GetPricelistXml(int nPricelistId, int nDesiredSize, int* nActualSize, wchar_t* pData)
{
	RpcTryExcept
	{
		return GetPricelistXml(nPricelistId, nDesiredSize, nActualSize, pData);
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return false;
	}
	RpcEndExcept
}

boolean _GetStandData(int nStandId, struct STANDDATA* pData)
{
	RpcTryExcept
	{
		return GetStandData(nStandId, pData);
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return false;
	}
	RpcEndExcept
}

boolean _RemoveStand(int nStandId)
{
	RpcTryExcept
	{
		return RemoveStand(nStandId);
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return false;
	}
	RpcEndExcept
}

wchar_t* _GetLastMessage()
{
	RpcTryExcept
	{
		return GetLastMessage();
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return NULL;
	}
	RpcEndExcept
}


boolean _CheckLicense()
{
	RpcTryExcept
	{
		return CheckLicence(getUserId());
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return false;
	}
	RpcEndExcept
}
