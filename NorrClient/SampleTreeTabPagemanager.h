#pragma once
#include "stdafx.h"
#include "Resource.h"
// #include "MDIStandTableFrame.h"
#include "MyTabControl.h"


// CSampleTreeTabPageManager form view

class CSampleTreeTabPageManager : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSampleTreeTabPageManager)

	CString m_sAbrevLangSet;
	CString m_sLangFN;

	//CFont m_fntTab;

public:
	/*CMyTabControl*/ CXTPTabControl m_wndTabControl;	
	

protected:
	CSampleTreeTabPageManager();           // protected constructor used by dynamic creation
	virtual ~CSampleTreeTabPageManager();

public:
	enum { IDD = IDD_TABPAGEMANAGER };

//	CMDIFrameDoc* GetDocument();
	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle,int spc_id, int nIcon);
	void setupDclsTables(int dcls);
	void deleteSampleTree(void);
	BOOL getSampleTrees(vecMySampleTrees &vecST);
	BOOL isOkToClose(void);
	void resetDataChanged(void);
	void addTree(CMyTransactionSampleTree *tree);

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


