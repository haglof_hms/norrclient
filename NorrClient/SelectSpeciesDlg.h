#pragma once

#include "Resource.h"
#include "MyTabControl.h"

// CSelectSpeciesDlg dialog

class CSelectSpeciesDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectSpeciesDlg)

	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_csDBErrorMsg;

	vecTransactionSpecies m_vecSpecies;
	vecTransactionSpecies m_vecSpeciesSelected;
	vecInt m_vecSpeciesUsed;

protected:
	CXTListCtrl	m_wndListCtrl;
	CXTHeaderCtrl m_wndHeaderCtrl;
	CMyExtStatic m_wndLbl1;

	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;

public:
	CSelectSpeciesDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectSpeciesDlg();

// Dialog Data
	enum { IDD = IDD_SELECTSPECIES };
	vecTransactionSpecies getSpeciesSelected(void){ return m_vecSpeciesSelected;}
	void setSpeciesUsed(int spc_id) {m_vecSpeciesUsed.push_back((int)spc_id);}
	

protected:
	void setListCtrl(void);
	void setLanguage(void);
	BOOL isSpecieUsed(int spc_id);
	CTransaction_species* getSpecie(int spc_id);
	

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnBnClickedOk();
};
