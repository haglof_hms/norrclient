#include "StdAfx.h"
#include "TemplateParser.h"

CTemplateParser::CTemplateParser():
m_nPrlId(-1),
m_nCostId(-1),
m_nLatitude(0),
m_nAltitude(0),
m_nAtCoast(0),
m_nSouthEast(0),
m_nRegion5(0),
m_nPartOfPlot(0),
m_pPrinter(NULL)
{
}

CTemplateParser::~CTemplateParser()
{
	m_doc.Clear();
	if( m_pPrinter ) delete m_pPrinter;
}

BOOL CTemplateParser::ParseXml(LPCSTR xml)
{
	return _ParseXml(xml);
}

BOOL CTemplateParser::UpdateXml(LPCSTR* pStr)
{
	return _ParseXml(NULL, pStr);
}

BOOL CTemplateParser::_ParseXml(LPCSTR xml/*= NULL*/, LPCSTR *out/*= NULL*/)
{
	POSITION pos;
	SPECIE spc, val;
	TiXmlNode *pRoot;
	TiXmlElement *pElem, *pChild;

	// Clear values
	m_nPrlId = -1;
	spc.id = -1;
	if( !out ) m_species.RemoveAll();
	m_nLatitude = -1;
	m_nAltitude = -1;
	m_csSI = _T("");


	// Parse template
	if( !out ) m_doc.Clear();
	m_doc.Parse(xml, NULL, TIXML_ENCODING_LEGACY); // Legacy encoding, this make sure ��� won't break the parsing

	if( !(pRoot = m_doc.FirstChild("template")) )
		return FALSE;

	// Associated pricelist, cost template
	pElem = pRoot->FirstChildElement("template_pricelist");
	pElem->Attribute("id", &m_nPrlId);

	pElem = pRoot->FirstChildElement("template_costs_tmpl");
	pElem->Attribute("id", &m_nCostId);

	// Stand settings
	pElem = pRoot->FirstChildElement("template_dcls");
	if( !pElem ) return FALSE;
	m_fDcls = localeStrToDbl(pElem->GetText());

	pElem = pRoot->FirstChildElement("template_hgt_over_sea");
	if( !pElem ) return FALSE;
	m_nAltitude = atoi(pElem->GetText());

	pElem = pRoot->FirstChildElement("template_latitude");
	if( !pElem ) return FALSE;
	m_nLatitude = atoi(pElem->GetText());

	pElem = pRoot->FirstChildElement("template_si_h100");
	if( !pElem ) return FALSE;
	m_csSI = pElem->GetText();

	pElem = pRoot->FirstChildElement("template_at_coast");
	if( !pElem ) return FALSE;
	m_nAtCoast = atoi(pElem->GetText());

	pElem = pRoot->FirstChildElement("template_south_east");
	if( !pElem ) return FALSE;
	m_nSouthEast = atoi(pElem->GetText());

	pElem = pRoot->FirstChildElement("template_region_5");
	if( !pElem ) return FALSE;
	m_nRegion5 = atoi(pElem->GetText());

	pElem = pRoot->FirstChildElement("template_part_of_plot");
	if( !pElem ) return FALSE;
	m_nPartOfPlot = atoi(pElem->GetText());

	pElem = pRoot->FirstChildElement("template_si_h100_pine");
	if( !pElem ) return FALSE;
	m_csH100Pine = pElem->GetText();



	// Species
	pElem = pRoot->FirstChildElement("template_specie");
	while( pElem )
	{
		pElem->Attribute("id", &spc.id);
		spc.name = pElem->Attribute("name");

		pChild = pElem->FirstChildElement("template_specie_hgt");
		if( !pChild ) return FALSE;
		pChild->Attribute("id", &spc.hgtId);
		pChild->Attribute("spc_id", &spc.hgtSpcId);
		pChild->Attribute("index", &spc.hgtIdx);

		pChild = pElem->FirstChildElement("template_specie_vol");
		if( !pChild ) return FALSE;
		pChild->Attribute("id", &spc.volId);
		pChild->Attribute("spc_id", &spc.volSpcId);
		pChild->Attribute("index", &spc.volIdx);

		pChild = pElem->FirstChildElement("template_specie_bark");
		if( !pChild ) return FALSE;
		pChild->Attribute("id", &spc.barkId);
		pChild->Attribute("spc_id", &spc.barkSpcId);
		pChild->Attribute("index", &spc.barkIdx);

		pChild = pElem->FirstChildElement("template_specie_vol_ub");
		if( !pChild ) return FALSE;
		pChild->Attribute("id", &spc.ubId);
		pChild->Attribute("spc_id", &spc.ubSpcId);
		pChild->Attribute("index", &spc.ubIdx);
		pChild->Attribute("m3sk_m3ub", &spc.sk2ub);

		pChild = pElem->FirstChildElement("template_specie_qdesc");
		if( !pChild ) return FALSE;
		if( out )
		{
			// Update xml with new quality id
			pos = m_species.GetHeadPosition();
			while( pos )
			{
				val = m_species.GetNext(pos);
				if( val.id == spc.id )
				{
					pChild->SetAttribute("id", val.qualityId);
				}
			}
		}
		pChild->Attribute("id", &spc.qualityId);
		spc.qualityName = pChild->Attribute("qdesc_name");

		pChild = pElem->FirstChildElement("template_specie_grot");
		if( !pChild ) return FALSE;
		pChild->Attribute("id", &spc.grotId);
		pChild->Attribute("percent", &spc.grotPercent);
		pChild->Attribute("price", &spc.grotPrice);
		pChild->Attribute("cost", &spc.grotCost);

		pChild = pElem->FirstChildElement("template_transp_dist1");
		if( !pChild ) return FALSE;
		spc.dist1 = atoi(pChild->GetText());

		pChild = pElem->FirstChildElement("template_transp_dist2");
		if( !pChild ) return FALSE;
		spc.dist2 = atoi(pChild->GetText());

		pChild = pElem->FirstChildElement("template_specie_sk_to_fub");
		if( !pChild ) return FALSE;
		spc.sk2fub = localeStrToDbl(pChild->GetText());

		pChild = pElem->FirstChildElement("template_specie_fub_to_to");
		if( !pChild ) return FALSE;
		spc.fub2to = localeStrToDbl(pChild->GetText());

		pChild = pElem->FirstChildElement("template_specie_h25");
		if( !pChild ) return FALSE;
		spc.h25 = localeStrToDbl(pChild->GetText());

		pChild = pElem->FirstChildElement("template_specie_gcrown");
		if( !pChild ) return FALSE;
		spc.gcrown = atoi(pChild->GetText());

		if( !out )
		{
			// Add to list
			m_species.AddTail(spc);
		}

		pElem = pElem->NextSiblingElement("template_specie");
	}

	// Output new xml when updating
	if( out )
	{
		// There is no way to clear a printer so we need to re-create it (otherwise previous xml will remain in the output string)
		if( m_pPrinter ) delete m_pPrinter;
		m_pPrinter = new TiXmlPrinter;

		m_doc.Accept(m_pPrinter);
		*out = m_pPrinter->CStr();
	}

	return TRUE;
}

double localeStrToDbl(LPCSTR value)
{
	char szDecPoint[2];
	GetLocaleInfoA(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, szDecPoint, sizeof(szDecPoint));

	CStringA sDecPnt(szDecPoint);
	CStringA sValue(value);
	if (sValue.Find(",") && sDecPnt == ".")	sValue.Replace(",",sDecPnt);
	else if (sValue.Find(".") && sDecPnt == ",")	sValue.Replace(".",sDecPnt);

	return atof(sValue);
}
