#include "stdafx.h"
#include "NorrClient.h"
#include "HXLParser.h"
#include "tinyxml.h"
#include <math.h>

CHXLParser::CHXLParser()
{
	m_pPrinter = NULL;
}

CHXLParser::~CHXLParser()
{
	m_doc.Clear();
	if( m_pPrinter ) delete m_pPrinter;
}

int CHXLParser::getHeaderData(CString csFilename, HXLHEADER *pHxlHeader)
{
	if( Open(csFilename) )
		return getHeaderData(pHxlHeader);
	else
		return FALSE;
}

int CHXLParser::getHeaderData(HXLHEADER *pHxlHeader)
{
	TiXmlElement *pElem;
	TiXmlNode *pObjectNode, *pRootNode, *pNode, *pTextNode, *pNode2, *pObjectDataNode;
	char szTmp[1024], szTmpSpecies[1024], szTmpTree[8192];
	const char *pTag, *pValue;
	HXLDATA hxldata;
	CArray<HXLDATA, HXLDATA> caTreeVars;

	// reset variables
	pHxlHeader->nAreal = 0;
	pHxlHeader->nBestAlder = 0;
	pHxlHeader->nDelbestand = 0;
	pHxlHeader->nAltitud = 0;
	pHxlHeader->nLatitud = 0;
	pHxlHeader->nNumPlots = 0;
	pHxlHeader->nNumSampleTrees = 0;
	pHxlHeader->nNumTrees = 0;
	pHxlHeader->csSI = _T("");
	pHxlHeader->csDatum = _T("");
	pHxlHeader->nInventeringsmetod = 0;
	caTreeVars.RemoveAll();

	pHxlHeader->csFilePath = m_csFilePath;

	pRootNode = m_doc.FirstChild("HXL");
	pObjectNode = pRootNode->FirstChild("Object");
	pObjectDataNode = pObjectNode->FirstChild("objectData");

	// Best�nd
	pNode = pObjectDataNode->FirstChild("Best");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode)
		{
			pHxlHeader->csBestand = pTextNode->Value();
			int nPos = pHxlHeader->csBestand.Find('_');

			if(nPos != -1)
			{
				pHxlHeader->nDelbestand = _wtoi( pHxlHeader->csBestand.Right( pHxlHeader->csBestand.GetLength() - nPos - 1 ));
				pHxlHeader->csBestand = pHxlHeader->csBestand.Left(nPos);
			}
		}
	}

	// Datum
	pNode = pObjectDataNode->FirstChild("Date");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode)
		{
			pHxlHeader->csDatum = pTextNode->Value();
			pHxlHeader->csDatum = pHxlHeader->csDatum.Left(8);	// strip away the time part since it always is zero anyway
		}
	}

	// Areal
	pNode = pObjectDataNode->FirstChild("Areal");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode)
		{
			pHxlHeader->nAreal = atoi(pTextNode->Value());	// m2
		}
	}

	// Altitud
	pNode = pObjectDataNode->FirstChild("HgtAboveSea");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode)
		{
			pHxlHeader->nAltitud = atoi(pTextNode->Value());
		}
	}


	// Latitude
	pNode = pObjectDataNode->FirstChild("Latitude");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode)
		{
			pHxlHeader->nLatitud = atoi(pTextNode->Value())/10;
		}
	}

	// Inventeringsmetod
	pNode = pObjectDataNode->FirstChild("Metod");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode)
		{
			pHxlHeader->nInventeringsmetod = atoi(pTextNode->Value());
		}
	}

	// Best�nds�lder
	pNode = pObjectDataNode->FirstChild("BestAge");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode)
		{
			pHxlHeader->nBestAlder = atoi(pTextNode->Value());
		}
	}


	// Bonitet (SI)
	pNode = pObjectDataNode->FirstChild("Bonitet");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode)
		{
			pHxlHeader->csSI = pTextNode->Value();
			pHxlHeader->csSI = pHxlHeader->csSI.Left(3);
			if(pHxlHeader->csSI.GetAt(0) == '1') pHxlHeader->csSI.SetAt(0, 'T');	// Tall
			else if(pHxlHeader->csSI.GetAt(0) == '2') pHxlHeader->csSI.SetAt(0, 'G');	// Gran
			else if(pHxlHeader->csSI.GetAt(0) == '3') pHxlHeader->csSI.SetAt(0, 'B');	// Bj�rk
			else if(pHxlHeader->csSI.GetAt(0) == '6') pHxlHeader->csSI.SetAt(0, 'C');	// Contorta
		}
	}
	else
	{
		pNode = pObjectDataNode->FirstChild("Bonitet2");
		if(pNode)	// exitsting tag
		{
			pTextNode = pNode->FirstChild();
			if(pTextNode)
			{
				pHxlHeader->csSI = pTextNode->Value();
				pHxlHeader->csSI = pHxlHeader->csSI.Left(3);
			}
		}
	}

	// Antal provytor
	pNode = pObjectDataNode->FirstChild("Numplots");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode)
		{
			pHxlHeader->nNumPlots = atoi(pTextNode->Value());
		}
	}


	// Antal tr�d och provtr�d
	pNode = pObjectNode->FirstChild("TreeSet");
	if(pNode)	// exitsting tag
	{
		TiXmlElement* pTreeNode = pNode->FirstChildElement("TreeVar");
		while( pTreeNode )
		{
			// get the HXL attributes for the node
			getHxlAttributes(pTreeNode, &hxldata);
			caTreeVars.Add(hxldata);

			pTreeNode = pTreeNode->NextSiblingElement("TreeVar");
		}

		pNode = pNode->FirstChild("Tree");
		if(pNode)	// exitsting tag
		{
			pTextNode = pNode->FirstChild();
			if(pTextNode)
			{
				CString csTrees;
				CString csLine, csValue;
				int nCurPos = 0, nCurPos2 = 0, nValue = 0;
	
				csTrees.Format(_T("%S"), pTextNode->Value());

				csLine = csTrees.Tokenize(_T("\t\n@"), nCurPos);
				while (csLine != _T(""))
				{
					nCurPos2 = 0;
					nValue = 0;
					csValue = csLine.Tokenize(_T(","), nCurPos2);
					while (csValue != _T(""))
					{
						hxldata = caTreeVars.GetAt(nValue);
						if(hxldata.csName == _T("HGTNUM") && hxldata.nVar == 654 && hxldata.nType == 1)
						{
							if(csValue == _T("0"))
								pHxlHeader->nNumTrees++;
							else
								pHxlHeader->nNumSampleTrees++;
						}

						csValue = csLine.Tokenize(_T(","), nCurPos2);
						nValue++;
					};

					csLine = csTrees.Tokenize(_T("\t\n@"), nCurPos);
				};

			}
		}
	}


	return 0;
}

int CHXLParser::setHeaderData(CString csFilename, HXLHEADER *pHeader)
{
	if( Open(csFilename) )
		return setHeaderData(pHeader);
	else
		return -1;
}

int CHXLParser::setHeaderData(HXLHEADER *pHeader)
{
	// open HXL file for reading/writing
	TiXmlElement *pElem;
	TiXmlNode *pObjectNode, *pRootNode, *pNode, *pTextNode, *pNode2, *pObjectDataNode;
	char szTmp[1024], szTmpSpecies[1024], szTmpTree[8192];
	const char *pTag, *pValue;


	// change header variables
	pRootNode = m_doc.FirstChild("HXL");
	pObjectNode = pRootNode->FirstChild("Object");
	pObjectDataNode = pObjectNode->FirstChild("objectData");

	// create the default attributes
	TiXmlAttribute *aAttributes[3];
	aAttributes[0] = new TiXmlAttribute();
	aAttributes[0]->SetName("Name");
	aAttributes[0]->SetValue("1");
	aAttributes[1] = new TiXmlAttribute();
	aAttributes[1]->SetName("Var");
	aAttributes[1]->SetValue("2");
	aAttributes[2] = new TiXmlAttribute();
	aAttributes[2]->SetName("Type");
	aAttributes[2]->SetValue("3");

	TiXmlAttributeSet *pAttributes = new TiXmlAttributeSet;
	pAttributes->Add(aAttributes[0]);
	pAttributes->Add(aAttributes[1]);
	pAttributes->Add(aAttributes[2]);


	// Best�ndsnamn
	if(pHeader->nDelbestand > 0)
		sprintf(szTmp, "%S_%d", pHeader->csBestand, pHeader->nDelbestand);
	else
		sprintf(szTmp, "%S", pHeader->csBestand);

	pNode = pObjectDataNode->FirstChild("Best");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode) pTextNode->SetValue(szTmp);
	}
	else	// create a new tag
	{
		aAttributes[0]->SetValue("STANDID");
		aAttributes[1]->SetValue("2");
		aAttributes[2]->SetValue("1");
		InsertTag(pObjectDataNode, "Best", pAttributes, szTmp);
	}


	// Datum
	sprintf(szTmp, "%S000000", pHeader->csDatum);

	pNode = pObjectDataNode->FirstChild("Date");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode) pTextNode->SetValue(szTmp);
	}
	else	// create a new tag
	{
		aAttributes[0]->SetValue("DATE");
		aAttributes[1]->SetValue("12");
		aAttributes[2]->SetValue("4");
		InsertTag(pObjectDataNode, "Date", pAttributes, szTmp);
	}


	// �lder
	sprintf(szTmp, "%d", pHeader->nBestAlder);

	pNode = pObjectDataNode->FirstChild("BestAge");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode) pTextNode->SetValue(szTmp);
	}
	else	// create a new tag
	{
		aAttributes[0]->SetValue("BESTAGE");
		aAttributes[1]->SetValue("660");
		aAttributes[2]->SetValue("1");
		InsertTag(pObjectDataNode, "BestAge", pAttributes, szTmp);
	}


	// Areal
	sprintf(szTmp, "%d", pHeader->nAreal);

	pNode = pObjectDataNode->FirstChild("Areal");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode) pTextNode->SetValue(szTmp);
	}
	else	// create a new tag
	{
		aAttributes[0]->SetValue("AREAL");
		aAttributes[1]->SetValue("670");
		aAttributes[2]->SetValue("1");
		InsertTag(pObjectDataNode, "Areal", pAttributes, szTmp);
	}


	// Latitude
	sprintf(szTmp, "%d", pHeader->nLatitud*10);

	pNode = pObjectDataNode->FirstChild("Latitude");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode) pTextNode->SetValue(szTmp);
	}
	else	// create a new tag
	{
		aAttributes[0]->SetValue("LATITUDE");
		aAttributes[1]->SetValue("2011");
		aAttributes[2]->SetValue("2");
		InsertTag(pObjectDataNode, "Latitude", pAttributes, szTmp);
	}


	// H�jd �ver havet
	sprintf(szTmp, "%d", pHeader->nAltitud);

	pNode = pObjectDataNode->FirstChild("HgtAboveSea");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode) pTextNode->SetValue(szTmp);
	}
	else	// create a new tag
	{
		aAttributes[0]->SetValue("HGTABOVESEA");
		aAttributes[1]->SetValue("2012");
		aAttributes[2]->SetValue("1");
		InsertTag(pObjectDataNode, "HgtAboveSea", pAttributes, szTmp);
	}


	// Bonitet1
	sprintf(szTmp, "%S0", pHeader->csSI);
	if(szTmp[0] == 'T') szTmp[0] = '1';	// Tall
	else if(szTmp[0] == 'G') szTmp[0] = '2';	// Gran
	else if(szTmp[0] == 'B') szTmp[0] = '3';	// Bj�rk
	else if(szTmp[0] == 'C') szTmp[0] = '6';	// Contorta

	pNode = pObjectDataNode->FirstChild("Bonitet1");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode) pTextNode->SetValue(szTmp);
	}
	else	// create a new tag
	{
		aAttributes[0]->SetValue("BONITET1");
		aAttributes[1]->SetValue("2095");
		aAttributes[2]->SetValue("1");
		InsertTag(pObjectDataNode, "Bonitet1", pAttributes, szTmp);
	}


	// Bonitet2
	sprintf(szTmp, "%S0", pHeader->csSI);

	pNode = pObjectDataNode->FirstChild("Bonitet2");
	if(pNode)	// exitsting tag
	{
		pTextNode = pNode->FirstChild();
		if(pTextNode) pTextNode->SetValue(szTmp);
	}
	else	// create a new tag
	{
		aAttributes[0]->SetValue("BONITET2");
		aAttributes[1]->SetValue("2095");
		aAttributes[2]->SetValue("2");
		InsertTag(pObjectDataNode, "Bonitet2", pAttributes, szTmp);
	}


	Save(pHeader->csFilePath);

	// free memory
	delete aAttributes[0];
	delete aAttributes[1];
	delete aAttributes[2];
	delete pAttributes;

	return TRUE;
}

int CHXLParser::InsertTag(TiXmlNode *pNode, char* pszTag, TiXmlAttributeSet *pAttributes /*TiXmlAttribute *pAttributes[]*/, char* pszValue)
{
	// create the new element (tag)
	TiXmlElement *element = new TiXmlElement(pszTag);
	if(!pNode->LinkEndChild(element))
		return -1;
    
	// add any attributes
	int nPos = 0;
	TiXmlAttribute *pAttribute = pAttributes->First(); //pAttributes[nPos];
	while(pAttribute)
	{
		element->SetAttribute(pAttribute->Name(), pAttribute->Value());

		nPos++;
		pAttribute = pAttribute->Next();	//pAttributes[nPos];
	}

	// set the value for the element (tag)
	TiXmlText *text = new TiXmlText(pszValue);
	if(!element->LinkEndChild(text))
	{
		return -1;
	}

	return 0;
}

int CHXLParser::Open(CString csFilename)
{
	char szFilename[1024];

	m_doc.Clear();

	// load file content into a buffer
	CFile cfXml;
	if(!cfXml.Open(csFilename, CFile::modeRead))
	{
		AfxMessageBox(_T("Kunde inte �ppna HXL-fil!"), MB_ICONERROR);
		return FALSE;
	}

	char *pszBuf;
	pszBuf = (char*)malloc(cfXml.GetLength()+2);
	if(pszBuf == NULL)
	{
		AfxMessageBox(_T("Kunde inte allokera minne f�r att l�sa in fil!"));
		return FALSE;
	}

	cfXml.Read(pszBuf, cfXml.GetLength());
	pszBuf[cfXml.GetLength()] = ' '; // For some reason parsing will fail if there are no chars after the end tag
	pszBuf[cfXml.GetLength()+1] = '\0';
	cfXml.Close();
	CStringA csBuf = pszBuf;
	free(pszBuf);


	// remove all XML violating linefeeds inside tags
	ReplaceChars(&csBuf, "Species", '\n', '@');
	ReplaceChars(&csBuf, "coordinates", '\n', '@');
	ReplaceChars(&csBuf, "Tree", '\n', '@');


	// read XML from buffer
	if( !m_doc.Parse(csBuf.GetBuffer()) )
	{
		AfxMessageBox(_T("Kunde inte �ppna HXL-fil!"), MB_ICONERROR);
		return FALSE;
	}

	if(m_doc.Error())
	{
		AfxMessageBox( CString(m_doc.ErrorDesc()) );
		return FALSE;
	}

	m_csFilePath = csFilename;

	return TRUE;
}

int CHXLParser::ReplaceChars(CStringA *pcsBuf, CStringA csTag, char szSearch, char szReplace)
{
	// search for char inside a tag and replace it
	CStringA csStart, csStop;
	csStart.Format("<%s>", csTag);
	csStop.Format("</%s>", csTag);

	int nStart = pcsBuf->Find(csStart);
	if(nStart == -1) return FALSE;

	int nStop = pcsBuf->Find(csStop, nStart);
	if(nStop == -1) return FALSE;

	int nFound = pcsBuf->Find(szSearch, nStart+9);
	while(nFound != -1 && nFound < nStop)
	{
		pcsBuf->SetAt(nFound, szReplace);

		nFound = pcsBuf->Find(szSearch, nFound);
	};

	return TRUE;
}

int CHXLParser::Save(CString csFilename)
{
	// save HXL file, do not use tinyxml's internal saving, because it html encodes characters
	if( m_pPrinter ) delete m_pPrinter;
	m_pPrinter = new TiXmlPrinter;
	m_doc.Accept(m_pPrinter);

	CStringA csBuf;
	csBuf.Format("%s", m_pPrinter->CStr());
	csBuf.Replace("&#x0A;", "\n");
	delete m_pPrinter;
	m_pPrinter = NULL;

	// <Species> <Tree> and <coordinates> has to be handled differently becuase of the linefeed separators
	ReplaceChars(&csBuf, "Species", '@', '\n');
	ReplaceChars(&csBuf, "coordinates", '@', '\n');
	ReplaceChars(&csBuf, "Tree", '@', '\n');

	CFile cfOutput;
	if(cfOutput.Open(csFilename, CFile::modeCreate|CFile::modeReadWrite))
	{
		cfOutput.Write(csBuf.GetBuffer(), csBuf.GetLength());
		cfOutput.Close();
	}
	else
	{
		AfxMessageBox(_T("Kunde inte spara HXL-fil!"));
		return FALSE;
	}

	return TRUE;
}

int CHXLParser::getHxlAttributes(TiXmlElement* pElement, HXLDATA* pHxlData)
{
	// get HXL attributes from a node
	pHxlData->csName.Format(_T("%S"), pElement->Attribute("Name"));
	pHxlData->nVar = atoi(pElement->Attribute("Var"));
	pHxlData->nType = atoi(pElement->Attribute("Type"));
	pHxlData->csDef.Format(_T("%S"), pElement->Attribute("Def"));
				
	return TRUE;
}

int CHXLParser::setHxlAttributes(TiXmlElement* pElement, CStringA csName, CStringA csVar, CStringA csType)
{
	// remove old attributes
	TiXmlAttribute *pAttrib = pElement->FirstAttribute();
	while(pAttrib)
	{
		pElement->RemoveAttribute( pAttrib->Name() );

		pAttrib = pAttrib->Next();
	};

	// create the default attributes
	TiXmlAttribute *aAttributes[3];
	aAttributes[0] = new TiXmlAttribute();
	aAttributes[0]->SetName("Name");
	aAttributes[0]->SetValue(csName);
	aAttributes[1] = new TiXmlAttribute();
	aAttributes[1]->SetName("Var");
	aAttributes[1]->SetValue(csVar);
	aAttributes[2] = new TiXmlAttribute();
	aAttributes[2]->SetName("Type");
	aAttributes[2]->SetValue(csType);

	TiXmlAttributeSet *pAttributes = new TiXmlAttributeSet;
	pAttributes->Add(aAttributes[0]);
	pAttributes->Add(aAttributes[1]);
	pAttributes->Add(aAttributes[2]);

	// add the new attributes
	int nPos = 0;
	TiXmlAttribute *pAttribute = pAttributes->First(); //pAttributes[nPos];
	while(pAttribute)
	{
		pElement->SetAttribute(pAttribute->Name(), pAttribute->Value());

		nPos++;
		pAttribute = pAttribute->Next();	//pAttributes[nPos];
	}
	

	// free memory
	delete aAttributes[0];
	delete aAttributes[1];
	delete aAttributes[2];

	return TRUE;
}

CString CHXLParser::GetNodeValue(CStringA csNode)
{
	TiXmlNode *pNode = NULL;
	TiXmlElement *pElement = NULL;
	int nCurPos = 0;
	CStringA csTmp;
	CString csValue;

	csTmp = csNode.Tokenize("\\", nCurPos);
	while(csTmp != _T(""))
	{
		if(!pNode)
			pNode = m_doc.FirstChild(csTmp);
		else
			pNode = pNode->FirstChild(csTmp);

		if(!pNode)
			return _T("");

		csTmp = csNode.Tokenize("\\", nCurPos);
	};

	if(pNode)
	{
		pNode = pNode->FirstChild();
		if(pNode)
			return CString(pNode->Value());
	}

	return _T("");
}

int CHXLParser::SetNodeValue(CStringA csNode, CStringA csName, CStringA csVar, CStringA csType, int nValue)
{
	CString csTmp;
	csTmp.Format(_T("%d"), nValue);
	return SetNodeValue(csNode, csName, csVar, csType, csTmp);
}

int CHXLParser::SetNodeValue(CStringA csNode, CStringA csName, CStringA csVar, CStringA csType, LPCTSTR lpszValue)
{
	TiXmlNode *pNode = NULL, *pPrevNode = NULL;
	TiXmlElement *pElement = NULL;
	int nCurPos = 0;
	CStringA csTmp, csTmpValue;
	csTmpValue.Format("%S", lpszValue);

	csTmp = csNode.Tokenize("\\", nCurPos);
	while(csTmp != _T(""))
	{
		if(!pNode)
			pNode = m_doc.FirstChild(csTmp);
		else
			pNode = pNode->FirstChild(csTmp);

		if(!pNode)
			break;

		pPrevNode = pNode;

		csTmp = csNode.Tokenize("\\", nCurPos);
	};

	if(pNode)	// we found the node, set the value
	{
		pNode = pNode->FirstChild();
		if(pNode)
		{
			pNode->SetValue(csTmpValue);
		}
	}
	else if(csTmp != "")	// node did not exist, create a new
	{
		// create the default attributes
		TiXmlAttribute *aAttributes[3];
		aAttributes[0] = new TiXmlAttribute();
		aAttributes[0]->SetName("Name");
		aAttributes[0]->SetValue(csName);
		aAttributes[1] = new TiXmlAttribute();
		aAttributes[1]->SetName("Var");
		aAttributes[1]->SetValue(csVar);
		aAttributes[2] = new TiXmlAttribute();
		aAttributes[2]->SetName("Type");
		aAttributes[2]->SetValue(csType);

		TiXmlAttributeSet *pAttributes = new TiXmlAttributeSet;
		pAttributes->Add(aAttributes[0]);
		pAttributes->Add(aAttributes[1]);
		pAttributes->Add(aAttributes[2]);

		InsertTag(pPrevNode, csTmp.GetBuffer(), pAttributes, csTmpValue.GetBuffer());

		// free memory
		delete aAttributes[0];
		delete aAttributes[1];
		delete aAttributes[2];
	}

	return TRUE;
}



CString CHXLParser::getBestand()
{
/*	CString csTmp;

	csTmp = GetNodeValue("HXL\\Object\\objectData\\Best");
	int nFound = csTmp.Find('_');

	if(nFound != NULL)
	{
		csTmp = csTmp.Left(csTmp.GetLength() - nFound - 1 );
	}

	return csTmp;*/

	return GetNodeValue("HXL\\Object\\objectData\\Best");
}

int CHXLParser::setBestand(CString csValue)
{
	return SetNodeValue("HXL\\Object\\objectData\\Best", "STANDID", "2", "1", csValue);
}

int CHXLParser::getAreal()
{
	return _wtoi(GetNodeValue("HXL\\Object\\objectData\\Areal"));
}

int CHXLParser::setAreal(int nValue)
{
	return SetNodeValue("HXL\\Object\\objectData\\Areal", "AREAL", "670", "1", nValue);
}

int CHXLParser::getLatitud()
{
	return _wtoi(GetNodeValue("HXL\\Object\\objectData\\Latitude")) / 10;
}

int CHXLParser::setLatitud(int nValue)
{
	return SetNodeValue("HXL\\Object\\objectData\\Latitude", "LATITUDE", "2011", "2", nValue);
}

int CHXLParser::getInventeringsmetod()
{
	return _wtoi(GetNodeValue("HXL\\Object\\objectData\\Metod"));
}

int CHXLParser::setInventeringsmetod(int nValue)
{
	return SetNodeValue("HXL\\Object\\objectData\\Metod", "TAXMETOD", "2030", "1", nValue);
}

CString CHXLParser::getSI()
{
	CString csSI;

	csSI = GetNodeValue("HXL\\Object\\objectData\\Bonitet2");
	if(csSI == _T(""))
	{
		csSI = GetNodeValue("HXL\\Object\\objectData\\Bonitet");

		if(csSI != _T(""))
		{
			if(csSI.GetAt(0) == '1') csSI.SetAt(0, 'T');	// Tall
			else if(csSI.GetAt(0) == '2') csSI.SetAt(0, 'G');	// Gran
			else if(csSI.GetAt(0) == '3') csSI.SetAt(0, 'B');	// Bj�rk
			else if(csSI.GetAt(0) == '6') csSI.SetAt(0, 'C');	// Contorta
		}
	}

	csSI = csSI.Left(3);	// truncate the string

	return csSI;
}

int CHXLParser::setSI(CString csValue)
{
	SetNodeValue("HXL\\Object\\objectData\\Bonitet", "BONITET2", "2095", "2", csValue);

	if(csValue.GetAt(0) == 'T') csValue.SetAt(0, '1');	// Tall
	else if(csValue.GetAt(0) == 'G') csValue.SetAt(0, '2');	// Gran
	else if(csValue.GetAt(0) == 'B') csValue.SetAt(0, '3');	// Bj�rk
	else if(csValue.GetAt(0) == 'C') csValue.SetAt(0, '6');	// Contorta
	SetNodeValue("HXL\\Object\\objectData\\Bonitet", "BONITET", "2095", "1", csValue);

	return TRUE;
}

CString CHXLParser::getDatum()
{
	return GetNodeValue("HXL\\Object\\objectData\\Date");
}

int CHXLParser::setDatum(CString csValue)
{
	return SetNodeValue("HXL\\Object\\objectData\\Date", "DATA", "12", "4", csValue);
}

int CHXLParser::getAltitud()
{
	return _wtoi(GetNodeValue("HXL\\Object\\objectData\\HgtAboveSea"));
}

int CHXLParser::setAltitud(int nValue)
{
	return SetNodeValue("HXL\\Object\\objectData\\HgtAboveSea", "HGTABOVESEA", "2012", "1", nValue);
}

int CHXLParser::getBestAlder()
{
	return _wtoi(GetNodeValue("HXL\\Object\\objectData\\BestAge"));
}

int CHXLParser::setBestAlder(int nValue)
{
	return SetNodeValue("HXL\\Object\\objectData\\BestAge", "BESTAGE", "660", "1", nValue);
}

int CHXLParser::getNumPlots()
{
	return _wtoi(GetNodeValue("HXL\\Object\\objectData\\Numplots"));
}

int CHXLParser::getPlots(std::vector< PLOT > *pvecPlots)
{
	HXLDATA hxldata;
	CArray<HXLDATA, HXLDATA> caTreeVars, caSpeciesVars;
	TiXmlNode *pRootNode, *pObjectNode, *pNode, *pTextNode;
	DCLS dc;
	CMyTransactionSampleTree st;
	SPECIES sp;
	TREETYPE tt;
	PLOT pl;
	int nSpecNr, nDbh, nHgtNum, nHgt, nHgt2, nAge, nType, nBonitet, nPlot;

	pRootNode = m_doc.FirstChild("HXL");
	pObjectNode = pRootNode->FirstChild("Object");

	// get species from the HXL file
	vecTransactionSpecies vecSpecies;

	pNode = pObjectNode->FirstChild("objectData");
	if(pNode)	// exitsting tag
	{
		pNode = pNode->FirstChild("SpecieSet");
		if(pNode)	// exitsting tag
		{
			TiXmlElement* pTreeNode = pNode->FirstChildElement("SpecieVar");
			while( pTreeNode )
			{
				// get the HXL attributes for the node
				getHxlAttributes(pTreeNode, &hxldata);
				caSpeciesVars.Add(hxldata);

				pTreeNode = pTreeNode->NextSiblingElement("SpecieVar");
			}
		}

		CString csSpeciesName;
		int nSpeciesNumber;

		pNode = pNode->FirstChild("Species");
		if(pNode)	// exitsting tag
		{
			pTextNode = pNode->FirstChild();
			if(pTextNode)
			{
				CString csTrees;
				CString csLine, csValue;
				int nCurPos = 0, nCurPos2 = 0, nValue = 0;

				csTrees.Format(_T("%S"), pTextNode->Value());

				csLine = csTrees.Tokenize(_T("\t\n@"), nCurPos);
				while (csLine != _T(""))
				{
					nCurPos2 = 0;
					nValue = 0;
					csValue = csLine.Tokenize(_T(","), nCurPos2);
					while (csValue != _T(""))
					{
						hxldata = caSpeciesVars.GetAt(nValue);
						if(hxldata.nVar == 120 && hxldata.nType == 1)	// SPECIENAME
						{
							csSpeciesName = csValue;
						}
						else if(hxldata.nVar == 120 && hxldata.nType == 3)	// SPECIENUMBER
						{
							nSpeciesNumber = _wtoi(csValue);
						}

						csValue = csLine.Tokenize(_T(","), nCurPos2);
						nValue++;
					};

					vecSpecies.push_back( CTransaction_species(nSpeciesNumber, nSpeciesNumber, csSpeciesName) );

					csLine = csTrees.Tokenize(_T("\t\n@"), nCurPos);
				};

			}
		}
	}

	// read plot data and create plots in plot vector
	TiXmlNode *pNodePlotSet = pObjectNode->FirstChild("PlotSet");
	TiXmlNode* pNodePlot = pNodePlotSet->FirstChild("Plot");
	TiXmlNode* pNodeTmp;
	TiXmlText* pTextTmp;
	nPlot = 1;
	while(pNodePlot)
	{
		// initialize all plot variables
		pl.csId = _T("");
		pl.nAntal = 0;
		pl.nAreal = 0;
		pl.nBonitet = 0;
		pl.nDbhAge = 0;
		pl.nOhgt = 0;
		pl.nPlotNum = 0;
		pl.nSi = 0;
		pl.nSiTree = 0;

		// ID
		pNodeTmp = pNodePlot->FirstChild("Id");
		if(pNodeTmp)
		{
			pTextTmp = (TiXmlText*)pNodeTmp->FirstChild();
			if(pTextTmp)
				pl.csId = pTextTmp->Value();
		}
		
		// AREAL
		pNodeTmp = pNodePlot->FirstChild("Areal");
		if(pNodeTmp)
		{
			pTextTmp = (TiXmlText*)pNodeTmp->FirstChild();
			if(pTextTmp)
				pl.nAreal = atoi(pTextTmp->Value());
		}

		// OHGT
		pNodeTmp = pNodePlot->FirstChild("Ohgt");
		if(pNodeTmp)
		{
			pTextTmp = (TiXmlText*)pNodeTmp->FirstChild();
			if(pTextTmp)
				pl.nOhgt = atoi(pTextTmp->Value());
		}

		// DBHAGE
		pNodeTmp = pNodePlot->FirstChild("DbhAge");
		if(pNodeTmp)
		{
			pTextTmp = (TiXmlText*)pNodeTmp->FirstChild();
			if(pTextTmp)
				pl.nDbhAge = atoi(pTextTmp->Value());
		}

		// BONITET
		pNodeTmp = pNodePlot->FirstChild("Bonitet");
		if(pNodeTmp)
		{
			pTextTmp = (TiXmlText*)pNodeTmp->FirstChild();
			if(pTextTmp)
				pl.nBonitet = atoi(pTextTmp->Value());
		}

		// SITREE
		pNodeTmp = pNodePlot->FirstChild("SiTree");
		if(pNodeTmp)
		{
			pTextTmp = (TiXmlText*)pNodeTmp->FirstChild();
			if(pTextTmp)
				pl.nSiTree = atoi(pTextTmp->Value());
		}

		// SI
		pNodeTmp = pNodePlot->FirstChild("Si");
		if(pNodeTmp)
		{
			pTextTmp = (TiXmlText*)pNodeTmp->FirstChild();
			if(pTextTmp)
				pl.nSi = atoi(pTextTmp->Value());
		}

		// ANTAL
		pNodeTmp = pNodePlot->FirstChild("Antal");
		if(pNodeTmp)
		{
			pTextTmp = (TiXmlText*)pNodeTmp->FirstChild();
			if(pTextTmp)
				pl.nAntal = atoi(pTextTmp->Value());
		}


		for(int nTreeType=0; nTreeType<3; nTreeType++)
		{
			for(int nSpecies=0; nSpecies<vecSpecies.size(); nSpecies++)
			{
				sp.nSpeciesNum = vecSpecies[nSpecies].getID();
				tt.vecSpecies.push_back(sp);
			}

			tt.nTreeTypeNum = nTreeType;
			pl.vecTreeType.push_back(tt);
			tt.vecSpecies.clear();
		}

		pl.nPlotNum = nPlot;
		pvecPlots->push_back(pl);
		pl.vecTreeType.clear();

		nPlot++;
		pNodePlot = pNodePlot->NextSibling();
	};


	pNode = pObjectNode->FirstChild("TreeSet");
	if(pNode)	// exitsting tag
	{
		TiXmlElement* pTreeNode = pNode->FirstChildElement("TreeVar");
		while( pTreeNode )
		{
			// get the HXL attributes for the node
			getHxlAttributes(pTreeNode, &hxldata);
			caTreeVars.Add(hxldata);

			pTreeNode = pTreeNode->NextSiblingElement("TreeVar");
		}

		pNode = pNode->FirstChild("Tree");
		if(pNode)	// exitsting tag
		{
			pTextNode = pNode->FirstChild();
			if(pTextNode)
			{
				CString csTrees;
				CString csLine, csValue;
				int nCurPos = 0, nCurPos2 = 0, nValue = 0;

				csTrees.Format(_T("%S"), pTextNode->Value());

				csLine = csTrees.Tokenize(_T("\t\n@"), nCurPos);
				while (csLine != _T(""))
				{
					nSpecNr = 0;
					nDbh = 0;
					nHgtNum = 0;
					nHgt = 0;
					nHgt2 = 0;
					nAge = 0;
					nType = 0;
					nBonitet = 0;
					nPlot = 0;

					st = CMyTransactionSampleTree();	// initialize the sample tree variable

					nCurPos2 = 0;
					nValue = 0;
					csValue = csLine.Tokenize(_T(","), nCurPos2);
					while (csValue != _T(""))
					{
						hxldata = caTreeVars.GetAt(nValue);
						if(hxldata.nVar == 652 && hxldata.nType == 1)	// SPECNR
						{
							int nValue = _wtoi(csValue);

							// find the species that has id == csValue
							nSpecNr = -1;
							for(int nLoop=0; nLoop<vecSpecies.size(); nLoop++)
							{
								if(vecSpecies.at(nLoop).getSpcID() == nValue)
								{
									nSpecNr = nLoop;
									break;
								}
							}

							if(nSpecNr == -1)	// we could not find the plot?!?
							{
								AfxMessageBox(_T("Fel i HXL-fil; tr�dslag registrerad p� tr�det finns inte!"));
								return FALSE;
							}

						}
						else if(hxldata.nVar == 653 && hxldata.nType == 1)	// DBH
						{
							nDbh = _wtoi(csValue);
						}
						else if(hxldata.csName == _T("HGTNUM") && hxldata.nVar == 654 && hxldata.nType == 1)	// heights
						{
							nHgtNum = _wtoi(csValue);
						}
						else if(hxldata.nVar == 656 && hxldata.nType == 1 && hxldata.csDef == _T("50"))	// HGT
						{
							nHgt = _wtoi(csValue);
						}
						else if(hxldata.nVar == 656 && hxldata.nType == 1 && hxldata.csDef == _T("51"))	// HGT2
						{
							nHgt2 = _wtoi(csValue);
						}
						else if(hxldata.nVar == 2001 && hxldata.nType == 2)	// AGE
						{
							nAge = _wtoi(csValue);
						}
						else if(hxldata.nVar == 2004 && hxldata.nType == 2)	// TYPE
						{
							nType = _wtoi(csValue);
						}
						else if(hxldata.nVar == 2006 && hxldata.nType == 1)	// BONITET
						{
							nBonitet = _wtoi(csValue);
						}
						else if(hxldata.nVar == 2052 && hxldata.nType == 1)	// PLOT
						{
							csValue = csValue.Trim();

							// find the plot that has Plot id == csValue
							nPlot = -1;
							for(int nLoop=0; nLoop<pvecPlots->size(); nLoop++)
							{
								if(pvecPlots->at(nLoop).csId == csValue)
								{
									nPlot = nLoop;
									break;
								}
							}

							if(nPlot == -1)	// we could not find the plot?!?
							{
								AfxMessageBox(_T("Fel i HXL-fil; provyta registrerad p� tr�det finns inte!"));
								return FALSE;
							}
						}

						csValue = csLine.Tokenize(_T(","), nCurPos2);
						nValue++;
					};

					// save the tree into the proper plot
					if(nHgtNum > 0)	// sample tree
					{
						st.setAge(nAge);
						st.setBonitet(nBonitet);
						//st.setBrk();	// bark?
						st.setDbh(nDbh);
						st.setGCrown(nHgt2 / 10);	// height2 in dm
						st.setHgt(nHgt / 10);	// height in dm
						st.setSpcId(nSpecNr);
						//st.setTypeName(_T(""))	// uttag, kvarl�mnat?

						pvecPlots->at(nPlot).vecTreeType[2].vecSpecies[nSpecNr].vecSampleTrees.push_back(st);
					}
					else	// tree
					{
						// kolla om storleken p� diameterklassvektorn �r mindre �n index f�r nuvarande diameterklass
						// om den �r det, skapa X antal diameterklasser fr�n vector.size() till nuvarande diameterklass.
						// annars bara addera ett tr�d p� l�mplig diameterklass
						int nDcls = (int)ceil(nDbh / 20.0) - 1;	// diameterklass
						
						if(pvecPlots->at(nPlot).vecTreeType[0].vecSpecies[nSpecNr].vecDcls.size() <= nDcls)
						{
							for(int nLoop=pvecPlots->at(nPlot).vecTreeType[0].vecSpecies[nSpecNr].vecDcls.size(); nLoop<nDcls; nLoop++)
							{
								dc.nAmount = 0;
								dc.nDclsNum = nLoop;
								pvecPlots->at(nPlot).vecTreeType[0].vecSpecies[nSpecNr].vecDcls.push_back(dc);
							}

							dc.nAmount = 1;
							dc.nDclsNum = nDcls;
							pvecPlots->at(nPlot).vecTreeType[0].vecSpecies[nSpecNr].vecDcls.push_back(dc);
						}
						else
						{
							pvecPlots->at(nPlot).vecTreeType[0].vecSpecies[nSpecNr].vecDcls[nDcls].nAmount++;
						}
					}

					csLine = csTrees.Tokenize(_T("\t\n@"), nCurPos);
				};

			}
		}
	}

	return TRUE;
}

int CHXLParser::setPlots(std::vector< PLOT > *pvecPlots)
{
	HXLDATA hxldata;
	CArray<HXLDATA, HXLDATA> caTreeVars;

	TiXmlNode *pRootNode = m_doc.FirstChild("HXL");
	TiXmlNode *pObjectNode = pRootNode->FirstChild("Object");
	TiXmlNode *pNodePlotSet = pObjectNode->FirstChild("PlotSet");

	// delete old plots
	TiXmlNode *pNodePlot = pNodePlotSet->FirstChild("Plot");
	while(pNodePlot)
	{
		pNodePlotSet->RemoveChild(pNodePlot);

		pNodePlot = pNodePlotSet->FirstChild("Plot");
	};

	// insert plots
	PLOT pl;
	TiXmlNode *pNodeTmp;
	TiXmlText *pTextTmp;
	CStringA csBuf;

	for(int nPlot=0; nPlot<pvecPlots->size(); nPlot++)
	{
		pl = pvecPlots->at(nPlot);

		// create a new Plot-tag
		TiXmlElement *element = new TiXmlElement("Plot");
		pNodePlot = pNodePlotSet->LinkEndChild(element);

		// create ID tag
		TiXmlElement *elementId = new TiXmlElement("Id");
		pNodeTmp = pNodePlot->LinkEndChild(elementId);
		setHxlAttributes(elementId, "ID", "2052", "1");
		csBuf.Format("%S", pl.csId);
		pTextTmp = new TiXmlText(csBuf);
		pNodeTmp->LinkEndChild(pTextTmp);

		// create AREAL tag
		TiXmlElement *elementAreal = new TiXmlElement("Areal");
		pNodeTmp = pNodePlot->LinkEndChild(elementAreal);
		setHxlAttributes(elementAreal, "AREAL", "671", "1");
		csBuf.Format("%d", pl.nAreal);
		pTextTmp = new TiXmlText(csBuf);
		pNodeTmp->LinkEndChild(pTextTmp);

		// create OHGT tag
		TiXmlElement *elementOhgt = new TiXmlElement("Ohgt");
		pNodeTmp = pNodePlot->LinkEndChild(elementOhgt);
		setHxlAttributes(elementOhgt, "OHGT", "", "1");
		csBuf.Format("%d", pl.nOhgt);
		pTextTmp = new TiXmlText(csBuf);
		pNodeTmp->LinkEndChild(pTextTmp);

		// create DBHAGE tag
		TiXmlElement *elementDbhAge = new TiXmlElement("DbhAge");
		pNodeTmp = pNodePlot->LinkEndChild(elementDbhAge);
		setHxlAttributes(elementDbhAge, "DBHAGE", "2096", "1");
		csBuf.Format("%d", pl.nDbhAge);
		pTextTmp = new TiXmlText(csBuf);
		pNodeTmp->LinkEndChild(pTextTmp);

		// create BONITET tag
		TiXmlElement *elementBonitet = new TiXmlElement("Bonitet");
		pNodeTmp = pNodePlot->LinkEndChild(elementBonitet);
		setHxlAttributes(elementBonitet, "BONITET", "2095", "1");
		csBuf.Format("%d", pl.nBonitet);
		pTextTmp = new TiXmlText(csBuf);
		pNodeTmp->LinkEndChild(pTextTmp);

		// create SITREE tag
		TiXmlElement *elementSiTree = new TiXmlElement("SiTree");
		pNodeTmp = pNodePlot->LinkEndChild(elementSiTree);
		setHxlAttributes(elementSiTree, "SITREE", "", "");
		csBuf.Format("%d", pl.nSiTree);
		pTextTmp = new TiXmlText(csBuf);
		pNodeTmp->LinkEndChild(pTextTmp);

		// create SI tag
		TiXmlElement *elementSi = new TiXmlElement("Si");
		pNodeTmp = pNodePlot->LinkEndChild(elementSi);
		setHxlAttributes(elementSi, "SI", "", "");
		csBuf.Format("%d", pl.nSi);
		pTextTmp = new TiXmlText(csBuf);
		pNodeTmp->LinkEndChild(pTextTmp);

		// create ANTAL tag
		TiXmlElement *elementAntal = new TiXmlElement("Antal");
		pNodeTmp = pNodePlot->LinkEndChild(elementAntal);
		setHxlAttributes(elementAntal, "ANTAL", "", "");
		csBuf.Format("%d", pl.nAntal);
		pTextTmp = new TiXmlText(csBuf);
		pNodeTmp->LinkEndChild(pTextTmp);
	}

	return TRUE;
}

int CHXLParser::setTrees(std::vector< PLOT > *pvecPlots)
{
	HXLDATA hxldata;
	CArray<HXLDATA, HXLDATA> caTreeVars;

	TiXmlNode *pRootNode = m_doc.FirstChild("HXL");
	TiXmlNode *pObjectNode = pRootNode->FirstChild("Object");

	// get tree variables
	TiXmlNode *pNode = pObjectNode->FirstChild("TreeSet");
	if(pNode)	// exitsting tag
	{
		TiXmlElement* pTreeNode = pNode->FirstChildElement("TreeVar");
		while( pTreeNode )
		{
			// get the HXL attributes for the node
			getHxlAttributes(pTreeNode, &hxldata);
			caTreeVars.Add(hxldata);

			pTreeNode = pTreeNode->NextSiblingElement("TreeVar");
		}
	}

	// clear tree tag
//	pNode->RemoveChild( pNode->FirstChild("Tree") );

	// insert the new tree tag with trees
	DCLS dc;
	CMyTransactionSampleTree st;
	SPECIES sp;
	TREETYPE tt;
	PLOT pl;
	CStringA csOutput, csTmp;

	for(int nPlot=0; nPlot<pvecPlots->size(); nPlot++)
	{
		pl = pvecPlots->at(nPlot);

		for(int nTreeType=0; nTreeType<pl.vecTreeType.size(); nTreeType++)
		{
			tt = pl.vecTreeType[nTreeType];
		
			for(int nSpecies=0; nSpecies<tt.vecSpecies.size(); nSpecies++)
			{
				sp = tt.vecSpecies[nSpecies];
				
				// diameter class
				for(int nDcls=0; nDcls<sp.vecDcls.size(); nDcls++)
				{
					dc = sp.vecDcls[nDcls];

					if(dc.nAmount > 0)	// got any diameters?
					{
						for(int nTrees=0; nTrees<dc.nAmount; nTrees++)
						{
							for(int nTreeVar=0; nTreeVar<caTreeVars.GetSize(); nTreeVar++)
							{
								if(caTreeVars.GetAt(nTreeVar).nVar == 652 && caTreeVars.GetAt(nTreeVar).nType == 1)	// SPECNR
									csTmp.Format("%d,", sp.nSpeciesNum);
								else if(caTreeVars.GetAt(nTreeVar).nVar == 653 && caTreeVars.GetAt(nTreeVar).nType == 1)	// DBH
									csTmp.Format("%d,", (dc.nDclsNum * 20) + 10);	// middle of diameter class
								else if(caTreeVars.GetAt(nTreeVar).nVar == 654 && caTreeVars.GetAt(nTreeVar).nType == 1)	// HGTNUM
									csTmp.Format("%d,", 0);
								else if(caTreeVars.GetAt(nTreeVar).nVar == 656 && caTreeVars.GetAt(nTreeVar).nType == 1 && caTreeVars.GetAt(nTreeVar).csDef == _T("50"))	// HGT
									csTmp.Format("%d,", 0);
								else if(caTreeVars.GetAt(nTreeVar).nVar == 656 && caTreeVars.GetAt(nTreeVar).nType == 1 && caTreeVars.GetAt(nTreeVar).csDef == _T("51"))	// HGT2
									csTmp.Format("%d,", 0);
								else if(caTreeVars.GetAt(nTreeVar).nVar == 2001 && caTreeVars.GetAt(nTreeVar).nType == 2)	// AGE
									csTmp.Format("%d,", 0);
								else if(caTreeVars.GetAt(nTreeVar).nVar == 2004 && caTreeVars.GetAt(nTreeVar).nType == 2)	// TYPE
									csTmp.Format("%d,", 0);
								else if(caTreeVars.GetAt(nTreeVar).nVar == 2006 && caTreeVars.GetAt(nTreeVar).nType == 1)	// BONITET
									csTmp.Format("%d,", 0);
								else if(caTreeVars.GetAt(nTreeVar).nVar == 2052 && caTreeVars.GetAt(nTreeVar).nType == 1)	// PLOT
									csTmp.Format("%S,", pl.csId);
								else
									csTmp.Format("0,");

								csOutput += csTmp;
							}
							csOutput.SetAt(csOutput.GetLength()-1, '\n');	// end with a linefeed
						}
					}
				}

				// sample trees
				for(int nSampleTree=0; nSampleTree<sp.vecSampleTrees.size(); nSampleTree++)
				{
					st = sp.vecSampleTrees[nSampleTree];

					for(int nTreeVar=0; nTreeVar<caTreeVars.GetSize(); nTreeVar++)
					{
						if(caTreeVars.GetAt(nTreeVar).nVar == 652 && caTreeVars.GetAt(nTreeVar).nType == 1)	// SPECNR
							csTmp.Format("%d,", sp.nSpeciesNum);
						else if(caTreeVars.GetAt(nTreeVar).nVar == 653 && caTreeVars.GetAt(nTreeVar).nType == 1)	// DBH
							csTmp.Format("%.0f,", st.getDbh());
						else if(caTreeVars.GetAt(nTreeVar).nVar == 654 && caTreeVars.GetAt(nTreeVar).nType == 1)	// HGTNUM
						{
							int nTmp = 0;
							if(st.getHgt() != 0.0) nTmp++;
							if(st.getGCrown() != 0.0) nTmp++;

							csTmp.Format("%d,", nTmp);
						}
						else if(caTreeVars.GetAt(nTreeVar).nVar == 656 && caTreeVars.GetAt(nTreeVar).nType == 1 && caTreeVars.GetAt(nTreeVar).csDef == _T("50"))	// HGT
							csTmp.Format("%.0f,", st.getHgt() * 10);	// height in cm
						else if(caTreeVars.GetAt(nTreeVar).nVar == 656 && caTreeVars.GetAt(nTreeVar).nType == 1 && caTreeVars.GetAt(nTreeVar).csDef == _T("51"))	// HGT2
							csTmp.Format("%.0f,", st.getGCrown() * 10);	// height in cm
						else if(caTreeVars.GetAt(nTreeVar).nVar == 2001 && caTreeVars.GetAt(nTreeVar).nType == 2)	// AGE
							csTmp.Format("%.0f,", st.getAge());
						else if(caTreeVars.GetAt(nTreeVar).nVar == 2004 && caTreeVars.GetAt(nTreeVar).nType == 2)	// TYPE
							csTmp.Format("%d,", st.getType());
						else if(caTreeVars.GetAt(nTreeVar).nVar == 2006 && caTreeVars.GetAt(nTreeVar).nType == 1)	// BONITET
							csTmp.Format("%.0f,", st.getBonitet());
						else if(caTreeVars.GetAt(nTreeVar).nVar == 2052 && caTreeVars.GetAt(nTreeVar).nType == 1)	// PLOT
							csTmp.Format("%S,", pl.csId);
						else
							csTmp.Format("0,");

						csOutput += csTmp;
					}
					csOutput.SetAt(csOutput.GetLength()-1, '\n');	// end with a linefeed
				}
			}
		}
	}

	// update the Tree tag
	TiXmlNode* pnodeTree = pNode->FirstChild("Tree");
	TiXmlText* textTree = (TiXmlText*)pnodeTree->FirstChild();
	textTree->SetValue(csOutput);
	
	return TRUE;
}

int CHXLParser::setSpecies(vecTransactionSpecies *pvecSpecies)
{
	HXLDATA hxldata;
	CArray<HXLDATA, HXLDATA> caSpeciesVars;

	TiXmlNode *pRootNode = m_doc.FirstChild("HXL");
	TiXmlNode *pObjectNode = pRootNode->FirstChild("Object");

	// get species variables
	vecTransactionSpecies vecSpecies;

	TiXmlNode *pNode = pObjectNode->FirstChild("objectData");
	if(pNode)	// exitsting tag
	{
		pNode = pNode->FirstChild("SpecieSet");
		if(pNode)	// exitsting tag
		{
			TiXmlElement* pTreeNode = pNode->FirstChildElement("SpecieVar");
			while( pTreeNode )
			{
				// get the HXL attributes for the node
				getHxlAttributes(pTreeNode, &hxldata);
				caSpeciesVars.Add(hxldata);

				pTreeNode = pTreeNode->NextSiblingElement("SpecieVar");
			}
		}
	}

	// clear existing species tags
	pNode = pObjectNode->FirstChild("objectData");
	if(pNode)	// exitsting tag
	{
		pNode = pNode->FirstChild("SpecieSet");
		if(pNode)	// existing tag
		{
			// update the species tag
			CStringA csBuf, csTmp;
			for(int nLoop=0; nLoop<pvecSpecies->size(); nLoop++)
			{
				for(int nVar=0; nVar<caSpeciesVars.GetSize(); nVar++)
				{
					if(caSpeciesVars[nVar].nVar == 120 && caSpeciesVars[nVar].nType == 1)	// SPECIESNAME
					{
						csTmp.Format("%S,", pvecSpecies->at(nLoop).getSpcName() );
					}
					else if(caSpeciesVars[nVar].nVar == 120 && caSpeciesVars[nVar].nType == 3)	// SPECIESNUMBER
					{
						csTmp.Format("%d,", pvecSpecies->at(nLoop).getSpcID() );
					}

					csBuf += csTmp;
				}

				csBuf.SetAt(csBuf.GetLength()-1, '\n');
			}

			TiXmlNode* pnodeSpecies = pNode->FirstChild("Species");
			TiXmlText* textSpecies = (TiXmlText*)pnodeSpecies->FirstChild();
			textSpecies->SetValue(csBuf);
		}
	}


	return TRUE;
}
