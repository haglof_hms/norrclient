// stdafx.cpp : source file that includes just the standard includes
// NorrClient.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

void setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos)
{
	CWnd *pWnd = wnd;
	if (pWnd)
	{
		if (!use_winpos)
		{
			pWnd->MoveWindow(x,y,w,h);
		}
		else
		{
			pWnd->SetWindowPos(&CWnd::wndBottom, x, y, w, h,SWP_NOACTIVATE);
		}
	}	// if (pWnd)
}

//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_species
CTransaction_species::CTransaction_species(void)
{
	m_nID					= -1;
	m_nSpcID			= -1;
	m_sSpcName		= _T("");
	m_sNotes			= _T("");
	m_sCreated		= _T("");
}

CTransaction_species::CTransaction_species(int id,int spc_id,LPCTSTR spc_name,LPCTSTR notes,LPCTSTR created)
{
	m_nID					= id;
	m_nSpcID			= spc_id;
	m_sSpcName		= (spc_name);
	m_sNotes			= (notes);
	m_sCreated		= (created);
}

CTransaction_species::CTransaction_species(const CTransaction_species &c)
{
	*this = c;
}

int CTransaction_species::getID(void)
{
	return m_nID;
}

int CTransaction_species::getSpcID(void)
{
	return m_nSpcID;
}

CString CTransaction_species::getSpcName(void)
{
	return m_sSpcName;
}

CString CTransaction_species::getNotes(void)
{
	return m_sNotes;
}

CString CTransaction_species::getCreated(void)
{
	return m_sCreated;
}

BOOL getSpecies(vecTransactionSpecies &pvecSpecies)
{
	pvecSpecies.push_back( CTransaction_species(1, 1, _T("Tall"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(2, 2, _T("Gran"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(3, 3, _T("Bj�rk"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(4, 4, _T("�vrigt l�v"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(5, 5, _T("�vrigt barr"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(6, 6, _T("Contorta"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(7, 7, _T("Torra"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(8, 8, _T("Alm"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(9, 9, _T("Ek"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(10, 10, _T("Ask"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(11, 11, _T("L�nn"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(12, 12, _T("Lind"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(13, 13, _T("Bok"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(14, 14, _T("Gr�al"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(15, 15, _T("Klibbal"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(16, 16, _T("F�gelb�r"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(17, 17, _T("Sitka"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(18, 18, _T("L�rk"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(19, 19, _T("Avenbok"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(20, 20, _T("S�lg"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(21, 21, _T("Pil"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(22, 22, _T("Poppel"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(23, 23, _T("Asp"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(24, 24, _T("Douglas"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(25, 25, _T("Fj�llbj�rk"), _T(""), _T("")) );
	pvecSpecies.push_back( CTransaction_species(26, 26, _T("L�v"), _T(""), _T("")) );

	return TRUE;
}

CString CheckStringNotForbidden(CString str)
{
	str.Replace(_T("&"),_T("&amp;"));
	return str;
}

CString getSpcName(vecTransactionSpecies &vec, int spcid)
{
	if(vec.size() > 0)
	{
		for(UINT i=0; i<vec.size(); i++)
		{
			if(vec[i].getSpcID() == spcid)
				return vec[i].getSpcName();
		}
	}

	return NULL;
}

CString getTmpPath()
{
	// Get the temp path.
	DWORD dwBufSize = 1024 /** sizeof(TCHAR)*/;
	TCHAR lpPathBuffer[1024];

	DWORD dwRetVal = GetTempPath(dwBufSize, lpPathBuffer);
	if (dwRetVal > dwBufSize || (dwRetVal == 0))
	{
		return _T("");
	}

	return CString(lpPathBuffer);
}

LPCTSTR getUserId()
{
	DWORD dwSize;
	static TCHAR szRet[600];
	TCHAR szUser[256], szComputer[256];

	dwSize = sizeof(szUser) / sizeof(TCHAR);
	GetUserName(szUser, &dwSize);
	
	dwSize = sizeof(szComputer) / sizeof(TCHAR);
	GetComputerName(szComputer, &dwSize);

	_stprintf(szRet, _T("%s-%s"), szUser, szComputer);

	return szRet;
}

LPCTSTR getUserName()
{
	DWORD dwSize;
	static TCHAR szUser[256];

	dwSize = sizeof(szUser) / sizeof(TCHAR);
	GetUserName(szUser, &dwSize);

	return szUser;
}

CString regGetStr_LM(LPCTSTR root, LPCTSTR key, LPCTSTR item, LPCTSTR def_str)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[512];
	TCHAR szValue[512];
	DWORD dwKeySize = 255;
	LONG lRet = 0;

	_tcscpy(szValue, def_str);
	_stprintf(szRoot, _T("%s\\%s"),root, key);

	if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
		szRoot,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_READ,
		NULL,
		&hk,
		&dwDisp) == ERROR_SUCCESS)
	{
		lRet = RegQueryValueEx(hk,
			item,
			0,
			NULL,
			(PBYTE)&szValue,
			&dwKeySize);
	}

	RegCloseKey(hk);

	return szValue;
}

DWORD regGetInt_LM(LPCTSTR root, LPCTSTR key, LPCTSTR item, int def_value)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[127];
	DWORD dwKeySize = 255;
	DWORD nValue = def_value;

	_stprintf(szRoot, _T("%s\\%s"), root, key);

	if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
		szRoot,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_READ,
		NULL,
		&hk,
		&dwDisp) == ERROR_SUCCESS)
	{

		RegQueryValueEx(hk, item, 0,NULL, 
			(LPBYTE)&nValue, 
			&dwKeySize); 	
	}

	RegCloseKey(hk);

	return nValue;
}
