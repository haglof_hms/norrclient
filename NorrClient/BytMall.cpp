// BytMall.cpp : implementation file
//

#include "stdafx.h"
#include "NorrClient.h"
#include "PricelistParser.h"
#include "RpcProcedures.h"
#include "TemplateParser.h"
#include "BytMall.h"


// CBytMall dialog

IMPLEMENT_DYNAMIC(CBytMall, CDialog)

CBytMall::CBytMall(int nStandId, CWnd* pParent /*=NULL*/)
	: CDialog(CBytMall::IDD, pParent),
	m_nStandId(nStandId)
{

}

CBytMall::~CBytMall()
{
}

void CBytMall::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_template);
}


BEGIN_MESSAGE_MAP(CBytMall, CDialog)
	ON_WM_CREATE()
	ON_CBN_SELCHANGE(IDC_COMBO1, &CBytMall::OnCbnSelchangeTemplate)
END_MESSAGE_MAP()


int CBytMall::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CDialog::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Create template grid
	m_grid.Create(CRect(14, 80, 400, 260), this, IDC_TEMPLATE_GRID);
	m_grid.SetOwner(this);
	m_grid.ShowHelp(FALSE);
	m_grid.SetViewDivider(0.35);
	m_grid.SetTheme(xtpGridThemeOffice2003);

	return 0;
}

BOOL CBytMall::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// List existing templates
	int ct=0;
	if( !_GetTemplateCount(&ct) )
		return FALSE;

	TEMPLATE *pData = new TEMPLATE[ct];
	if( pData )
	{
		if( !_GetTemplateArray(ct, &ct, pData) )
			return FALSE;
		
		for( int i=0; i < ct; i++ )
		{
			int idx = m_template.AddString(pData[i].name);
			m_template.SetItemData(idx, pData[i].id);
		}
	}

	// get texts from language file
	SetDlgItemText(IDC_STATIC_TOP, theApp.m_XML->str(33));
	SetDlgItemText(IDC_STATIC_BERAKNINGSMALL, theApp.m_XML->str(25));

	return TRUE;
}

void CBytMall::OnOK()
{
	if( m_template.GetCurSel() < 0 )
	{
		AfxMessageBox(theApp.m_XML->str(23));
		return;
	}

	if( _ChangeTemplate(m_nStandId, GenerateTemplateXml()) )
	{
		CDialog::OnOK();
	}
	else
	{
		CString msg = GetLastMessage();
		if( !msg.IsEmpty() )
			AfxMessageBox(msg);
	}
}

CStringA CBytMall::GenerateTemplateXml()
{
	CXTPPropertyGridItem *pItem, *pChild;
	LPCSTR lpStr = NULL;
	POSITION pos;
	SPECIE spc;
	SpecieList &spclst = m_tmplParser.GetSpecieList();

	pItem = m_grid.GetCategories()->GetAt(0);
	if( pItem )
	{
		// Go through species
		for( int i=0; i < pItem->GetChilds()->GetCount(); i++ )
		{
			// Find specie in list, update quality id for specie
			pChild = pItem->GetChilds()->GetAt(i);

			pos = spclst.GetHeadPosition();
			while( pos )
			{
				spc = spclst.GetAt(pos);
				if( _tcscmp(pChild->GetCaption(), spc.name) == 0 )
				{
					// Replace specie record
					spc.qualityId = pChild->GetConstraints()->GetConstraintAt(pChild->GetConstraints()->GetCurrent())->m_dwData;
					spclst.RemoveAt(pos);
					spclst.AddTail(spc);
					break;
				}
				spclst.GetNext(pos);
			}
		}

		m_tmplParser.UpdateXml(&lpStr);
	}

	return CStringA(lpStr);
}

BOOL CBytMall::GetPricelistXml(int id)
{
	int size=0;
	wchar_t *pData = new wchar_t[0];

	// Get template xml
	RpcTryExcept
	{
		::GetPricelistXml(id, 0, &size, pData);
		pData = new wchar_t[size];
		if( pData )
		{
			::GetPricelistXml(id, size, &size, pData);
			m_pricelistXml = pData;
		}
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return FALSE;
	}
	RpcEndExcept

	return TRUE;
}

BOOL CBytMall::GetTemplateXml()
{
	int size=0, id;
	wchar_t *pData = new wchar_t[0];

	// Get template xml
	RpcTryExcept
	{
		id = m_template.GetItemData(m_template.GetCurSel());
		::GetTemplateXml(id, 0, &size, pData);
		pData = new wchar_t[size];
		if( pData )
		{
			::GetTemplateXml(id, size, &size, pData);
			m_templateXml = pData;
		}
	}
	RpcExcept(1)
	{
		DisplayException(RpcExceptionCode());
		return FALSE;
	}
	RpcEndExcept

	return TRUE;
}


// CBytMall message handlers

void CBytMall::OnCbnSelchangeTemplate()
{
	CXTPPropertyGridItem *pItem, *pChild;
	CXTPPropertyGridItemConstraint *pConstraint;
	SPECIE spc;
	QDESC qd;
	POSITION pos, p2;

	// Remove any existing categories
	m_grid.GetCategories()->Clear();

	// Get template xml
	if( !GetTemplateXml() )
		return;

	if( !m_tmplParser.ParseXml(m_templateXml) )
		return;

	// Get pricelist template xml
	if( !GetPricelistXml(m_tmplParser.GetPricelistId()) )
		return;

	if( !m_prlParser.ParseXml(m_pricelistXml) )
		return;

	pItem = m_grid.AddCategory(theApp.m_XML->str(26));	//_T("Kvalitetsmallar"));
	pItem->Expand();

	// List species, quality descriptions from template
	SpecieList& spclst = m_tmplParser.GetSpecieList();
	const QDescList& qlst = m_prlParser.GetQDescList();
	pos = spclst.GetHeadPosition();
	while( pos )
	{
		spc = spclst.GetNext(pos);
		pChild = pItem->AddChildItem(new CXTPPropertyGridItem(spc.name));
		
		p2 = qlst.GetHeadPosition();
		while( p2 )
		{
			qd = qlst.GetNext(p2);
			if( qd.spcId == spc.id )
			{
				pConstraint = pChild->GetConstraints()->AddConstraint(qd.name, qd.id);

				// Use value from template as default
				if( qd.id == spc.qualityId )
				{
					pChild->SetValue(qd.name);
					pChild->GetConstraints()->SetCurrent(pConstraint->GetIndex());
				}
			}
		}
		pChild->SetFlags(xtpGridItemHasComboButton);
		pChild->SetConstraintEdit(FALSE);
	}
}
