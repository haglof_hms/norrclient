// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>                      // MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>                     // MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>                     // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


#include <XTToolkitPro.h>

#define IDC_GENERAL_DATA_PANE				0x8101
#define IDC_SETTINGS_PANE					0x8102
#define IDC_TABCONTROL_1					0x8103
#define IDC_TABCONTROL_2					0x8104
#define IDC_REPORT							0x8105
#define IDC_SEL_PROP_REPORT					0x8106
#define ID_COPY_ONE_ROW						0x8107
#define ID_COPY_ROW_MULTIPLE				0x8108
#define IDC_TEMPLATE_GRID					0x8109
#define IDC_TABCONTROL_PLOTS				0x8110

const LPCTSTR sz0dec							= _T("%.0f");
const LPCTSTR sz1dec							= _T("%.1f");

void setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos = FALSE);


//class sample tree
#include <vector>
#include <afxdlgs.h>
class CMyTransactionSampleTree
{
	int m_nSpcId;
	double m_fDbh;	//mm
	double m_fHgt; //dm
	double m_fGCrown;	//dm
	double m_fBrk;	//mm
	double m_fAge;	//years
	double m_fBonitet;
	int m_nType;
	CString m_csTypeName;

public:
	CMyTransactionSampleTree(void)
	{
		m_nSpcId = 1;
		m_fDbh = 0.0;
		m_fHgt = 0.0;
		m_fGCrown = 0.0;
		m_fBrk = 0.0;
		m_fAge = 0.0;
		m_fBonitet = 0.0;
		m_nType = 0;
		m_csTypeName = _T("");
	}

	CMyTransactionSampleTree(int spc, double dbh, double hgt, double gcrown, double brk, double age, double bonitet, int type, LPCTSTR type_name = _T(""))
	{
		m_nSpcId = spc;
		m_fDbh = dbh;
		m_fHgt = hgt;
		m_fGCrown = gcrown;
		m_fBrk = brk;
		m_fAge = age;
		m_fBonitet = bonitet;
		m_nType = type;
		m_csTypeName = type_name;
	}

	CMyTransactionSampleTree(const CMyTransactionSampleTree &c) { *this = c;}

	int getSpcId(void) { return m_nSpcId;}
	double getDbh(void) { return m_fDbh;}
	double getHgt(void) { return m_fHgt;}
	double getGCrown(void) { return m_fGCrown;}
	double getBrk(void) { return m_fBrk;}
	double getAge(void) { return m_fAge;}
	double getBonitet(void) { return m_fBonitet;}
	int getType(void) { return m_nType;}
	CString getTypeName(void) {return m_csTypeName;}

	void setTypeName(CString name) { m_csTypeName = name;}
	void setSpcId(int nSpcId) { m_nSpcId = nSpcId; }
	void setDbh(double fDbh) { m_fDbh = fDbh; }
	void setHgt(double fHgt) { m_fHgt = fHgt; }
	void setGCrown(double fGCrown) { m_fGCrown = fGCrown; }
	void setBrk(double fBrk) { m_fBrk = fBrk; }
	void setAge(double fAge) { m_fAge = fAge; }
	void setBonitet(double fBonitet) { m_fBonitet = fBonitet; }
};

typedef std::vector<CMyTransactionSampleTree> vecMySampleTrees;

// Species transaction classes; 060317 p�d
class CTransaction_species
{
	int m_nID;
	int m_nSpcID;
	CString m_sSpcName;
	CString m_sNotes;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_species(void);

	CTransaction_species(int id,int spc_id = 0,LPCTSTR spc_name = _T(""),LPCTSTR notes = _T(""),LPCTSTR created = _T(""));
	
	// Copy constructor
	CTransaction_species(const CTransaction_species &);

	inline BOOL operator ==(CTransaction_species &c) const
	{
		return (m_nID == c.m_nID && m_nSpcID == c.m_nSpcID && m_sSpcName == c.m_sSpcName && m_sNotes == c.m_sNotes);
	}

	int getID(void);
	int getSpcID(void);
	CString getSpcName(void);
	CString getNotes(void);
	CString getCreated(void);
};

typedef std::vector<CTransaction_species> vecTransactionSpecies;
//typedef std::vector<int> vecIntegers;
typedef std::vector<long> vecInt;

/////////////////////////////////////////////////////////////////////////////////////////////////
// enumrated column values used in:

enum 
{
	COLUMN_0,
	COLUMN_1,
	COLUMN_2,
	COLUMN_3,
	COLUMN_4,
	COLUMN_5,
	COLUMN_6,
	COLUMN_7,
	COLUMN_8,
	COLUMN_9,
	COLUMN_10,
	COLUMN_11,
	COLUMN_12,
	COLUMN_13,
	COLUMN_14,
	COLUMN_15,
	COLUMN_16,
	COLUMN_17,
	COLUMN_18,
	COLUMN_19,
	COLUMN_20,
	COLUMN_21,
	COLUMN_22
};

enum{
	TAB_CUT = 0,
	TAB_STANDING = 1,
//	TAB_CUT_TRACK = 2,
	TAB_SAMPLETREE = 2
};	//tab types

enum{
	DCLS_COL_START						= 0,
	DCLS_COL_END						= 1,
	DCLS_COL_NUM_OF						= 2
};// dcls columns

enum{
	ST_COL_DBH							= 0,
	ST_COL_HGT							= 1,
	ST_COL_GCROWN						= 2,
	ST_COL_BARK							= 3,
	ST_COL_AGE							= 4,
	ST_COL_BONITET						= 5,
	ST_COL_TYPE							= 6,
	MAX_GCROWN							= 100	
};// sample tree variables

enum 
{
	COL_PROP_0,
	COL_PROP_1,
	COL_PROP_2,
	COL_PROP_3
};

#define BLACK				RGB(  0,  0,  0)
#define WHITE				RGB(255,255,255)
#define INFOBK			::GetSysColor(COLOR_INFOBK)

#define NUM_OF_MSPEC 50

struct SPECNAMES
{
	TCHAR name[20];
	int id;
	int use;
};

BOOL getSpecies(vecTransactionSpecies &vec);
CString CheckStringNotForbidden(CString str);
CString getSpcName(vecTransactionSpecies &vec, int spcid);
CString getTmpPath();
LPCTSTR getUserId();
LPCTSTR getUserName();

// structs and vector used for saving/loading data into tabs
typedef struct dcls
{
	int nDclsNum;
	int nAmount;
} DCLS;

typedef struct species
{
	int nSpeciesNum;
	std::vector<DCLS> vecDcls;
	std::vector<CMyTransactionSampleTree> vecSampleTrees;
} SPECIES;

typedef struct treetype
{
	int nTreeTypeNum;
	std::vector<SPECIES> vecSpecies;
} TREETYPE;

typedef struct plot
{
	int nPlotNum;	// plot number
	CString csId;	// plot id (usually same as number)
	int nAreal;		// plot area in m2
	int nOhgt;		// ???
	int nDbhAge;	// ???
	int nBonitet;	// ???
	int nSiTree;	// ???
	int nSi;		// ???
	int nAntal;		// ???

	std::vector<TREETYPE> vecTreeType;
} PLOT;

CString regGetStr_LM(LPCTSTR /*root*/, LPCTSTR /*key*/, LPCTSTR /*item*/, LPCTSTR /*def_str*/);
DWORD regGetInt_LM(LPCTSTR /*root*/, LPCTSTR /*key*/, LPCTSTR /*item*/, int /*def_value*/);
