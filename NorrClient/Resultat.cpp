// Resultat.cpp : implementation file
//

#include "stdafx.h"
#include "NorrClient.h"
#include "Resultat.h"
#include "RpcProcedures.h"
#include "WizardSheet.h"


// CResultat dialog

IMPLEMENT_DYNAMIC(CResultat, CPropertyPage)

CResultat::CResultat()
	: CPropertyPage(CResultat::IDD)
{

}

CResultat::~CResultat()
{
}

void CResultat::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_text);
}


BEGIN_MESSAGE_MAP(CResultat, CPropertyPage)
END_MESSAGE_MAP()


// CResultat message handlers

BOOL CResultat::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	m_text.SetEnabledColor(BLACK, WHITE);
	m_text.SetDisabledColor(BLACK, INFOBK);
	m_text.SetReadOnly();
	

	return TRUE;
}

BOOL CResultat::OnSetActive()
{
	CPropertySheet* pSheet = (CPropertySheet*)GetParent();
	ASSERT_KINDOF(CPropertySheet, pSheet);
	if( ((CWizardSheet*)GetParent())->m_bKomplettera )
		pSheet->SetWizardButtons(PSWIZB_FINISH);
	else
		pSheet->SetWizardButtons(PSWIZB_BACK | PSWIZB_FINISH);

	STANDDATA data;
	if(	_GetStandData(((CWizardSheet*)GetParent())->m_nBestandId, &data) )
	{
		CString str;
		str.Format( _T("Best�nd:\t\t%s\t\tAreal:\t%.3f\r\n")
					_T("Antal stam:\t%d\t\t\tTall\t%.0f %%\r\n")
					_T("GY:\t\t%.0f\t\t\tGran\t%.0f %%\r\n")
					_T("H�jd:\t\t%.0f\t\t%.0f\tBj�rk\t%.0f %%\r\n")
					_T("Da:\t\t%.0f\r\n")
					_T("Volym m3sk:\t%.1f\t\t%.3f\r\n")
					_T("Volym m3fub:\t%.1f\t\t%.3f\r\n")
					_T("Rotnetto:\t\t%d kr\t\t%.0f kr\r\n")
					_T("Kostnad:\t\t%d kr\t\t%.0f kr\r\n"),
					data.name, data.area,
					data.numTrees, data.pine,
					data.gy, data.spruce,
					data.hgv, data.avgHgt, data.birch,
					data.da,
					data.m3sk, data.avgM3sk,
					data.m3fub, data.avgM3fub,
					data.netto, data.m3fub > 0 ? data.netto / data.m3fub : 0,
					data.cost, data.m3fub > 0 ? data.cost / data.m3fub : 0);
		m_text.SetWindowText(str);
	}

	return CPropertyPage::OnSetActive();
}

LRESULT CResultat::OnWizardNext() 
{
	UpdateData(TRUE);

	return CPropertyPage::OnWizardNext();
}

LRESULT CResultat::OnWizardBack()
{
	// radera skapat best�nd om vi backar fr�n resultat-fliken.
	if( ((CWizardSheet*)GetParent())->m_nBestandId != -1)
	{
		if( _RemoveStand( ((CWizardSheet*)GetParent())->m_nBestandId ) )
		{
			((CWizardSheet*)GetParent())->m_nBestandId = -1;
		}
		else
		{
			AfxMessageBox(/*_T("Kunde inte radera tidigare skapat best�nd!")*/ theApp.m_XML->str(22), MB_ICONERROR);
		}
	}


	return CPropertyPage::OnWizardBack();
}

int CResultat::OnWizardFinish()
{
	return CPropertyPage::OnWizardFinish();
}

BOOL CResultat::OnQueryCancel()
{
	if( AfxMessageBox(theApp.m_XML->str(41), MB_YESNO|MB_ICONEXCLAMATION) != IDYES )
	{
		return FALSE;
	}

	return TRUE;
}

void CResultat::OnCancel()
{
	// radera skapat best�nd
	if( ((CWizardSheet*)GetParent())->m_nBestandId != -1 && !((CWizardSheet*)GetParent())->m_bKomplettera )
	{

		if( _RemoveStand( ((CWizardSheet*)GetParent())->m_nBestandId ) )
		{
			((CWizardSheet*)GetParent())->m_nBestandId = -1;
		}
		else
		{
			AfxMessageBox(/*_T("Kunde inte radera tidigare skapat best�nd!")*/ theApp.m_XML->str(22), MB_ICONERROR);
		}
	}

	CPropertyPage::OnCancel();
}
