// Indata.cpp : implementation file
//

#include "stdafx.h"
#include "NorrClient.h"
#include "Indata.h"
#include "StandTableDlg.h"
#include "WizardSheet.h"
#include "HXLParser.h"

// CIndata dialog

IMPLEMENT_DYNAMIC(CIndata, CPropertyPage)

CIndata::CIndata()
	: CPropertyPage(CIndata::IDD)
{
	m_nInventoryType = -1;
	m_nSelectedItem = -1;
}

CIndata::~CIndata()
{
}

void CIndata::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_FILES, m_clFiles);
	DDX_Control(pDX, IDC_LIST_DESC, m_clDesc);
}


BEGIN_MESSAGE_MAP(CIndata, CPropertyPage)
	ON_BN_CLICKED(IDC_BUTTON_LOAD, OnBnClickedButtonLoad)
	ON_BN_CLICKED(IDC_BUTTON_NEW, OnBnClickedButtonNew)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, OnBnClickedButtonRemove)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_FILES, &CIndata::OnLvnItemchangedListFiles)
	ON_BN_CLICKED(IDC_BUTTON_EDIT, &CIndata::OnBnClickedButtonEdit)
END_MESSAGE_MAP()


// CIndata message handlers

BOOL CIndata::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	m_clFiles.InsertColumn(0, _T("1"));
	m_clFiles.SetColumnWidth(0, 200);

	m_clDesc.InsertColumn(0, _T("1"));
	m_clDesc.SetColumnWidth(0, 80);
	m_clDesc.InsertColumn(1, _T("2"));
	m_clDesc.SetColumnWidth(1, 150);

	m_clDesc.InsertItem(0, theApp.m_XML->str(10));	//_T("Provtr�d:"));
	m_clDesc.InsertItem(0, theApp.m_XML->str(9));	//_T("Antal tr�d:"));
	m_clDesc.InsertItem(0, theApp.m_XML->str(8));	//_T("Breddgrad:"));
	m_clDesc.InsertItem(0, theApp.m_XML->str(7));	//_T("Altitud:"));
	m_clDesc.InsertItem(0, theApp.m_XML->str(6));	//_T("Bonitet H100:"));
	m_clDesc.InsertItem(0, theApp.m_XML->str(5));	//_T("�lder:"));
	m_clDesc.InsertItem(0, theApp.m_XML->str(4));	//_T("Datum:"));
	m_clDesc.InsertItem(0, theApp.m_XML->str(3));	//_T("Areal Ha:"));
	m_clDesc.InsertItem(0, theApp.m_XML->str(2));	//_T("Delbest�nd:"));
	m_clDesc.InsertItem(0, theApp.m_XML->str(1));	//_T("Best�nd:"));

	SetDlgItemText(IDC_STATIC_TOP, theApp.m_XML->str(16));
	SetDlgItemText(IDC_STATIC_INDATAFILER, theApp.m_XML->str(14));
	SetDlgItemText(IDC_STATIC_BESKRIVNING, theApp.m_XML->str(15));

	return TRUE;
}

BOOL CIndata::OnSetActive()
{
	SetWizardButtons();

	return CPropertyPage::OnSetActive();
}

LRESULT CIndata::OnWizardNext() 
{
	UpdateData(TRUE);

	return CPropertyPage::OnWizardNext();
}

void CIndata::OnBnClickedButtonLoad()
{
	// v�lj inventeringsfil(er)
	CFileDialog dlgF(TRUE);

	TCHAR* pszFile = new TCHAR[32767];	// buffert f�r filnamn
	ZeroMemory(pszFile, 32767*sizeof(TCHAR));
	dlgF.m_ofn.lpstrFile = pszFile;
	dlgF.m_ofn.nMaxFile = 32767;
	dlgF.m_ofn.nFileOffset = 0;

	dlgF.m_ofn.lpstrFilter = (LPTSTR)_T("HXL (*.hxl)\0*.hxl\0\0");
	dlgF.m_ofn.hwndOwner = AfxGetMainWnd()->GetSafeHwnd();
	dlgF.m_ofn.Flags = OFN_EXPLORER|OFN_ALLOWMULTISELECT|OFN_ENABLEHOOK|OFN_HIDEREADONLY;
	dlgF.m_ofn.lpstrDefExt = _T(".hxl");

	if(dlgF.DoModal() != IDOK)
	{
		delete[] pszFile;
		return;
	}

	// l�gg till fil(er) till fillistan
	POSITION pos ( dlgF.GetStartPosition() );

	HXLHEADER hxlHeader;
	BOOL bExist;

	// h�mta ut alla filer, ta inte med samma filnamn flera g�nger
	while( pos )
	{
		CString csFileName( dlgF.GetNextPathName(pos) );

		bExist = FALSE;
		for(int nLoop=0; nLoop<m_caFiles.GetSize(); nLoop++)
		{
			if(csFileName == m_caFiles.GetAt(nLoop))
			{
				bExist = TRUE;
				break;
			}
		}

		if(bExist == FALSE)
		{
			// l�s in headerdata fr�n HXL och l�gg till i fillista
			CHXLParser *pHxl = new CHXLParser();
			if(pHxl->getHeaderData(csFileName, &hxlHeader) != 0)
			{
				delete[] pszFile;
				return;
			}
			delete pHxl;

			// Kontrollera att inventeringstyp �r samma p� alla filer.
			if(hxlHeader.nInventeringsmetod == m_nInventoryType || m_clFiles.GetItemCount() == 0)
			{
				m_caFiles.Add(csFileName);

				// s�tt inventeringstyp fr�n f�rsta filen
				m_nInventoryType = hxlHeader.nInventeringsmetod;

				((CWizardSheet*)GetParent())->m_csaHxlFiles.Add(hxlHeader);

				int nPos = m_clFiles.InsertItem(m_clFiles.GetItemCount()+1, csFileName.Right( csFileName.GetLength() - csFileName.ReverseFind('\\') - 1) );
				m_clFiles.SetItemState(nPos, LVIS_SELECTED, LVIS_SELECTED);
			}
			else
			{
				AfxMessageBox(theApp.m_XML->str(28));	//_T("Fil har inte samma inventeringstyp som f�reg�ende filer!"));
			}
		}
	}

	delete[] pszFile;

	SetWizardButtons();
}

void CIndata::OnBnClickedButtonNew()
{
	// visa manuell st�mplingsl�ngd
	CStandTableDlg dlg(_T(""), m_nInventoryType);
	CHXLParser hxl;
	HXLHEADER hxlHeader;

	if(dlg.DoModal() == IDOK)
	{
		// l�s in headerdata fr�n HXL och l�gg till i fillista
		if(hxl.getHeaderData(dlg.getFilename(), &hxlHeader) != 0)
			return;

		// Kontrollera att inventeringstyp �r samma p� alla filer.
		if(hxlHeader.nInventeringsmetod == m_nInventoryType || m_clFiles.GetItemCount() == 0)
		{
			// l�gg till inventeringsfil i intern fillista
			m_caFiles.Add(dlg.getFilename());

			// s�tt inventeringstyp fr�n f�rsta filen
			m_nInventoryType = hxlHeader.nInventeringsmetod;

			((CWizardSheet*)GetParent())->m_csaHxlFiles.Add(hxlHeader);

			int nPos = m_clFiles.InsertItem(m_clFiles.GetItemCount()+1, dlg.getFilename().Right( dlg.getFilename().GetLength() - dlg.getFilename().ReverseFind('\\') - 1 ) );
			m_clFiles.SetItemState(nPos, LVIS_SELECTED, LVIS_SELECTED);

			SetWizardButtons();
		}
		else
		{
			AfxMessageBox(theApp.m_XML->str(28));	//_T("Fil har inte samma inventeringstyp som f�reg�ende filer!"));
		}
	}
}

void CIndata::OnBnClickedButtonRemove()
{
	// radera inventeringsfil(er) fr�n rapportkontroll och fillistan
	POSITION pos = m_clFiles.GetFirstSelectedItemPosition();
	if (pos != NULL)
	{
		int i = 0;
		int NOF_Item = m_clFiles.GetSelectedCount();
		int *arrItem = new int[NOF_Item+1];

		while(pos)
		{
			arrItem[i] = m_clFiles.GetNextSelectedItem(pos);
			i++;
		}

		for(int k=0; k<i; k++)
		{
			m_clFiles.DeleteItem(arrItem[k]-k);
			((CWizardSheet*)GetParent())->m_csaHxlFiles.RemoveAt(arrItem[k]-k);

			// ta bort inventeringsfil fr�n den interna fillistan
			m_caFiles.RemoveAt(arrItem[k]-k);
		}

		delete []arrItem;
	}

	// l�t inventeringsmetod vara ol�st vid redigering
	if(m_caFiles.GetSize() == 0)
		m_nInventoryType = -1;

	ClearDesc();
	SetWizardButtons();
}

void CIndata::OnLvnItemchangedListFiles(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	if( m_clFiles.GetSelectedCount() )
	{
		// anv�ndaren har valt en fil, populera beskrivning med data fr�n den
		if(pNMLV->uNewState & LVIS_SELECTED)
		{
			m_nSelectedItem = pNMLV->iItem;
			UpdateDesc();
		}
	}
	else
	{
		ClearDesc();
		m_nSelectedItem = -1;
	}

	*pResult = 0;
}

void CIndata::UpdateDesc()
{
	CString csBuf;
	HXLHEADER hxlHeader;
	hxlHeader = ((CWizardSheet*)GetParent())->m_csaHxlFiles.GetAt(m_nSelectedItem);

	// best�ndsnamn
	m_clDesc.SetItemText(0, 1, hxlHeader.csBestand);

	// delbest�nd
	csBuf.Format(_T("%d"), hxlHeader.nDelbestand);
	m_clDesc.SetItemText(1, 1, csBuf);

	// areal
	csBuf.Format(_T("%.3f"), hxlHeader.nAreal / 10000.0);
	m_clDesc.SetItemText(2, 1, csBuf);

	// datum
	m_clDesc.SetItemText(3, 1, hxlHeader.csDatum);

	// �lder
	csBuf.Format(_T("%d"), hxlHeader.nBestAlder);
	m_clDesc.SetItemText(4, 1, csBuf);

	// bonitet
	m_clDesc.SetItemText(5, 1, hxlHeader.csSI);

	// altitud
	csBuf.Format(_T("%d"), hxlHeader.nAltitud);
	m_clDesc.SetItemText(6, 1, csBuf);

	// breddgrad
	csBuf.Format(_T("%d"), hxlHeader.nLatitud);
	m_clDesc.SetItemText(7, 1, csBuf);

	// antal tr�d
	csBuf.Format(_T("%d"), hxlHeader.nNumTrees);
	m_clDesc.SetItemText(8, 1, csBuf);

	// antal provtr�d
	csBuf.Format(_T("%d"), hxlHeader.nNumSampleTrees);
	m_clDesc.SetItemText(9, 1, csBuf);


	m_clDesc.UpdateWindow();
}

void CIndata::ClearDesc()
{
	for(int nLoop=0; nLoop<m_clDesc.GetItemCount(); nLoop++)
	{
		m_clDesc.SetItemText(nLoop, 1, _T(""));
	}
}

void CIndata::SetWizardButtons()
{
	CPropertySheet* pSheet = (CPropertySheet*)GetParent();
	ASSERT_KINDOF(CPropertySheet, pSheet);

	if(m_clFiles.GetItemCount() == 0)
		pSheet->SetWizardButtons(PSWIZB_BACK);
	else
		pSheet->SetWizardButtons(PSWIZB_BACK | PSWIZB_NEXT);
}

void CIndata::OnBnClickedButtonEdit()
{
	// redigera befintlig inventeringsfil
	HXLHEADER hxlHeader;
	if(m_nSelectedItem != -1)
	{
		hxlHeader = ((CWizardSheet*)GetParent())->m_csaHxlFiles.GetAt( m_nSelectedItem );

		CStandTableDlg dlg(hxlHeader.csFilePath, m_nInventoryType);
		if(dlg.DoModal() == IDOK)
		{
			// reload file information
			CHXLParser hxl;
			hxl.getHeaderData(hxlHeader.csFilePath, &hxlHeader);
			((CWizardSheet*)GetParent())->m_csaHxlFiles.SetAt(m_nSelectedItem, hxlHeader);

			// update the header information box
			UpdateDesc();
		}
	}
}
