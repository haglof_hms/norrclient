#pragma once
#include "afxcmn.h"
#include <XTToolkitPro.h>
#include "afxwin.h"
#include "PricelistParser.h"
#include "TemplateParser.h"


// CBytMall dialog

class CBytMall : public CDialog
{
	DECLARE_DYNAMIC(CBytMall)

public:
	CBytMall(int nStandId, CWnd* pParent = NULL);   // standard constructor
	virtual ~CBytMall();

	CStringA GenerateTemplateXml();

// Dialog Data
	enum { IDD = IDD_BYT_MALL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual void OnOK();

	BOOL GetPricelistXml(int id);
	BOOL GetTemplateXml();

	afx_msg void OnCbnSelchangeTemplate();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

	DECLARE_MESSAGE_MAP()
	CXTPPropertyGrid m_grid;
	CComboBox m_template;
	CStringA m_templateXml;
	CStringA m_pricelistXml;
	CPricelistParser m_prlParser;
	CTemplateParser m_tmplParser;
	int m_nStandId;
};
