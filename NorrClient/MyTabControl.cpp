//////////////////////////////////////////////////////////////////////////////////
// 	CMyTabControl

#include "stdafx.h"
#include "MyTabControl.h"

CMyTabControl::CMyTabControl()
	: CXTPTabControl()
{
}

void CMyTabControl::RedrawControl(LPCRECT lpRect,BOOL bAnimate)
{
	CXTPTabControl::RedrawControl(lpRect,bAnimate);
}

void CMyTabControl::OnItemClick(CXTPTabManagerItem* pItem)
{
	CXTPTabControl::OnItemClick(pItem);

/*	CXTPTabManager *pTabManager = pItem->GetTabManager();
	CXTPTabManagerItem *pTabPage = NULL;
	if (pTabManager)
	{
		pTabPage = GetSelectedItem();
		if (pTabPage)
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, 
										 ID_WPARAM_VALUE_FROM + 0xFA,
										 pTabPage->GetIndex());
		}	// if (pTabPage)
	}	// if (pTabManager)
*/
}

CXTPTabManagerItem *CMyTabControl::getSelectedTabPage(void)
{

	CXTPTabManagerItem *pTabPage = NULL;
	pTabPage = GetSelectedItem();
	if (pTabPage)
	{
		return pTabPage;
	}

	return NULL;
}

CXTPTabManagerItem *CMyTabControl::getTabPage(int idx)
{
	// Make sure the index (idx) is less than tabs; 060405 p�d
	if (idx < GetItemCount())
	{
		CXTPTabManagerItem *pTabPage = NULL;
		pTabPage = GetItem(idx);
		if (pTabPage)
		{
			return pTabPage;
		}
	}
	return NULL;
}

int CMyTabControl::getNumOfTabPages(void)
{
	return GetItemCount();
}

/////////////////////////////////////////////////////////////////////////////
// CMyExtEdit

//	Defines
//
#define BLANK_ENTRY		-1

BEGIN_MESSAGE_MAP(CMyExtEdit, CXTEdit)
	//{{AFX_MSG_MAP(CMyExtEdit)
	ON_WM_KEYUP()
	ON_WM_CTLCOLOR_REFLECT()
	ON_CONTROL_REFLECT(EN_UPDATE, OnUpdate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMyExtEdit::CMyExtEdit()

{
  // defaults
  m_crFGEnabled = ENABLED_FG;
  m_crBGEnabled = ENABLED_BG;
  m_crFGDisabled = DISABLED_FG;
  m_crBGDisabled = DISABLED_BG;

  // to changes the colors dynamic
  m_pbrushEnabled = new CBrush;
  m_pbrushDisabled = new CBrush;
  m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
  m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);

	m_bModified = FALSE;
	m_bIsNumeric = FALSE;
	m_bIsAsH100	= FALSE;
	m_bIsValidate = FALSE;

	m_nItemData = -1;
	m_sItemData = _T("");

	m_nMin  = 0;		//	Initialize member variables to zero
	m_nMax	= 99999;
	m_nLastValidValue = 0;

	szValue[0] = '\0';
	szIdentifer[0] = '\0';
}

CMyExtEdit::~CMyExtEdit()
{
  delete m_pbrushEnabled;
  delete m_pbrushDisabled;
}

BOOL CMyExtEdit::PreTranslateMessage(MSG *pMSG)
{
	UINT  nKeyCode = pMSG->wParam; // virtual key code of the key pressed
	if (pMSG->message == WM_KEYDOWN)
	{
		if (nKeyCode < 65)
		{
			return 0;
		}

	  // CTRL+C, CTRL+X, or, CTRL+V?
	  if ( nKeyCode == _T('C') && (::GetKeyState(VK_CONTROL) & 0x8000) )
	  {
		  Copy();
	  }
	  else if( nKeyCode == _T('X') && (::GetKeyState(VK_CONTROL) & 0x8000) )
	  {
		  Cut();
	  }
	  else if( nKeyCode == _T('V') && (::GetKeyState(VK_CONTROL) & 0x8000) )
	  {
		  Paste();
	  }

	  // Only accept numberic input?
	  if (m_bIsNumeric)
	  {
		if (isNumeric(nKeyCode))
		{
			return 0;
		}
		else
		{
			return 1;
		}
	  }
	  else
	  {
			return 0;
	  }
	}

	return CXTEdit::PreTranslateMessage(pMSG);
}

void CMyExtEdit::RemoveLeadingZeros ( void )
{
	if (m_bIsNumeric && m_bIsValidate)
	{
		//	Local Variables
		CString strWindowText;
		SHORT index;
		//	Initialize Variables
		strWindowText.Empty ( );
		index = 0;
		//	Copy the contents of the edit control
		//		to a string
		GetWindowText ( strWindowText );
		//	Check if the length of the text in the edit
		//		control is greater than 1
		//
		//	If the length is 1 then there can not be any
		//		leading zeroes
		//
		if ( strWindowText.GetLength ( ) > 1 )
		{
			//	Get the index of the first
			//		non-zero character
			//
			index = strWindowText.FindOneOf ( _T("123456789") );	
			//	Check if there are any leading zeroes
			//		if the first non-zero character
			//		is at any position but 0, then
			//		there are leading zeros
			//
			if ( index > 0 )
			{
				//	Leading zeroes were found
				//		Remove them
				//
				strWindowText = strWindowText.Mid ( index );
			}
			//	Check if no non-zero characters were found
			//		That means there are only zeros in the
			//		string
			//
			else  if ( index == -1 )
			{
				//	There is only zeroes in the string
				//		change the string to only one zero
				//
				strWindowText = "0";
			}
			//	Check if the text was formatted, the index is not 0
			//
			if ( index != 0 )
			{
				//	The text was formatted, update the edit controls text
				//
				SetWindowText ( strWindowText );
				//	Set the cursor position
				//
				SetSel ( 0, 0 );
			}
		}
	} // if (m_bIsNumeric)
}

/////////////////////////////////////////////////////////////////////////////
// CMyExtEdit message handlers

void CMyExtEdit::PreSubclassWindow() 
{
	CXTEdit::PreSubclassWindow();
}

void CMyExtEdit::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	char tmp[120];
	if (nChar == VK_TAB && !m_bModified)
		m_bModified = FALSE;
	else
		m_bModified = TRUE;
	BOOL bControlKey = (GetKeyState(VK_CONTROL) < 0);
	CXTEdit::OnKeyUp(nChar,nRepCnt,nFlags);
}


void CMyExtEdit::SetAsNumeric(void)
{
	m_bIsNumeric = TRUE;
}

void CMyExtEdit::SetAsH100(void)
{
	m_bIsAsH100 = TRUE;
}

HBRUSH CMyExtEdit::CtlColor(CDC* pDC, UINT nCtlColor)
{
   // Setting color in en- disabled mode
   if(GetStyle() & ES_READONLY)
   {
      pDC->SetTextColor(m_crFGDisabled);
      pDC->SetBkColor(m_crBGDisabled);
      return *m_pbrushDisabled;
   }
   else
   {
      pDC->SetTextColor(m_crFGEnabled);
      pDC->SetBkColor(m_crBGEnabled);
      return *m_pbrushEnabled;
   }
}

void CMyExtEdit::OnUpdate() 
{
	if (m_bIsNumeric && m_bIsValidate)
	{
		//	Local Variables
		CString strWindowText;
		//	Initialize Variables
		strWindowText.Empty ( );
		LONG ValidationNumber;
		ValidationNumber = 0;
		//	Remove the leading zeros from the	edit control
		RemoveLeadingZeros();
		//	Copy the contents of the edit control to a string
		GetWindowText ( strWindowText );
		//	Check if the edit control is blank
		if ( strWindowText.IsEmpty ( ) )
		{
			//	The operator erased the last number.  Thus the last valid value
			//		entered by the operator is NULL.
			m_nLastValidValue = BLANK_ENTRY;
		}
		else
		{
			//	There is a number to validate in the string
			//	Convert the string to a LONG
			ValidationNumber = _tstol ( strWindowText );
			//	Check if the number falls within the specified range
			if ( ( ValidationNumber < m_nMin ) || ( ValidationNumber > m_nMax ) )
			{
				//	The number is invalid, it is out of the specified range
				//	Check what type of value the last Valid Value is:
				//		1.	A number:	which is indicated by a value greater than
				//						or equal to 0.
				//
				//		2.	NULL:		which is indicated by BLANK_ENTRY (-1).
				//
				if ( m_nLastValidValue >= 0 )
				{
					//	The last value is a number
					//
					strWindowText.Format ( _T("%d"), m_nLastValidValue );
				}
				else
				{
					//	The last value was a NULL
					//
					strWindowText.Empty ( );
				}
				//	Set the Edit control with the Last Valid Value
				//
				SetWindowText ( strWindowText );
				//	Place the cursor at the end of the string
				//
				SetSel ( strWindowText.GetLength ( ), strWindowText.GetLength ( ) );
				//	Alert the operator of the error
				//
				MessageBeep ( 0xFFFFFFFF );
			}
			else
			{
				//	The number is valid, that is, it is in the specified range
				//
				//	Set the last valid value to the current number 
				//		in the edit control
				//
				m_nLastValidValue = ValidationNumber;
			}
		}
	}	// if (m_bIsNumeric)
	this->RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN | RDW_UPDATENOW);
}

void CMyExtEdit::SetReadOnly(BOOL flag)
{
		if (flag == TRUE)
		{
			ModifyStyle(WS_TABSTOP,0);
		}
		else
		{
			ModifyStyle(0,WS_TABSTOP);
		}
		Invalidate(TRUE);

		CXTEdit::SetReadOnly( flag );
}

void CMyExtEdit::SetEnabledColor(COLORREF crFG, COLORREF crBG)
{
   // Setting the colors and the brush
   m_crFGEnabled = crFG;
   m_crBGEnabled = crBG;
   delete m_pbrushEnabled;
   m_pbrushEnabled = new CBrush;
   m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
}

void CMyExtEdit::SetDisabledColor(COLORREF crFG, COLORREF crBG)
{
  // Setting the colors and the brush
  m_crFGDisabled = crFG;
  m_crBGDisabled = crBG;
  delete m_pbrushDisabled;
  m_pbrushDisabled = new CBrush;
  m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);
}

void CMyExtEdit::SetFontEx(int size,int weight,BOOL underline,BOOL italic)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	CFont* cf = GetFont();
	if (cf)
		cf->GetObject(sizeof(lf), &lf);
	else
		GetObject(GetStockObject(SYSTEM_FONT), sizeof(lf), &lf);

	if (size > -1) lf.lfHeight = size;
	lf.lfWeight = weight;
	lf.lfUnderline = underline;
	lf.lfItalic = italic;

	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}

/////////////////////////////////////////////////////////////////////////////
// CMyExtStatic

CMyExtStatic::CMyExtStatic()
{
	m_crBkColor = ::GetSysColor(COLOR_3DFACE); // Initializing background color to the system face color.
	m_crTextColor = BLACK; // Initializing text color to black
	m_brBkgnd.CreateSolidBrush(m_crBkColor); // Creating the Brush Color For the Edit Box Background
	m_fnt1 = new CFont();
}

CMyExtStatic::~CMyExtStatic()
{
	if (m_fnt1 != NULL)
		delete m_fnt1;
}


BEGIN_MESSAGE_MAP(CMyExtStatic, CStatic)
	//{{AFX_MSG_MAP(CMyExtStatic)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMyExtStatic message handlers

void CMyExtStatic::SetTextColor(COLORREF crColor)
{
	m_crTextColor = crColor; // Passing the value passed by the dialog to the member varaible for Text Color
	RedrawWindow();
}

void CMyExtStatic::SetBkColor(COLORREF crColor)
{
	m_crBkColor = crColor; // Passing the value passed by the dialog to the member varaible for Backgound Color
	m_brBkgnd.DeleteObject(); // Deleting any Previous Brush Colors if any existed.
	m_brBkgnd.CreateSolidBrush(crColor); // Creating the Brush Color For the Edit Box Background
	RedrawWindow();
}

HBRUSH CMyExtStatic::CtlColor(CDC* pDC, UINT nCtlColor)
{
	HBRUSH hbr;
	hbr = (HBRUSH)m_brBkgnd; // Passing a Handle to the Brush
	pDC->SetBkColor(m_crBkColor); // Setting the Color of the Text Background to the one passed by the Dialog
	pDC->SetTextColor(m_crTextColor); // Setting the Text Color to the one Passed by the Dialog

	if (nCtlColor)       // To get rid of compiler warning
      nCtlColor += 0;

	return hbr;
}

void CMyExtStatic::SetLblFont(int size,int weight,LPTSTR font_name)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight = size;
	lf.lfWeight = weight;

	if (font_name != _T(""))
		_tcscpy(lf.lfFaceName,font_name);

	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}

void CMyExtStatic::SetLblFontEx(int size,int weight,BOOL underline,BOOL italic)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	CFont* cf = GetFont();
	if (cf)
		cf->GetObject(sizeof(lf), &lf);
	else
		GetObject(GetStockObject(SYSTEM_FONT), sizeof(lf), &lf);

	if (size > -1) lf.lfHeight = size;
	lf.lfWeight = weight;
	lf.lfUnderline = underline;
	lf.lfItalic = italic;

	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}
