#pragma once
#include "afxcmn.h"
#include <XTToolkitPro.h>
#include "afxwin.h"
#include "PricelistParser.h"
#include "TemplateParser.h"


// CValjMall dialog

class CValjMall : public CPropertyPage
{
	DECLARE_DYNAMIC(CValjMall)

public:
	CValjMall();
	virtual ~CValjMall();
	virtual BOOL OnSetActive();

	CString GenerateTemplateXml();

// Dialog Data
	enum { IDD = IDD_WIZARD_VALJ_MALL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual LRESULT OnWizardNext();
	virtual BOOL OnInitDialog();

	BOOL GetPricelistXml(int id);
	BOOL GetTemplateXml();

	void SetWizardButtons();

	afx_msg void OnCbnSelchangeTemplate();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

	DECLARE_MESSAGE_MAP()
	CXTPPropertyGrid m_grid;
	CComboBox m_template;
	CStringA m_templateXml;
	CStringA m_pricelistXml;
	CPricelistParser m_prlParser;
	CTemplateParser m_tmplParser;
};
