#pragma once

#include <tinyxml.h>

struct QDESC
{
	int id;
	int spcId;
	CString name;
};

typedef CList<QDESC, QDESC&> QDescList;


class CPricelistParser
{
public:
	CPricelistParser();
	virtual ~CPricelistParser();

	BOOL ParseXml(LPCSTR xml);

	QDescList& GetQDescList() { return m_qlst; }

protected:
	QDescList m_qlst;

	TiXmlDocument m_doc;
};
