// Header.cpp : implementation file
//

#include "stdafx.h"
#include "NorrClient.h"
#include "Header.h"
#include "HXLParser.h"
#include "RpcProcedures.h"
#include "WizardSheet.h"

// CHeader dialog

IMPLEMENT_DYNAMIC(CHeader, CPropertyPage)

CHeader::CHeader()
	: CPropertyPage(CHeader::IDD)
	, m_csBestand(_T(""))
	, m_csDelbestand(_T(""))
	, m_ctDatum(0)
	, m_nAltitud(0)
	, m_fAreal(0)
	, m_nAlder(0)
	, m_csSI(_T(""))
	, m_nBreddgrad(0)
{

}

CHeader::~CHeader()
{
}

void CHeader::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_BESTAND, m_csBestand);
	DDX_Text(pDX, IDC_EDIT_DELBESTAND, m_csDelbestand);
	DDX_DateTimeCtrl(pDX, IDC_DATE, m_ctDatum);
	DDX_Text(pDX, IDC_EDIT_ALTITUD, m_nAltitud);
	DDX_Text(pDX, IDC_EDIT_AREAL, m_fAreal);
	DDX_Text(pDX, IDC_EDIT_ALDER, m_nAlder);
	DDX_Text(pDX, IDC_EDIT_BONITET, m_csSI);
	DDX_Text(pDX, IDC_EDIT_BREDDGRAD, m_nBreddgrad);
}


BEGIN_MESSAGE_MAP(CHeader, CPropertyPage)
END_MESSAGE_MAP()


// CHeader message handlers

BOOL CHeader::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	SetDlgItemText(IDC_STATIC_TOP, theApp.m_XML->str(17));
	SetDlgItemText(IDC_STATIC_BESTAND, theApp.m_XML->str(1));
	SetDlgItemText(IDC_STATIC_DELBESTAND, theApp.m_XML->str(2));
	SetDlgItemText(IDC_STATIC_DATUM, theApp.m_XML->str(4));
	SetDlgItemText(IDC_STATIC_ALTITUD, theApp.m_XML->str(7));
	SetDlgItemText(IDC_STATIC_AREAL, theApp.m_XML->str(3));
	SetDlgItemText(IDC_STATIC_ALDER, theApp.m_XML->str(5));
	SetDlgItemText(IDC_STATIC_BONITET, theApp.m_XML->str(6));
	SetDlgItemText(IDC_STATIC_BREDDGRAD, theApp.m_XML->str(8));

	return TRUE;
}

BOOL CHeader::OnSetActive()
{
	CPropertySheet* pSheet = (CPropertySheet*)GetParent();
	ASSERT_KINDOF(CPropertySheet, pSheet);
	pSheet->SetWizardButtons(PSWIZB_BACK | PSWIZB_NEXT);

	// h�mta ut info fr�n den f�rsta HXL-filen
	if(((CWizardSheet*)GetParent())->m_csaHxlFiles.GetCount() > 0)
	{
		HXLHEADER hxlHeader;
		hxlHeader = ((CWizardSheet*)GetParent())->m_csaHxlFiles.GetAt(0);

		// kolla om det �r en ny fil som �r f�rsta HXL-fil i listan
		if(hxlHeader.csFilePath != m_hxlHeader.csFilePath)
		{
			// uppdatera f�lt
			m_hxlHeader = hxlHeader;

			m_csBestand = m_hxlHeader.csBestand;
			m_csDelbestand.Format(_T("%d"), m_hxlHeader.nDelbestand);

			if(m_hxlHeader.csDatum.GetLength() == 8)	// YYYYMMDD
			{
				int nYear, nMonth, nDay;
				nYear = _wtoi(m_hxlHeader.csDatum.Left(4));
				nMonth = _wtoi(m_hxlHeader.csDatum.Mid(4, 2));
				nDay = _wtoi(m_hxlHeader.csDatum.Mid(6, 2));
				m_ctDatum = CTime(nYear, nMonth, nDay, 0, 0, 0, -1);
			}

			if(m_hxlHeader.nAltitud == 0)
				m_nAltitud = ((CWizardSheet*)GetParent())->m_nAltitude;
			else
				m_nAltitud = m_hxlHeader.nAltitud;
			m_fAreal = m_hxlHeader.nAreal / 10000.0;
			m_nAlder = m_hxlHeader.nBestAlder;

			if(m_hxlHeader.csSI == _T(""))
				m_csSI = ((CWizardSheet*)GetParent())->m_csSI;
			else
				m_csSI = m_hxlHeader.csSI;

			if(m_hxlHeader.nLatitud == 0)
				m_nBreddgrad = ((CWizardSheet*)GetParent())->m_nLatitude;
			else
				m_nBreddgrad = m_hxlHeader.nLatitud;

			UpdateData(FALSE);
		}
	}

	// komplettering av best�nd?
	if(((CWizardSheet*)GetParent())->m_bKomplettera)
	{
		// disabla alla kontrollers
		GetDlgItem(IDC_EDIT_BESTAND)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_DELBESTAND)->EnableWindow(FALSE);
		GetDlgItem(IDC_DATE)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_ALTITUD)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_AREAL)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_ALDER)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_BONITET)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_BREDDGRAD)->EnableWindow(FALSE);
			
		GetDlgItem(IDC_STATIC_TOP)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_BESTAND)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_DELBESTAND)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_DATUM)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_ALTITUD)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_AREAL)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_ALDER)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_BONITET)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_BREDDGRAD)->EnableWindow(FALSE);


		// �ndra font
		CFont LabelFont;
		LabelFont.CreateFont(20,20,0,0,FW_BOLD,FALSE,FALSE,0,DEFAULT_CHARSET,  OUT_CHARACTER_PRECIS,CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH,NULL);

		GetDlgItem(IDC_STATIC_KOMPLETTERA)->SetFont(&LabelFont);
		SetDlgItemText(IDC_STATIC_KOMPLETTERA, theApp.m_XML->str(42));	//
		GetDlgItem(IDC_STATIC_KOMPLETTERA)->ShowWindow(TRUE);
	}


	return CPropertyPage::OnSetActive();
}

LRESULT CHeader::OnWizardBack()
{
	m_bDontChangePage = FALSE;

	return CPropertyPage::OnWizardBack();
}

LRESULT CHeader::OnWizardNext() 
{
	UpdateData(TRUE);

	// kolla att alla v�rden �r ifyllda och ligger inom r�tt omr�de
	m_bDontChangePage = FALSE;
	BOOL bError = FALSE;
	CString csBuf;

	if(!((CWizardSheet*)GetParent())->m_bKomplettera)
	{
		if(m_csBestand == _T(""))
		{
			bError = TRUE;
			csBuf += theApp.m_XML->str(18) + _T("\n");	//_T("Best�ndsnamn saknas!\n");
		}

		if(m_nAltitud < 0)
		{
			bError = TRUE;
			csBuf += theApp.m_XML->str(19) + _T("\n");	//_T("Altitud m�ste vara st�rre eller lika med noll!\n");
		}

		if((m_csSI.GetAt(0) != 'T' && m_csSI.GetAt(0) != 'G' &&
			m_csSI.GetAt(0) != 'B') || m_csSI.GetLength() != 3)
		{
			bError = TRUE;
			csBuf += theApp.m_XML->str(20) + _T("\n");	//_T("SI anges i formen XYY, d�r X �r T, G eller B och YY �r h�jden i meter!\n");
		}


		if(m_nBreddgrad < 55 || m_nBreddgrad > 70)
		{
			bError = TRUE;
			csBuf += theApp.m_XML->str(21) + _T("\n");	//_T("Breddgrad m�ste vara mellan 1 och 2!\n");
		}

		if(m_fAreal < 0.001)
		{
			bError = TRUE;
			csBuf += theApp.m_XML->str(27) + _T("\n");	//_T("Areal m�ste vara st�rre �n 0!\n");
		}


		if(m_nAlder <= 0)
		{
			bError = TRUE;
			csBuf += theApp.m_XML->str(32) + _T("\n");	//_T("�lder m�ste vara st�rre �n 0!\n");
		}

		if(bError == TRUE)
		{
			m_bDontChangePage = TRUE;
			AfxMessageBox(csBuf, MB_ICONERROR);
			return -1;
		}
	}

	CWaitCursor wc;

	// spara ned f�r�ndrade huvudv�rden
	m_hxlHeader.csBestand = m_csBestand;
	if(m_csDelbestand != _T(""))
		m_hxlHeader.nDelbestand = _wtoi(m_csDelbestand);
	else
		m_hxlHeader.nDelbestand = 0;
	m_hxlHeader.csDatum.Format(_T("%04d%02d%02d"), m_ctDatum.GetYear(), m_ctDatum.GetMonth(), m_ctDatum.GetDay());
	m_hxlHeader.nAltitud = m_nAltitud;
	m_hxlHeader.nAreal = m_fAreal * 10000.0;
	m_hxlHeader.nBestAlder = m_nAlder;
	m_hxlHeader.csSI = m_csSI;
	m_hxlHeader.nLatitud = m_nBreddgrad;

	// kontrollera om huvud skiljer sig fr�n det i f�rsta HXL-filen
	HXLHEADER hxlHeader;
	hxlHeader = ((CWizardSheet*)GetParent())->m_csaHxlFiles.GetAt(0);
	if( memcmp(&hxlHeader, &m_hxlHeader, sizeof(HXLHEADER)) != 0)
	{
		// skapa en tempor�r kopia av f�rsta HXL-filen
		CString csBuf = hxlHeader.csFilePath;
		CString csNewFilePath;
		csNewFilePath = csBuf.Right( csBuf.GetLength() - csBuf.ReverseFind('\\') - 1 );
		csNewFilePath = getTmpPath() + csNewFilePath;

		CopyFile(hxlHeader.csFilePath, csNewFilePath, FALSE);

		// peka om s�kv�g till filen i huvudet
		m_hxlHeader.csFilePath = csNewFilePath;

		// spara ned f�r�ndrade huvudvariabler
		CHXLParser hxl;
		if(hxl.setHeaderData(csNewFilePath, &m_hxlHeader))
		{
			// uppdatera f�rsta elementet i fillistan
			((CWizardSheet*)GetParent())->m_csaHxlFiles.SetAt(0, m_hxlHeader);
		}
		else
		{
			wc.Restore();

			return FALSE;
		}
	}

	// Skapa best�ndet med alla inventeringsfiler
	bool bFirst = TRUE;
	CFile cfInvfile;
	CStringA csBufA;
	int nStandId = -1;
	csBuf = ((CWizardSheet*)GetParent())->m_csTemplateXml;

	for(int nLoop=0; nLoop<((CWizardSheet*)GetParent())->m_csaHxlFiles.GetCount(); nLoop++)
	{
		// �ppna upp filen och l�s dess inneh�ll till en buffer
		hxlHeader = ((CWizardSheet*)GetParent())->m_csaHxlFiles.GetAt(nLoop);

		if(cfInvfile.Open(hxlHeader.csFilePath, CFile::modeRead))
		{
			char *pszBuf;
			pszBuf = (char*)malloc(cfInvfile.GetLength()+2);
			if(pszBuf == NULL)
			{
				AfxMessageBox(_T("Kunde inte allokera minne f�r att l�sa in fil!"));
				return FALSE;
			}

			cfInvfile.Read(pszBuf, cfInvfile.GetLength());
			pszBuf[cfInvfile.GetLength()] = ' '; // For some reason parsing will fail if there are no chars after the end tag
			pszBuf[cfInvfile.GetLength()+1] = '\0';
			csBufA = pszBuf;
			free(pszBuf);
			cfInvfile.Close();

			if(bFirst && !((CWizardSheet*)GetParent())->m_bKomplettera)	// skapa det nya best�ndet med f�rsta filen
			{
				if(!_CreateStand(csBufA, csBuf, getUserName(), &nStandId))
				{
					wc.Restore();

					AfxMessageBox(_GetLastMessage(), MB_ICONERROR);
					m_bDontChangePage = TRUE;

					return -1;
				}

				((CWizardSheet*)GetParent())->m_nBestandId = nStandId;
				bFirst = FALSE;
			}
			else	// resterande filer anv�nds f�r att komplettera best�ndet
			{
				if( ((CWizardSheet*)GetParent())->m_bKomplettera ) nStandId = ((CWizardSheet*)GetParent())->m_nBestandId;

				if(!_CreateStand(csBufA, ((CWizardSheet*)GetParent())->m_csTemplateXml, getUserName(), &nStandId))
				{
					wc.Restore();

					AfxMessageBox(_GetLastMessage(), MB_ICONERROR);
					m_bDontChangePage = TRUE;

					return -1;
				}
			}
		}
	}

	// ber�kna best�ndet
	if(nStandId != -1)
	{
		_CalculateStand(nStandId);
	}

	wc.Restore();

	return CPropertyPage::OnWizardNext();
}

BOOL CHeader::OnKillActive() 
{
//	if(m_bDontChangePage == TRUE)
//		return FALSE;

	return CPropertyPage::OnKillActive();
}
