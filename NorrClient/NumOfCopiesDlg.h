#pragma once
#include "Resource.h"
#include "afxwin.h"
#include "MyTabControl.h"

// CNumOfCopiesDlg dialog

class CNumOfCopiesDlg : public CDialog
{
	DECLARE_DYNAMIC(CNumOfCopiesDlg)
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CMyExtEdit m_wndEdit;
	CStatic m_wndLbl;

public:
	int m_nNum;
	
	CNumOfCopiesDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CNumOfCopiesDlg();

// Dialog Data
	enum { IDD = IDD_NUMOFCOPIES };

protected:
	void setLanguage(void);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnEnChangeEditNumof();
};
