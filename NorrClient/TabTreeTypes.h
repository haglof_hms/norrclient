#pragma once
#include "Resource.h"
#include "MyTabControl.h"
//#include "MDIStandTableFrame.h"

// CTabTreeTypes form view

class CTabTreeTypes : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CTabTreeTypes)

	CString m_sAbrevLangSet;
	CString m_sLangFN;

	//CFont m_fntTab;

public:
	/*CMyTabControl*/ CXTPTabControl m_wndTabControl;		//changed from CXTPTabControl m_wndTabControl;

//protected:
public:
	CTabTreeTypes();           // protected constructor used by dynamic creation
	virtual ~CTabTreeTypes();

public:
	enum { IDD = IDD_TABTREETYPES };
	
	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);
	void setupDclsTables(double dcls);
	void deleteFromStandTable(void);

	BOOL isOkToClose(void);
	void resetDataChanged(void);
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
protected:
	virtual void OnDraw(CDC* /*pDC*/);
};

