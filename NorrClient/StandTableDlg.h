#pragma once

#include "MyTabControl.h"
#include "SampleTreeTabPageManager.h"
#include "OXMaskedEdit.h"
#include "afxwin.h"
#include <vector>
#include "HXLParser.h"

// CStandTableDlg dialog

class CStandTableDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CStandTableDlg)

	CXTPDockingPaneManager m_paneManager;
	/*CMyTabControl*/ CXTPTabControl m_wndTabControl;		//changed from CXTPTabControl m_wndTabControl;
	CWnd* m_pWnd;	// pointer to the view for tree types

	CString m_csFilename;

	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_csFileMsg;
	CString m_csErrCreateInvFile;
	CString m_csErrMsgNoTree;
	CString m_csMsgSide;
	CString m_csMsgSILimit;
	CString m_csMsgSISpec;
	CString m_csMsgDelnr;

	CXTResizeGroupBox m_wndGrp1;

	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;
	CMyExtStatic m_wndLbl10;
	CMyExtStatic m_wndLblInvmetod;
	CMyExtStatic m_wndLblYtareal;

	CMyExtEdit m_wndEdit3;		//fastighets-/best�ndsnamn
	CMyExtEdit m_wndEdit4;		//areal
	CMyExtEdit m_wndEdit5;		//breddgrad
	CMyExtEdit m_wndEdit6;		//h�jd �ver hav
	CMyExtEdit m_wndEdit7;		//best�nds�lder
	COXMaskedEdit m_wndEdit8;	//H100
	CMyExtEdit m_wndEdit10;		//Delnr
	CComboBox m_cbInvmetod;		// Inventeringsmetod
	CMyExtEdit m_wndEditYtareal;	// Ytareal

	int m_nInventeringsMetod;	// 0 = Provytor, 1 = Totaltaxering
	int m_nDefInventeringsMetod;	// default inventeringsmetod

	std::vector< PLOT > m_vecPlots;	// vector containing plots, tree types, species and diameter classes

	BOOL m_bDontLoadSave;	// variable used to prevent loading/saving to/from tabs when changing focus
	CHXLParser m_hxl;

public:
	CStandTableDlg(CString, int nDefInventeringsMetod = -1);
	CStandTableDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CStandTableDlg();

//	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);
	BOOL CreateView(CRuntimeClass* pViewClass);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);

//	CStringA getOutputHXL() { return m_csOutputHXL; }

	CString getFilename() { return m_csFilename; }

// Dialog Data
	enum { IDD = IDD_STANDTABLE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

	vecTransactionSpecies m_vecSpecies;
	vecInt m_vecSpeciesUsed;
	vecMySampleTrees m_vecSampleTrees;

	void setupDclsTables(int /*nPlot*/, double /*dcls*/);	// setup diameter class tables for a sample plot
	BOOL saveDataToHxlFile(CString);

	void setLanguage(void);

	BOOL m_bDataNotChanged;
	BOOL isOkToClose(void) { return m_bDataNotChanged;}	
	void setDataChanged(void) { m_bDataNotChanged = FALSE;}
	void resetDataChanged(void) {m_bDataNotChanged = TRUE;}
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnCancel();
	virtual void OnOK();

	int SavePlotData(int nPlot);	// used to save all data contained in the tabs
	int LoadPlotData(int nPlot);	// used to load all data contained in the tabs

	CString getName(void) {return m_wndEdit3.getText();}
	CString getArea(void) {return m_wndEdit4.getText();}
	CString getLatitude(void) {return m_wndEdit5.getText();}
	CString getHeightOverSea(void) {return m_wndEdit6.getText();}
	CString getAge(void) {return m_wndEdit7.getText();}
	CString getH100(void) {return m_wndEdit8.GetInputData();}
	CString getDelnr(void) {return m_wndEdit10.getText();}
	int getInventeringsmetod(void) { return m_cbInvmetod.GetCurSel() + 1; }
	CString getYtarea(void) { return m_wndEditYtareal.getText(); }

	void setProperty(LPCTSTR sName);
	void setName(LPCTSTR sName);
	void setArea(int nArea);
	void setLatitude(int nLatitude);
	void setHeightOverSea(int nHgtSea);
	void setAge(int nAge);
	void setH100(LPCTSTR sSi);
	void setDelnr(LPCTSTR sNum);
	void setUpdate(BOOL bUpd) {UpdateData(bUpd);}
	void setInventeringsmetod(int nMetod) { m_cbInvmetod.SetCurSel(nMetod-1); }
	void setYtarea(LPCTSTR csYtareal) { m_wndEditYtareal.SetWindowTextW(csYtareal); }

	int getNumOfSpcUsed(void);
	CString getSpcNamesUsedHxl();
	BOOL getSampleTrees(void);
	BOOL isSampleTreeSet(double id, double start, double stop, int type, CMyTransactionSampleTree &stree);

	int AddSpecies(int nPlot);	// add selected species to a plot

	afx_msg void OnSelChangeTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelChangingTab(NMHDR* pNMHDR, LRESULT* pResult);

	int isSampleTreesSelected();
	int DeleteSampleTree();

	LRESULT OnTimer(WPARAM wParam, LPARAM lParam);

public:
	afx_msg void OnBnClickedButtonSpeciesAdd();
	afx_msg void OnBnClickedButtonSpeciesDel();
	afx_msg void OnEnChangeEdit1();
	afx_msg void OnEnChangeEdit2();
	afx_msg void OnEnChangeEdit3();
	afx_msg void OnEnChangeEdit4();
	afx_msg void OnEnChangeEdit5();
	afx_msg void OnEnChangeEdit6();
	afx_msg void OnEnChangeEdit7();
	afx_msg void OnEnChangeEdit8();
	afx_msg void OnEnChangeEdit9();
	afx_msg void OnEnChangeEdit10();
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnBnClickedButtonCancel();
	afx_msg void OnCbnSelchangeInvmetod();
	afx_msg void OnBnClickedButtonPlotAdd();
	afx_msg void OnBnClickedButtonPlotDel();
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonDel();
};
