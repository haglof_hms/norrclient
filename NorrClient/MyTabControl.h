#pragma once

class CMyTabControl : public CXTPTabControl
{
protected:
	void OnItemClick(CXTPTabManagerItem* pItem);

public:
	CMyTabControl();

	void RedrawControl(LPCRECT lpRect,BOOL bAnimate);

	// Return the page that's active in control; 060324 p�d
	CXTPTabManagerItem *getSelectedTabPage(void);

	// Return the page set by index; 060405 p�d
	CXTPTabManagerItem *getTabPage(int idx);

	// Return number of tabs; 060405 p�d
	int getNumOfTabPages(void);
};

/////////////////////////////////////////////////////////////////////////////
// CMyExtEdit window

class CMyExtEdit : public CXTEdit 
{
//private:
  // default colors
  enum
  {
     ENABLED_FG = RGB(0,0,0), // black
     ENABLED_BG = RGB(255,255,255), // white
     DISABLED_FG = RGB(0,0,0), // black
     DISABLED_BG = RGB(192,192,192), // light grey
  };

//  Implementation
	void RemoveLeadingZeros ( void );

//	Attributes
	LONG	m_nMax;		//	Maximum number the edit control will accept
	LONG	m_nMin;		//	Minimum number the edit control will accept
	LONG	m_nLastValidValue;	//	The last valid value entered by the 

	int m_nItemData;
	CString m_sItemData;

	TCHAR szValue[128];
	TCHAR szIdentifer[128];

	BOOL m_bIsNumeric;	// TRUE if only numeric values are allowed

	BOOL m_bIsAsH100;		// TRUE if the editbox'll set value according to H100; 070508 p�d

	BOOL m_bIsValidate;	// TRUE if validation of numeric range, set in SetRange(...)

	BOOL isNumber(TCHAR *str)
	{
		BOOL bOk = FALSE;
		for (TCHAR *p = str;p < str + _tcslen(str);p++)
		{
			if (isNumeric(*p))
			{
				bOk = TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		return bOk;
		
	}

	BOOL isNumeric(UINT value)
	{
		if ((value >= 96 && value <= 105) || value == 188 || value == 190 || value == 110)
			return TRUE;
		return FALSE;
	}

	CString setAsH100(LPCTSTR str)
	{
		TCHAR szBuffer[32];
		_tcsncpy(szBuffer,str,sizeof(szBuffer) / sizeof(TCHAR));
		// If empty string, return; 070510 p�d
		if (_tcscmp(szBuffer,_T("")) == 0)
			return _T("");

		// Check first character in szBuffer, to see which
		// Specie's selected. I.e. 1 = Pine, 2 = Spruce all rest
		// will be Birch, if not IMP (Impediment) is set; 070508 p�d
		if (_tcscmp(szBuffer,_T("IMP")) == 0 || _tcscmp(szBuffer,_T("imp")) == 0)
			return _T("IMP");

		if (szBuffer[0] == '1' || szBuffer[0] == 'T' || szBuffer[0] == 't')
			szBuffer[0] = 'T';
		else if (szBuffer[0] == '2' || szBuffer[0] == 'G' || szBuffer[0] == 'g')
			szBuffer[0] = 'G';
		else if (szBuffer[0] == '3' || szBuffer[0] == 'B' || szBuffer[0] == 'b')
			szBuffer[0] = 'B';
		else
			szBuffer[0] = '\0';
		return szBuffer;
	}

	// Background brush
  CBrush *m_pbrushDisabled;
  // Foreground brush
  CBrush *m_pbrushEnabled;

	CFont *m_fnt1;

	BOOL	m_bModified;
// Construction
public:
	CMyExtEdit();

// Implementation
	void SetRange ( LONG inLMin, LONG inLMax )
	{
		m_nMin = inLMin;
		m_nMax = inLMax;
		m_bIsValidate = TRUE;
	}

	void setItemIntData(int v)
	{
		m_nItemData = v;
	}
	int getItemIntData(void)
	{
		return m_nItemData;
	}

	void setItemStrData(LPCTSTR v)
	{
		m_sItemData = v;
	}

	CString getItemStrData(void)
	{
		return m_sItemData;
	}

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyExtEdit)
	virtual void PreSubclassWindow();
	virtual BOOL PreTranslateMessage(MSG *);
	//}}AFX_VIRTUAL

	void SetReadOnly(BOOL bReadOnly = TRUE);
  void SetEnabledColor(COLORREF crFG = ENABLED_FG, COLORREF crBG = ENABLED_BG);
  void SetDisabledColor(COLORREF crFG = DISABLED_FG, COLORREF crBG = DISABLED_BG);
	void SetFontEx(int size = -1,int weight = FW_NORMAL,BOOL underline = FALSE,BOOL italic = FALSE);

//	void SetBkColor(COLORREF crColor); // This Function is to set the BackGround Color for the Text and the Edit Box.
//	void SetTextColor(COLORREF crColor); // This Function is to set the Color for the Text.
	void SetAsNumeric(void);
	void SetAsH100(void);

	virtual ~CMyExtEdit();

	double getFloat(void)
	{
		GetWindowText((LPTSTR)szValue,128);
		return _tstof(szValue);
	}
	void setFloat(double value,int dec)
	{
		_stprintf(szValue,_T("%.*f"),dec,value);
		SetWindowText((LPTSTR)szValue);
	}

	int getInt(void)
	{
		GetWindowText((LPTSTR)szValue,128);
		return _tstoi(szValue);
	}
	void setInt(int value)
	{
		_stprintf(szValue,_T("%d"),value);
		SetWindowText((LPCTSTR)szValue);
	}

	CString getText(void)
	{
		//�ndrat 20100317 h�mtar CString ist�llet f�r tchar storlek 128
		//Annars trunkeras texten i anteckningsf�lt till 128 tecken
		//GetWindowText((LPTSTR)szValue,128);
		if (m_bIsAsH100)
		{
			GetWindowText((LPTSTR)szValue,128);
			return setAsH100(szValue);
		}
		else
		{
			GetWindowText(m_sItemData);
			return m_sItemData;
		}
	}

	void setIdentifer(LPCTSTR id)
	{
		_tcscpy_s(szIdentifer,128,id);
	}

	LPCTSTR getIdentifer(void)
	{
		return (LPCTSTR)szIdentifer;
	}

	BOOL isDirty(void)
	{
		return m_bModified;
	}

	void setIsDirty(void)
	{
		m_bModified = TRUE;
	}

	void resetIsDirty(void)
	{
		m_bModified = FALSE;
	}

	// Generated message map functions
protected:
	CBrush m_brBkgnd; // Holds Brush Color for the Edit Box
	COLORREF m_crFGEnabled;			// Holds the Background Color for the Text for Enabled window
	COLORREF m_crBGEnabled;			// Holds the Color for the Text for Enabled window
	COLORREF m_crFGDisabled;		// Holds the Background Color for the Text for Disabled window
	COLORREF m_crBGDisabled;		// Holds the Color for the Text for Disabled window
	//{{AFX_MSG(CMyExtEdit)
  afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnUpdate();
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CMyExtStatic window

class CMyExtStatic : public CStatic
{
// Construction
public:
	CMyExtStatic();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyExtStatic)
	//}}AFX_VIRTUAL

	void SetBkColor(COLORREF crColor); // This Function is to set the BackGround Color for the Text and the Edit Box.
	void SetTextColor(COLORREF crColor); // This Function is to set the Color for the Text.
	void SetLblFont(int,int,LPTSTR font_name = _T("Arial"));
	// Set font items except fontface; 071130 p�d
	void SetLblFontEx(int size = -1,int weight = FW_NORMAL,BOOL underline = FALSE,BOOL italic = FALSE);
	virtual ~CMyExtStatic();

protected:
	CString m_sText;
	CFont *m_fnt1;
	// Generated message map functions
protected:
	CBrush m_brBkgnd; // Holds Brush Color for the Edit Box
	COLORREF m_crBkColor; // Holds the Background Color for the Text
	COLORREF m_crTextColor; // Holds the Color for the Text
	//{{AFX_MSG(CMyExtStatic)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor); // This Function Gets Called Every Time Your Window Gets Redrawn.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CMyExtTranspStatic

class CMyExtTranspStatic : public CStatic
{
	DECLARE_DYNAMIC(CMyExtTranspStatic)

public:
	CMyExtTranspStatic();
	virtual ~CMyExtTranspStatic();

protected:
	CBrush m_brBkGnd;
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};
