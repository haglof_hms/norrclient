#pragma once

#include "afxcmn.h"

// CIndata dialog

class CIndata : public CPropertyPage
{
	DECLARE_DYNAMIC(CIndata)

	int m_nInventoryType;
	int m_nSelectedItem;
	CStringArray m_caFiles;

public:
	CIndata();
	virtual ~CIndata();
	virtual BOOL OnSetActive();

// Dialog Data
	enum { IDD = IDD_WIZARD_INDATA };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual LRESULT OnWizardNext();
	virtual BOOL OnInitDialog();
	
	void SetWizardButtons();
	void ClearDesc();
	void UpdateDesc();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonNew();
	afx_msg void OnBnClickedButtonLoad();
	afx_msg void OnBnClickedButtonRemove();
	CListCtrl m_clFiles;
	CListCtrl m_clDesc;
	afx_msg void OnLvnItemchangedListFiles(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonEdit();
};
