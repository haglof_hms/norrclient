#pragma once
#include "stdafx.h"

class CSpeciesViewReportRec : public CXTPReportRecord
{

protected:
	class CFloatItem : public CXTPReportRecordItemNumber
	{
		double m_fValue;
	public:
		CFloatItem(double fValue, LPCTSTR fmt_str = sz1dec) : CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		double getFloatItem(void) { return m_fValue;}

		void setFloatItem(double value)
		{
			m_fValue = value;
			SetValue(value);
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_fValue = (double)_tstof(szText);
			SetValue(m_fValue);
		}
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
		int m_nValue;

	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)
		{
			m_nValue = nValue;
		}

		int getIntItem(void) { return m_nValue;}

		void setIntItem(int nValue)
		{
			m_nValue = nValue;
			SetValue(nValue);
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
		}

	};

	class CTextItem : public CXTPReportRecordItemText
	{
		CString m_sText;

	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		CString getTextItem(void) { return m_sText;}

		void setTextItem(LPCSTR sValue)
		{
			m_sText = sValue;
			SetValue(m_sText);
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR sValue)
		{
			m_sText = sValue;
			SetValue(sValue);
		}
	};

public:
	CSpeciesViewReportRec(double start = 0.0, double stop = 0.0, double ant = 0.0)	//int ant = 0)
	{
		AddItem(new CFloatItem(start, sz1dec));
		AddItem(new CFloatItem(stop, sz1dec));
		AddItem(new CFloatItem(ant, sz0dec));
	}

	CSpeciesViewReportRec(CString sValue)
	{
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(sValue));
	}

	double getColumnFloat(int item)
	{
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item, double value)
	{
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)
	{
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	void setColumnInt(int item, int value)
	{
		((CIntItem*)GetItem(item))->setIntItem(value);
	}

	CString getColumnText(int item)
	{
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	void setColumnText(int item, LPCSTR value)
	{
		((CTextItem*)GetItem(item))->setTextItem(value);
	}


	//CSpeciesViewReportRec(void);
	//~CSpeciesViewReportRec(void);
};
