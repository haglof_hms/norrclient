#include "stdafx.h"
#include "resource.h"
#include "RpcProcedures.h"
#include "WizardSheet.h"
#include "StandTableDlg.h"
#include "BytMall.h"

#pragma managed(push, off)

extern "C" BOOL PASCAL EXPORT SkapaBestand()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// do we have a valid license?
	if(!_CheckLicense())
	{
		// no valid license
		if(_GetLastMessage() != NULL)
			AfxMessageBox( _GetLastMessage() );
		return FALSE;
	}

	// visa wizard f�r att skapa best�nd
	CWizardSheet *pWizard = new CWizardSheet(_T("Test"), -1);
	int nRet = pWizard->DoModal();
	int nStandId;
	if(nRet != IDCANCEL)
		nStandId = pWizard->m_nBestandId;
	else
		nStandId = FALSE;

	delete pWizard;

	return nStandId;
}

extern "C" BOOL PASCAL EXPORT RaderaBestand(int id)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// do we have a valid license?
	if(!_CheckLicense())
	{
		// no valid license
		if(_GetLastMessage() != NULL)
			AfxMessageBox( _GetLastMessage() );
		return FALSE;
	}

	return _RemoveStand(id);
}

extern "C" BOOL PASCAL EXPORT RaknaomBestand(int id)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// do we have a valid license?
	if(!_CheckLicense())
	{
		// no valid license
		if(_GetLastMessage() != NULL)
			AfxMessageBox( _GetLastMessage() );
		return FALSE;
	}

	CBytMall dlg2(id);
	dlg2.DoModal();

	return TRUE;
}

extern "C" BOOL PASCAL EXPORT KompletteraBestand(int id)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// do we have a valid license?
	if(!_CheckLicense())
	{
		// no valid license
		if(_GetLastMessage() != NULL)
			AfxMessageBox( _GetLastMessage() );
		return FALSE;
	}

	// visa wizard f�r att skapa best�nd
	CWizardSheet *pWizard = new CWizardSheet(_T("Test"), id);
	pWizard->DoModal();
	delete pWizard;

	return TRUE;
}

#pragma managed(pop)
