#include "StdAfx.h"
#include "OpenFileFunctions.h"
//#include "MDIStandTableFrame.h"
#include "StandTableDlg.h"
#include "SpeciesView.h"
#include "SampleTreeView.h"

COpenFileFunctions::COpenFileFunctions():
m_hInstReader(NULL),
m_nFileType(0)
{

}

COpenFileFunctions::~COpenFileFunctions()
{
	if(m_hInstReader)
	{
		FreeLibrary(m_hInstReader);
		m_hInstReader = NULL;
	}
}

int COpenFileFunctions::OpenFile(LPCTSTR fn)
{
	CString dllPath, file(fn);
	Func_FileOpen procFileOpen;

	
	// Check file type
	CString ext = file.Right(4).MakeLower();
	if( ext == ".inv" )
	{
		dllPath = getStanfordDllPath();
		m_nFileType = FILETYPE_INV;
	}
	else if( ext == ".hxl" )
	{
		dllPath = getHXLDllPath();
		m_nFileType = FILETYPE_HXL;
	}
	else
	{
		// Unknown file type
		//TODO: AfxMessageBox( getLangStr(STRID_OKANDFILTYP), MB_OK | MB_ICONEXCLAMATION );
		return -1;
	}

	// Load appropriate dll
	m_hInstReader = LoadLibrary(dllPath);
	if( !m_hInstReader )
	{
		//TODO: AfxMessageBox(getLangStr(STRID_DLLNOTFOUND) + _T("\n") + dllPath, MB_OK | MB_ICONEXCLAMATION);
		return -1;
	}

	// Open specified file
	if( m_nFileType == FILETYPE_INV )
	{
		procFileOpen = (Func_FileOpen)GetProcAddress(m_hInstReader, STANFORDFUNC_FILEOPEN);
	}
	else if( m_nFileType = FILETYPE_HXL )
	{
		procFileOpen = (Func_FileOpen)GetProcAddress(m_hInstReader, HXLFUNC_FILEOPEN);
	}
	else
	{
		return -1; // unknown file type
	}
	if( !procFileOpen )
	{
		//TODO: AfxMessageBox( getLangStr(STRID_DLLOPENFAILED) + _T("\n") + dllPath, MB_OK | MB_ICONEXCLAMATION);
		return -1;
	}

	return( procFileOpen(CW2A(fn)) );
}

CString COpenFileFunctions::getStanfordDllPath()
{
	CString dllPath;

	dllPath.Format( _T("%s\\%s"), getDllDirectory(), STANFORDDLL_FILENAME );

	return dllPath;
}

CString COpenFileFunctions::getHXLDllPath()
{
	CString dllPath;

	dllPath.Format( _T("%s\\%s"), getDllDirectory(), HXLDLL_FILENAME );

	return dllPath;
}

// Returns path to dll directory (to be used by get___DllPath functions above)
CString COpenFileFunctions::getDllDirectory()
{
	CString path;

	// Get directory of this module
	::GetModuleFileName( AfxGetApp()->m_hInstance, path.GetBufferSetLength(_MAX_PATH), _MAX_PATH );
	path.ReleaseBuffer();

	int nIndex = path.ReverseFind( _T('\\') );
	if( nIndex > 0 )
	{
		path = path.Left( nIndex ); // Extract path (remove module filename)
	}
	else
	{
		path.Empty();
	}

	return path;
}

int COpenFileFunctions::GetNumOfSet()
{
	int nReturn;
	Func_GetNumOfSet procGetNumOfSet;

	if(!m_hInstReader)
		return -1;

	// Check file type
	if( m_nFileType == FILETYPE_INV )
	{
		procGetNumOfSet = (Func_GetNumOfSet)GetProcAddress(m_hInstReader, STANFORDFUNC_GETNUMOFSET);
	}
	else if( m_nFileType == FILETYPE_HXL )
	{
		procGetNumOfSet = (Func_GetNumOfSet)GetProcAddress(m_hInstReader, HXLFUNC_GETNUMOFSET);
	}
	else
	{
		return -1; // unknown file type
	}

	if( !procGetNumOfSet )
	{
		//TODO: AfxMessageBox( getLangStr(STRID_DLLERROR), MB_OK | MB_ICONEXCLAMATION );
		return -1;
	}

	if( procGetNumOfSet(&nReturn) < 0 )
		return -1;

	return nReturn;
}


int COpenFileFunctions::GetIntData(int set, int var, int type, int index)
{
	int nReturn;
	Func_GetInteger procGetInteger;

	if(!m_hInstReader)
		return -1;

	// Check file type
	if( m_nFileType == FILETYPE_INV )
	{
		procGetInteger = (Func_GetInteger)GetProcAddress(m_hInstReader, STANFORDFUNC_GETINTEGER);
	}
	else if( m_nFileType == FILETYPE_HXL )
	{
		procGetInteger = (Func_GetInteger)GetProcAddress(m_hInstReader, HXLFUNC_GETINTEGER);
	}
	else
	{
		return -1;  // unknown file type
	}

	if( !procGetInteger )
	{
		//TODO: AfxMessageBox( getLangStr(STRID_DLLERROR), MB_OK | MB_ICONEXCLAMATION );
		return -1;
	}

	if(procGetInteger(set, var, type, index, &nReturn) < 0)
		return -1;

	return nReturn;
}

TCHAR* COpenFileFunctions::GetTextData(int set, int var, int type, int index)
{
	char buf[256];
	static TCHAR retval[256];
	Func_GetText procGetText;

	if(!m_hInstReader)
		return NULL;

	// Check file type
	if( m_nFileType == FILETYPE_INV )
	{
		procGetText = (Func_GetText)GetProcAddress(m_hInstReader, STANFORDFUNC_GETTEXT);
	}
	else if( m_nFileType == FILETYPE_HXL )
	{
		procGetText = (Func_GetText)GetProcAddress(m_hInstReader, HXLFUNC_GETTEXT);
	}
	else
	{
		return NULL; // unknown file type
	}

	if( !procGetText )
	{
		//TODO: AfxMessageBox( getLangStr(STRID_DLLERROR), MB_OK | MB_ICONEXCLAMATION );
		return NULL;
	}

	if(procGetText(set, var, type, index, buf) < 0)
		return NULL;

	// Convert to unicode and return
	_tcsncpy(retval, CA2W(buf), sizeof(retval) / sizeof(TCHAR) - sizeof(TCHAR));
	return retval;
}

BOOL COpenFileFunctions::readDataFromFile()
{
	CFileDialog *m_pOpenFile = new CFileDialog(TRUE);
	CString csBuf;
	
	if(!m_pOpenFile)
		return FALSE;

	//TODO: titel p� dialog och f�rklaringar f�r filextensions
	m_pOpenFile->m_ofn.lpstrFilter = _T(" (*.inv)\0*.inv\0 (*.hxl)\0*.hxl\0 (*.inv; *.hxl)\0*.inv;*.hxl\0\0");	//_T("%s (*.inv)|*.inv|%s (*.hxl)|*.hxl|%s (*.inv; *.hxl)|*.inv;*.hxl||")
	m_pOpenFile->m_ofn.nFilterIndex = 3;
	m_pOpenFile->m_ofn.Flags = OFN_EXPLORER | OFN_HIDEREADONLY | OFN_ENABLESIZING;

	m_pOpenFile->GetOFN().lpstrFile = csBuf.GetBuffer(2048*(_MAX_PATH + 1) +1);

	if(m_pOpenFile->DoModal() == IDOK)
	{
		if(OpenFile(csBuf) == 0)
		{
			//file is open, extract data
			int nNumSet = 0;
			nNumSet = GetNumOfSet();
			for(int i=0; i<nNumSet; i++)
			extractData(i);
		}
	}

	csBuf.ReleaseBuffer();
	delete m_pOpenFile;

	return TRUE;
}

BOOL COpenFileFunctions::extractData(int set)
{
	int type=0;
	int area, age,latitude,hgtsea,side,temp;
	CString name, si;

	//get type, only total inventory can be handled
	if((type = GetIntData(set,2030,1,0)) == -1)
		return FALSE;

	if(type != 2)
	{
		AfxMessageBox(_T("TODO: Msg, endast totalinventering kan l�sas in"));
		return TRUE;
	}

	//2 tract name
	name = GetTextData(set,2,1,0);

	//670 areal
	area = GetIntData(set,670,1,0);

	//660 age
	age = GetIntData(set,660,1,0);

	//2011 latitude
	latitude = GetIntData(set,2011,2,0);
	if(latitude >= 0 && m_nFileType == FILETYPE_HXL)
		latitude /= 10;	//adjust value if hxl-file

	//2012 height above sea level
	hgtsea = GetIntData(set,2012,1,0);	

	//2095 site index
	if((si = GetTextData(set,2095,2,0)) == _T(""))	//type 2 used in hxl files
	{
		//no data found, check type 1
		if((temp = GetIntData(set,2095,1,0)) == -1)
			si = _T("");
		else
			convertSi(temp, si);
	}

	//2104 side
	side = GetIntData(set,2104,1,0);	

	CFrameWnd *pFrameWnd = STATIC_DOWNCAST(CFrameWnd, AfxGetMainWnd());
	if(!pFrameWnd)
		return FALSE;

/*	CMDIStandTableFrame *pWnd = (CMDIStandTableFrame*)pFrameWnd->GetActiveFrame();
	if(!pWnd)
		return FALSE;

	//set data in dialog
	pWnd->m_wndGeneralDataDlg.setProperty(name);	//set property, block and unit
	pWnd->m_wndGeneralDataDlg.setArea(area);
	pWnd->m_wndGeneralDataDlg.setLatitude(latitude);
	pWnd->m_wndGeneralDataDlg.setHeightOverSea(hgtsea);
	pWnd->m_wndGeneralDataDlg.setAge(age);
	pWnd->m_wndGeneralDataDlg.setH100(si);
	pWnd->m_wndGeneralDataDlg.setSide(side);

	if( m_nFileType == FILETYPE_INV )
	{
		getTreeDataInv(set);
	}
	else if( m_nFileType = FILETYPE_HXL )
	{
		getTreeDataHxl(set);
	}
*/
	return TRUE;
}

void COpenFileFunctions::convertSi(int nSi, CString &sSi)
{
	int si = 0,i;
	CString tmp;

	for(i=1;;i++)	//
	{
		if((si = nSi/((int)pow(10.0,i))) < 10)
			break;
	}

	nSi = (nSi%((int)pow(10.0,i)));		//remove first digit with modulus
	if( nSi >= 100 )		//
		nSi /= 10;			//dm -> m

	if(si == 1)
		tmp = _T("T");
	else if(si == 2)
		tmp = _T("G");
	else if(si == 3)
		tmp = _T("B");
	else				//C = 6,E = 9,F = 13 finns inte med i gamla inv-filerna
		tmp = _T("");

	sSi.Format(_T("%s%02d"),tmp,nSi);	
}

void COpenFileFunctions::getTreeDataInv(int set)
{

}

void COpenFileFunctions::getTreeDataHxl(int set)
{
	
	//get dcls from dialog and create standtables
	double dcls;
	
	if((dcls = getDcls()) < 0)
		return;

	if(getSpecNameFromFile(set) < 0)
		return;

	//get tree data and set spec names used
	if(getTreesFromFile(set) < 0)
		return;
	

	//add tabs with spec names and ids
/*	CStandTableView *pView = (CStandTableView*)getFormViewByID(IDD_FORMVIEW);
	if(pView)
	{
		CMyTabControl *pTab = &pView->m_wndTabControl;
		if(pTab)
		{
			int nTabPages = pTab->getNumOfTabPages();

			for(int i = 0; i<nTabPages; i++)
			{
				CXTPTabManagerItem *pItem = pTab->getTabPage(i);

				if(pItem)
				{
					if(i != TAB_SAMPLETREE)
					{
						CTabPageManager *pTabManager = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pItem->GetHandle()));

						if(pTabManager)
						{
							for(UINT j=0; j<NUM_OF_MSPEC; j++)
							{
								if(m_SpecNames[j].use == 1)
									pTabManager->AddView(RUNTIME_CLASS(CSpeciesView),m_SpecNames[j].name,m_SpecNames[j].id,3);
							}

							//TODO: insert trees and sample trees into diameter classes
						}//if(pManager)
					}
					else if(i == TAB_SAMPLETREE)
					{
						CSampleTreeTabPageManager *pSampleTreeTabManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pItem->GetHandle()));

						if(pSampleTreeTabManager)
						{
							for(UINT j=0; j<NUM_OF_MSPEC; j++)
							{
								if(m_SpecNames[j].use == 1)
									pSampleTreeTabManager->AddView(RUNTIME_CLASS(CSampleTreeView),m_SpecNames[j].name,m_SpecNames[j].id,3);
							}

							//sample trees
							for(UINT i=0; i<m_vecSampleTrees.size(); i++)
							{
								pSampleTreeTabManager->addTree(&m_vecSampleTrees[i]);
							}

							//rand trees
							for(UINT i=0; i<m_vecRandTrees.size(); i++)
							{
								pSampleTreeTabManager->addTree(&m_vecRandTrees[i]);
							}

							

						}//if(pSampleTreeManager)
					}
				}//if(pItem)
			}
		}//if(pTab)

		//create dclstables
		pView->setupDclsTables(dcls);
	}//if(pView)
*/
	
}

double COpenFileFunctions::getDcls()
{
	CXTPDockingPaneManager *pPaneManager = NULL;
	CXTPDockingPane *pPane = NULL;
	double dcls = 0.0;

/*	CMDIStandTableFrame *pMDIChild = (CMDIStandTableFrame*)getFormViewParentByID(IDD_FORMVIEW);
	if(pMDIChild != NULL)
	{
		pPaneManager = pMDIChild->GetDockingPaneManager();
		if(pPaneManager != NULL)
		{
			pPane = pPaneManager->FindPane(IDC_SETTINGS_PANE);
			if(pPane != NULL)
			{
				CSettingsDataDlg *pDlg = (CSettingsDataDlg*)pPane->GetChild();
				if(pDlg != NULL)
				{
					//get dcls value
					dcls = pDlg->getAndCheckDcls();
				}
				else
				{
					//dialog not visited (created), use default value 
					dcls = fDefDclsValue;	//2.0
				}
			}//if(pPane != NULL)
		}//if(pPaneManager != NULL)
	}//if(pMDIChild != NULL)

	if(dcls > 0.0)
	{
		if(pPane != NULL)
		{
			CSettingsDataDlg *pDlg = (CSettingsDataDlg*)pPane->GetChild();
			if(pDlg != NULL)
			{
				//disable m_wndEdit1 in Dlg
				pDlg->setEditDisabled(TRUE);
			}
		}//if(pPane != NULL)
	}
*/
	return dcls;
}

int COpenFileFunctions::getSpecNameFromFile(int set)
{
	int nNum_of_spec = 0;
	memset(m_SpecNames, 0, sizeof(m_SpecNames));
	
	//111 num_of_spec
	if((nNum_of_spec = GetIntData(set, 111, 1, 0)) < 0)
	{
		return 0;
	}

	for(int g=0; g<nNum_of_spec; g++)
	{
		//120 spec_name
		const TCHAR* pStr = GetTextData(set, 120, 1, g);
		if( pStr )
		{
			_tcsncpy(m_SpecNames[g].name, pStr, sizeof(m_SpecNames[g].name) / sizeof(TCHAR) - sizeof(TCHAR));
		}
		else
		{
			memset(m_SpecNames[g].name, 0, sizeof(m_SpecNames[g].name));
		}

		//120 specId
		if((m_SpecNames[g].id = GetIntData(set, 120, 3, g)) < 0)
		{
			m_SpecNames[g].id = 0;
			return -1;
		}

	}

	return nNum_of_spec;
}

int COpenFileFunctions::getTreesFromFile(int set)
{
	BOOL bError;
	int spc,dbh,hgtnum,hgt,hgt2,brk,age,typ,si,plt,gcrown;

	int numTrees = 0;
	for( int i = 0; ; i++ )
	{
		int spcs;
		if((spcs = GetIntData(set, 652, 1, i)) < 0)
		{
			break;
		}

		numTrees++;
	}


	m_vecTrees.clear();
	bError = FALSE;
	for( int i = 0; i<numTrees; i++ )
	{
		spc=dbh=hgtnum=hgt=hgt2=brk=age=typ=si=plt=gcrown=0;

		if((spc = GetIntData(set, 652, 1, i)) < 0)
		{
			bError = TRUE;	//needs spec name
			break;
		}

		if((dbh = GetIntData(set, 653, 1, i)) < 0)
		{
			bError = TRUE;	//needs dbh
			break;
		}

		if((hgtnum = GetIntData(set, 654, 1, i)) < 0)
		{
			hgtnum = 0;
		}
		/*		TODO: height*/
		if(hgtnum > 0)
		if((hgt = GetIntData(set, 655, 1, i)) < 0)
		{
		hgt = 0;
		}
		
		if(hgtnum > 1)
		if((hgt2 = GetIntData(set, 656, 1, i)) < 0)	//??
		{
		hgt2 = 0;
		}
		//*/
		if((brk = GetIntData(set, 2003, 2, i)) < 0)
		{
			brk = 0;
		}

		if((age = GetIntData(set, 2001, 2, i)) < 0)
		{
			if((age = GetIntData(set, 2001, 1, i)) < 0)
			{
				age = 0;
			}
		}

		if((typ = GetIntData(set, 2004, 2, i)) < 0)
		{
			typ = 0;
		}

		if((si = GetIntData(set, 2006, 1, i)) < 0)
		{
			si = 0;
		}
		else
		{
			si /= 10;	//dm -> m
		}

		if((plt = GetIntData(set, 2052, 1, i)) < 0)
		{
			plt = 0;
		}

		gcrown = 0;	//TODO: gcrown

		//TODO: split into trees, sample trees and rand trees vectors
		if(typ == 3)	//rand tree
			m_vecRandTrees.push_back(CMyTransactionSampleTree(spc,dbh,hgt,gcrown,brk,age,si,typ));
		else if(hgtnum > 0)	//sample tree
			m_vecSampleTrees.push_back(CMyTransactionSampleTree(spc,dbh,hgt,gcrown,brk,age,si,typ));
		else
			m_vecTrees.push_back(CMyTransactionSampleTree(spc,dbh,hgt,gcrown,brk,age,si,typ));
		setSpecUsed(spc);
	}

	if(bError == TRUE)
	{
		//TODO: error in tree data
		return -1;
	}

	return numTrees;
}

void COpenFileFunctions::setSpecUsed(int id)
{
	for(int i=0; i<NUM_OF_MSPEC; i++)
	{
		if(m_SpecNames[i].id == id)
		{
			m_SpecNames[i].use = 1;
			break;
		}
	}
}