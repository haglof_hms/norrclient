// SampleTreeTabPageManager.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "SampleTreeTabPageManager.h"
#include "SampleTreeView.h"


// CSampleTreeTabPageManager

IMPLEMENT_DYNCREATE(CSampleTreeTabPageManager, CXTResizeFormView)

CSampleTreeTabPageManager::CSampleTreeTabPageManager()
	: CXTResizeFormView(CSampleTreeTabPageManager::IDD)
{

}

CSampleTreeTabPageManager::~CSampleTreeTabPageManager()
{
}

void CSampleTreeTabPageManager::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSampleTreeTabPageManager, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CSampleTreeTabPageManager diagnostics

#ifdef _DEBUG
void CSampleTreeTabPageManager::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CSampleTreeTabPageManager::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CSampleTreeTabPageManager message handlers

BOOL CSampleTreeTabPageManager::PreCreateWindow(CREATESTRUCT& cs)
{
	
	if(!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CSampleTreeTabPageManager::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
	{
		//AfxMessageBox(_T("CXTResizeFormView::OnCreate not ok!"));
		return -1;
	}

/*	LOGFONT lf;
	m_fntTab.CreateFont(
		24,                        // nHeight
		0,                         // nWidth
		0,                         // nEscapement
		0,                         // nOrientation
		FW_NORMAL,                 // nWeight
		FALSE,                     // bItalic
		FALSE,                     // bUnderline
		0,                         // cStrikeOut
		ANSI_CHARSET,              // nCharSet
		OUT_DEFAULT_PRECIS,        // nOutPrecision
		CLIP_DEFAULT_PRECIS,       // nClipPrecision
		DEFAULT_QUALITY,           // nQuality
		DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
		_T("Times New Roman"));    // lpszFacename

	m_fntTab.GetLogFont(&lf);
*/
//	m_wndTabControl = new CXTPTabControl;
	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP, CRect(0, 0, 0, 0), this, IDC_TABCONTROL_1);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearanceVisualStudio2005);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
//	m_wndTabControl.GetPaintManager()->m_bBoldSelected = TRUE;
//	m_wndTabControl.GetPaintManager()->SetFontIndirect( &lf );
	m_wndTabControl.GetPaintManager()->DisableLunaColors( FALSE );
	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

	

	return 0;
}

void CSampleTreeTabPageManager::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();
}

void CSampleTreeTabPageManager::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);
	if(m_wndTabControl.GetSafeHwnd())
	{
		setResize(&m_wndTabControl,1,1,rect.right-1,rect.bottom-1);
	}
}

BOOL CSampleTreeTabPageManager::AddView(CRuntimeClass *pViewClass, LPCTSTR lpszTitle, int spc_id, int nIcon)
{
		
	CCreateContext contextT;
	contextT.m_pNewViewClass = pViewClass;

	CWnd* pWnd;

	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if(pWnd == NULL)
			AfxThrowMemoryException();
	}
	CATCH_ALL(e)
	{

		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();
	

	CRect rect(0,0,0,0);
	if(!pWnd->Create(NULL, NULL, dwStyle, rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
		return NULL;

	CXTPTabManagerItem *pItem = m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	pItem->SetData(spc_id);
	pItem->SetTooltip(lpszTitle);
	
	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);
	
	return TRUE;
}

void CSampleTreeTabPageManager::setupDclsTables(int dcls)
{
	int nTabs = m_wndTabControl.GetItemCount();
	for(int i=0; i<nTabs; i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.GetItem(i);
		if(pItem)
		{
			CSampleTreeView* pSampleTreeView = DYNAMIC_DOWNCAST(CSampleTreeView, CWnd::FromHandle(pItem->GetHandle()));
			if(pSampleTreeView)
			{
				pSampleTreeView->setupDclsTable(dcls);
			}//if(pSampleTreeView)
		}//if(pItem)
	}//for(int i=0; i<nTabs; i++)
}

void CSampleTreeTabPageManager::deleteSampleTree()
{
	CXTPTabManagerItem *pItem = m_wndTabControl.GetSelectedItem();
	if(pItem)
	{
		CSampleTreeView *pView = DYNAMIC_DOWNCAST(CSampleTreeView, CWnd::FromHandle(pItem->GetHandle()));
		if(pView)
		{
			pView->deleteSampleTree();
		}
	}
}

BOOL CSampleTreeTabPageManager::getSampleTrees(vecMySampleTrees &vecST)
{
	BOOL bRes = FALSE;
	for(int i=0; i<m_wndTabControl.GetItemCount(); i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.GetItem(i);
		if(!pItem)
			return FALSE;

		CSampleTreeView *pView = DYNAMIC_DOWNCAST(CSampleTreeView, CWnd::FromHandle(pItem->GetHandle()));
		if(!pView)
			return FALSE;

		if((bRes = pView->getSampleTree(vecST, (int)pItem->GetData())) == FALSE)
			return FALSE;
	}

	return bRes;
}

BOOL CSampleTreeTabPageManager::isOkToClose()
{
	//check specie tabs if data changed

	for(int i=0; i<m_wndTabControl.GetItemCount(); i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.GetItem(i);
		if(pItem)
		{
			CSampleTreeView *pView = DYNAMIC_DOWNCAST(CSampleTreeView, CWnd::FromHandle(pItem->GetHandle()));
			if(pView)
			{
				if(pView->isOkToClose() == FALSE)
					return FALSE;
			}//if(pView)
		}//if(pItem)
	}//for(i)

	return TRUE;
}

void CSampleTreeTabPageManager::resetDataChanged()
{
	//reset m_bDataNotChanged for all specie tabs

	for(int i=0; i<m_wndTabControl.GetItemCount(); i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.GetItem(i);
		if(pItem)
		{
			CSampleTreeView *pView = DYNAMIC_DOWNCAST(CSampleTreeView, CWnd::FromHandle(pItem->GetHandle()));
			if(pView)
			{
				pView->resetDataChanged();
			}//if(pView)
		}//if(pItem)
	}//for(i)
}

void CSampleTreeTabPageManager::addTree(CMyTransactionSampleTree *tree)
{
	int nNum;
	CString csTitle = _T(""), csOldTitle = _T("");
	for(int i=0; i<m_wndTabControl.GetItemCount(); i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.GetItem(i);
		if(pItem)
		{
			if(pItem->GetData() == tree->getSpcId())
			{
				CSampleTreeView *pView = DYNAMIC_DOWNCAST(CSampleTreeView, CWnd::FromHandle(pItem->GetHandle()));
				if(pView)
				{
					pView->addTree(tree);
					nNum = pView->getNumOfTrees();
					csOldTitle = pItem->GetTooltip();
							if(nNum > 0)
								csTitle.Format(_T("%s (%d)"), csOldTitle, nNum);
							else
								csTitle.Format(_T("%s"), csOldTitle);
							pItem->SetCaption(csTitle);
				}//if(pView)
			}
		}//if(pItem)
	}//for(i)
}
