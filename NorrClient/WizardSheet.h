#pragma once

#include "ValjMall.h"
#include "Indata.h"
#include "Resultat.h"
#include "Header.h"
#include "HXLParser.h"

// CWizardSheet

class CWizardSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CWizardSheet)

public:
	CWizardSheet(UINT nIDCaption, int nStandId = -1, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CWizardSheet(LPCTSTR pszCaption, int nStandId = -1, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

/*	CWizard97Sheet(UINT nIDCaption, CWnd* pParentWnd = NULL,
			UINT iSelectPage = 0, HBITMAP hbmWatermark = NULL,
			HPALETTE hpalWatermark = NULL, HBITMAP hbmHeader = NULL);
	CWizard97Sheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL,
			UINT iSelectPage = 0, HBITMAP hbmWatermark = NULL,
			HPALETTE hpalWatermark = NULL, HBITMAP hbmHeader = NULL);*/


	virtual ~CWizardSheet();
	virtual BOOL OnInitDialog();

	CValjMall pageValjMall;
	CIndata pageIndata;
	CResultat pageResultat;
	CHeader pageHeader;

	
	CArray<HXLHEADER, HXLHEADER> m_csaHxlFiles;	// array som inneh�ller alla inventeringsfiler som ska l�sas in
	int m_nBestandId;	// id f�r det skapade best�ndet
	CString m_csTemplateXml;	// best�ndsmall att anv�nda
	bool m_bKomplettera; // om komplettering

	int m_nAltitude;	// h�jd �ver havet fr�n template
	int m_nLatitude;	// breddgrad fr�n template
	CString m_csSI;		// st�ndortsindex fr�n template

protected:
	DECLARE_MESSAGE_MAP()
	LRESULT OnTimer(WPARAM wParam, LPARAM lParam);
	void OnClose();
public:
	afx_msg void OnDestroy();
};
