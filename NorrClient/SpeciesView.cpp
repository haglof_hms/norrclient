// SpeciesView.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "SpeciesView.h"
#include "SpeciesViewReportRec.h"
#include "TabPageManager.h"
#include "StandTableDlg.h"
#include "NorrClient.h"
#include <math.h>

// CSpeciesView

IMPLEMENT_DYNCREATE(CSpeciesView, CXTResizeFormView)

CSpeciesView::CSpeciesView()
	: CXTResizeFormView(CSpeciesView::IDD)
{
	m_bPopulated = FALSE;
	m_bDataNotChanged = TRUE;
}

CSpeciesView::~CSpeciesView()
{
}

void CSpeciesView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSpeciesView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_MOUSEACTIVATE()
	ON_NOTIFY(NM_CLICK, IDC_REPORT, OnClickAddDcls)
	ON_NOTIFY(NM_KEYDOWN, IDC_REPORT, OnKeyDownAddDcls)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_REPORT, OnValueChanged)
END_MESSAGE_MAP()


// CSpeciesView diagnostics

#ifdef _DEBUG
void CSpeciesView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CSpeciesView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CSpeciesView message handlers

BOOL CSpeciesView::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

//	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
//	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CSpeciesView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	if(m_wndReport.GetSafeHwnd() == NULL)
	{
		//create sheet
		if(!m_wndReport.Create(WS_CHILD | WS_TABSTOP | WS_VISIBLE | WM_VSCROLL|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, IDC_REPORT))
		{
			return -1;
		}
	}

	return 0;
}

void CSpeciesView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);
	if(m_wndReport.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndReport,1,1,rect.right-1,rect.bottom-1);
	}
}

int CSpeciesView::OnMouseActivate(CWnd *pDesktopWnd, UINT nHitTest, UINT message)
{
	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

void CSpeciesView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	// remove scrollbars
	SetScaleToFitSize(CSize(1,1));

	CXTPReportColumn *pCol = NULL;

	if(m_wndReport.GetSafeHwnd() != NULL)
	{
		m_wndReport.ShowWindow(SW_NORMAL);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(0, theApp.m_XML->str(140), 100, FALSE));
		pCol->SetEditable(FALSE);
		pCol->SetHeaderAlignment(DT_CENTER);
		pCol->SetAlignment(DT_CENTER);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(1, theApp.m_XML->str(141), 100, FALSE));
		pCol->SetEditable(FALSE);
		pCol->SetHeaderAlignment(DT_CENTER);
		pCol->SetAlignment(DT_CENTER);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(2, theApp.m_XML->str(142), 100, FALSE));
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER | ES_CENTER;	//only allow numbers
		pCol->SetEditable(TRUE);
		pCol->SetHeaderAlignment(DT_CENTER);
		pCol->SetAlignment(DT_CENTER);


		m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
		m_wndReport.GetReportHeader()->AllowColumnReorder(FALSE);
		m_wndReport.GetReportHeader()->AllowColumnResize(FALSE);
		m_wndReport.GetReportHeader()->SetAutoColumnSizing(FALSE);
		m_wndReport.SetMultipleSelection(FALSE);
		m_wndReport.SetGridStyle(TRUE, xtpReportGridSolid);
		m_wndReport.SetGridStyle(FALSE, xtpReportGridSolid);	
		m_wndReport.FocusSubItems(TRUE);
		m_wndReport.AllowEdit(TRUE);
		m_wndReport.GetReportHeader()->AllowColumnSort(FALSE);	//set TRUE if sortable

		COLORREF clr = m_wndReport.GetPaintManager()->m_clrHeaderControl.GetStandardColor();
		m_wndReport.GetPaintManager()->m_clrHighlight.SetCustomValue(clr);
		m_wndReport.GetPaintManager()->m_clrHighlightText.SetCustomValue(BLACK);

		RECT rect;
		GetClientRect(&rect);
		setResize(&m_wndReport,1,1,rect.right-1,rect.bottom-1);

		UpdateWindow();
		//AfxMessageBox(_T("ReportControl initialized!"));

	}//if(m_wndReport.GetSafeHwnd() != NULL)

	SetResize(IDC_REPORT, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);
}

void CSpeciesView::setupDclsTable(double dcls)
{
	double fStart = 0.0, fEnd = 60.0, fDcls = dcls /*(double)(dcls/10.0)*/, fNext;

	int nNumOfRows;

	if(fDcls > 0.0)
	{
		nNumOfRows = (int)ceil((fEnd - fStart)/fDcls);
		fNext = fStart + fDcls;

		if(m_bPopulated == FALSE)
		{
			m_wndReport.BeginUpdate();

			for(int i=0; i<nNumOfRows; i++)
			{
				m_wndReport.AddRecord(new CSpeciesViewReportRec(fStart, fNext, 0.0));

				fStart = fNext;
				fNext += fDcls;
			}

			//add row for insertion of dcls
			addInsertRow();

			//set not focusable for columns 0 and 1
			setColumnsFocusState();

			//set focus on column 2
			m_wndReport.SetFocusedColumn(m_wndReport.GetColumns()->GetAt(DCLS_COL_NUM_OF));

			m_wndReport.Populate();
			m_wndReport.UpdateWindow();

			m_wndReport.EndUpdate();
			m_bPopulated = TRUE;
		}
	}
}

void CSpeciesView::addInsertRow()
{
	m_wndReport.AddRecord(new CSpeciesViewReportRec(m_csAddStr));

	m_wndReport.Populate();

	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	CXTPReportRecord* pRec = pRecs->GetAt(pRecs->GetCount()-1);
	CXTPReportColumn *pCol = m_wndReport.GetColumns()->GetAt(DCLS_COL_NUM_OF);

	//set not editable on last row column 2
	pRec->GetItem(pCol)->SetEditable(FALSE);

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}


void CSpeciesView::setColumnsFocusState()
{
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	CXTPReportColumn *pColStart = m_wndReport.GetColumns()->GetAt(DCLS_COL_START);
	CXTPReportColumn *pColEnd = m_wndReport.GetColumns()->GetAt(DCLS_COL_END);
	
	//set not focusable on columns 0 and 1 for all records
	for(int i=0; i<pRecs->GetCount(); i++)
	{
		CXTPReportRecord* pRec = pRecs->GetAt(i);
		if(pRec)
		{
			pRec->GetItem(pColStart)->SetFocusable(FALSE);
			pRec->GetItem(pColEnd)->SetFocusable(FALSE);
		}//if(pRec)
	}//for(i)

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}


void CSpeciesView::deleteFromStandTable()
{
	BOOL bChanged = FALSE;

	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	if(pRecs)
	{
		if(AfxMessageBox(m_csDelMsg, MB_YESNO|MB_ICONEXCLAMATION) == IDYES)
		{
			for(int i=0; i < pRecs->GetCount()-1; i++)	//don't set last row because text not int
			{
				CSpeciesViewReportRec *pView = (CSpeciesViewReportRec*)pRecs->GetAt(i);
				if(pView)
				{
					if(pView->getColumnFloat(2) != 0.0)
					{
						pView->setColumnFloat(2, 0.0);
						bChanged = TRUE;
					}
				}//if(pView)
			}//for
		}//if(AfxMessageBox(_T(csDelMsg,MB_YESNO) == IDYES)
	}//if(pRecs)

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();

	//data changed
	if(bChanged == TRUE)
		setDataChanged();

	//count number of trees and show in tab
	int nNum = getNumOfTrees();
	CString csTitle = _T(""), csOldTitle = _T("");
	int nCurrent;
}

int CSpeciesView::getNumOfTrees()
{
	int nTot = 0;
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	if(pRecs)
	{
		for(int i=0; i<pRecs->GetCount() - 1; i++)	//don't count last row because text not int
		{
			CSpeciesViewReportRec *pView = (CSpeciesViewReportRec*)pRecs->GetAt(i);
			if(pView)
			{
				if(pView->getColumnFloat(2) > 0.0)
				{
					nTot += (int)pView->getColumnFloat(2);
				}
			}//if(pView)
		}//for(int i=0; i<pRecs->GetCount(); i++)
	}//if(pRecs)

	return nTot;
}

void CSpeciesView::updateRecord()
{
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}

void CSpeciesView::OnClickAddDcls(NMHDR *pNMHDR, LRESULT *pResult)
{
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	CXTPReportColumn *pCol = m_wndReport.GetFocusedColumn();

	if(!pRow || !pCol)
		return;

	CXTPReportColumn *pColStart = m_wndReport.GetColumns()->GetAt(DCLS_COL_START);
	CXTPReportColumn *pColEnd = m_wndReport.GetColumns()->GetAt(DCLS_COL_END);
	
	int nRowIndex = pRow->GetIndex();
	int nColIndex = pCol->GetIndex();
	double fStart = 0, fNext = 0, fPrev = 0;

	// check if on last row and last column 
	if((nRowIndex == pRecs->GetCount() - 1) && (nColIndex == DCLS_COL_NUM_OF))
	{
		if(nRowIndex - 1 >= 0)
		{
			CSpeciesViewReportRec *pRec = (CSpeciesViewReportRec*)pRecs->GetAt(nRowIndex - 1);

			fPrev = pRec->getColumnFloat(0);
			fStart = pRec->getColumnFloat(1);
			fNext = fStart + (fStart - fPrev);

			//insert row
			pRecs->InsertAt(nRowIndex,new CSpeciesViewReportRec(fStart, fNext, 0));

			//set focus status
			CXTPReportRecord *pRecord = pRecs->GetAt(nRowIndex);
			pRecord->GetItem(pColStart)->SetFocusable(FALSE);
			pRecord->GetItem(pColEnd)->SetFocusable(FALSE);

			m_wndReport.Populate();
			m_wndReport.UpdateWindow();
		}//if(nRowIndex - 1 >= 0)
	}//if((nRowIndex == pRecs->GetCount() - 1) && (nColIndex == DCLS_COL_NUM_OF))
}

void CSpeciesView::OnKeyDownAddDcls(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNMHDR;
		
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	CXTPReportColumn *pCol = m_wndReport.GetFocusedColumn();

	if(!pRow || !pCol)
		return;

	CXTPReportColumn *pColStart = m_wndReport.GetColumns()->GetAt(DCLS_COL_START);
	CXTPReportColumn *pColEnd = m_wndReport.GetColumns()->GetAt(DCLS_COL_END);

	int nRowIndex = pRow->GetIndex();
	int nColIndex = pCol->GetIndex();
	double fStart = 0, fNext = 0, fPrev = 0;

	// check if on last row and last column 
	if((nRowIndex == pRecs->GetCount() - 1) && (nColIndex == DCLS_COL_NUM_OF))
	{
		if(lpNMKey->nVKey == VK_SPACE)	//doesn't work for VK_RETURN
		{
			if(nRowIndex - 1 >= 0)
			{
				CSpeciesViewReportRec *pRec = (CSpeciesViewReportRec*)pRecs->GetAt(nRowIndex - 1);

				fPrev = pRec->getColumnFloat(0);
				fStart = pRec->getColumnFloat(1);
				fNext = fStart + (fStart - fPrev);

				//insert row
				pRecs->InsertAt(nRowIndex,new CSpeciesViewReportRec(fStart, fNext, 0));

				//set focus status
				CXTPReportRecord *pRecord = pRecs->GetAt(nRowIndex);
				pRecord->GetItem(pColStart)->SetFocusable(FALSE);
				pRecord->GetItem(pColEnd)->SetFocusable(FALSE);

				m_wndReport.Populate();
				m_wndReport.UpdateWindow();
			}//if(nRowIndex - 1 >= 0)
		}//if(lpNMKey->nVKey == VK_RETURN || lpNMKey->nVKey == VK_SPACE)
	}//if((nRowIndex == pRecs->GetCount() - 1) && (nColIndex == DCLS_COL_NUM_OF))
}

void CSpeciesView::OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	//check that value < max_value, max_value = 32000
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	int index;
	
	if(!pRecs || !pRow)
		return;

	if(pRecs)
	{
		index = pRow->GetIndex();

		if(index < pRecs->GetCount()-1)	//don't check last row because text not int
		{
			CSpeciesViewReportRec *pView = (CSpeciesViewReportRec*)pRecs->GetAt(index);
			if(pView)
			{
				if(pView->getColumnFloat(2) > 32000.0)
				{
					AfxMessageBox(m_csMaxValue);
					pView->setColumnFloat(2, 0.0);
					
					m_wndReport.Populate();
					m_wndReport.UpdateWindow();
				}
				else
				{
					//data changed
					setDataChanged();
				}
			}//if(pView)
		}//if
	}//if(pRecs)

	
	//count number of trees and show in tab
	int nNum = getNumOfTrees();
	CString csTitle = _T(""), csOldTitle = _T("");
	int nCurrent;
}

void CSpeciesView::removeAllItems()
{
//	m_wndReport.BeginUpdate();
//	m_wndReport.GetRecords()->RemoveAll();
	m_wndReport.ResetContent();
//	m_wndReport.Populate();
//	m_wndReport.UpdateWindow();
//	m_wndReport.EndUpdate();

	m_bPopulated = FALSE;
}
