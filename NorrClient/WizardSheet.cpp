// WizardSheet.cpp : implementation file
//

#include "stdafx.h"
#include "NorrClient.h"
#include "WizardSheet.h"
#include "RpcProcedures.h"


// CWizardSheet

IMPLEMENT_DYNAMIC(CWizardSheet, CPropertySheet)

CWizardSheet::CWizardSheet(UINT nIDCaption, int nStandId, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	if( nStandId < 0 )
	{
		AddPage(&pageValjMall);
		m_bKomplettera = false;
	}
	else
	{
		m_bKomplettera = true;
	}
	AddPage(&pageIndata);
	AddPage(&pageHeader);
	AddPage(&pageResultat);

	SetWizardMode();

	// initialize variables
	m_nBestandId = nStandId;
	m_nAltitude = 0;
	m_nLatitude = 0;
	m_csSI = _T("");
}

CWizardSheet::CWizardSheet(LPCTSTR pszCaption, int nStandId, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	if( nStandId < 0 )
	{
		AddPage(&pageValjMall);
		m_bKomplettera = false;
	}
	else
	{
		m_bKomplettera = true;
	}
	AddPage(&pageIndata);
	AddPage(&pageHeader);
	AddPage(&pageResultat);

	SetWizardMode();

	// initialize variables
	m_nBestandId = nStandId;
	m_nAltitude = 0;
	m_nLatitude = 0;
	m_csSI = _T("");
}

CWizardSheet::~CWizardSheet()
{
}


BEGIN_MESSAGE_MAP(CWizardSheet, CPropertySheet)
	ON_MESSAGE(WM_TIMER, OnTimer)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CWizardSheet message handlers

BOOL CWizardSheet::OnInitDialog()
{
	// set a license-poker-timer on 10 minutes
	SetTimer(1, 10*60*1000, 0);

	return CPropertySheet::OnInitDialog();
}

void CWizardSheet::OnClose()
{
	CPropertySheet::OnClose();
}

LRESULT CWizardSheet::OnTimer(WPARAM wParam, LPARAM lParam)
{
	// do we have a valid license?
	if(!_CheckLicense())
	{
		// no valid license
		AfxMessageBox( GetLastMessage() );
	}

	return 0;
}

void CWizardSheet::OnDestroy()
{
	KillTimer(1);

	CPropertySheet::OnDestroy();
}
