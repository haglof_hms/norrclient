// TabPageManager.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "TabPageManager.h"
#include "SpeciesView.h"


// CTabPageManager

IMPLEMENT_DYNCREATE(CTabPageManager, CXTResizeFormView)

CTabPageManager::CTabPageManager()
	: CXTResizeFormView(CTabPageManager::IDD)
{
	//AfxMessageBox(_T("CTabPageManager::CTabPageManager()"));
}

CTabPageManager::~CTabPageManager()
{
	//AfxMessageBox(_T("CTabPageManager::~CTabPageManager()"));
}

void CTabPageManager::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTabPageManager, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CTabPageManager diagnostics

#ifdef _DEBUG
void CTabPageManager::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CTabPageManager::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CTabPageManager message handlers


BOOL CTabPageManager::PreCreateWindow(CREATESTRUCT& cs)
{
	
	if(!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CTabPageManager::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
	{
		//AfxMessageBox(_T("CXTResizeFormView::OnCreate not ok!"));
		return -1;
	}

	/*LOGFONT lf;
	m_fntTab.CreateFont(
		24,                        // nHeight
		0,                         // nWidth
		0,                         // nEscapement
		0,                         // nOrientation
		FW_NORMAL,                 // nWeight
		FALSE,                     // bItalic
		FALSE,                     // bUnderline
		0,                         // cStrikeOut
		ANSI_CHARSET,              // nCharSet
		OUT_DEFAULT_PRECIS,        // nOutPrecision
		CLIP_DEFAULT_PRECIS,       // nClipPrecision
		DEFAULT_QUALITY,           // nQuality
		DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
		_T("Times New Roman"));                 // lpszFacename

	m_fntTab.GetLogFont(&lf);*/

//	m_wndTabControl	= new CXTPTabControl;

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP, CRect(0, 0, 0, 0), this, IDC_TABCONTROL_1);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearanceVisualStudio2005);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = TRUE;
//	m_wndTabControl.GetPaintManager()->SetFontIndirect( &lf );
	m_wndTabControl.GetPaintManager()->DisableLunaColors( FALSE );
	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

	return 0;
}

void CTabPageManager::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	// remove scrollbars
	SetScaleToFitSize(CSize(1,1));
}

void CTabPageManager::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);
	if(m_wndTabControl.GetSafeHwnd())
	{
		setResize(&m_wndTabControl,1,1,rect.right-1,rect.bottom-1);
	}
}

void CTabPageManager::OnDraw(CDC* /*pDC*/)
{
}

BOOL CTabPageManager::AddView(CRuntimeClass *pViewClass, LPCTSTR lpszTitle, int spc_id, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pNewViewClass = pViewClass;

	CWnd* pWnd;
	TRY
	{
		if(pViewClass->IsDerivedFrom(RUNTIME_CLASS(CSpeciesView)))
		{
			pWnd = (CWnd*)pViewClass->CreateObject();
			if(pWnd == NULL)
			{
				DWORD dError = GetLastError();
				AfxThrowMemoryException();
			}
		}
	}
	CATCH_ALL(e)
	{
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();
	

	CRect rect(0,0,0,0);
	if(!(pWnd)->Create(NULL, NULL, dwStyle, rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
		return FALSE;

	CXTPTabManagerItem *pItem = m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	pItem->SetData(spc_id);
	pItem->SetTooltip(lpszTitle);
	
	pWnd->SetOwner(this);
	pWnd->SendMessage(WM_INITIALUPDATE);
	
	return TRUE;
}

void CTabPageManager::setupDclsTables(double dcls)
{
	int nTabs = m_wndTabControl.GetItemCount();
	for(int i=0; i<nTabs; i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.GetItem(i);
		if(pItem)
		{
			CSpeciesView* pSpecView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pItem->GetHandle()));
			if(pSpecView)
			{
				pSpecView->setupDclsTable(dcls);
			}//if(pSpecView)
		}//if(pItem)
	}//for(int i=0; i<nTabs; i++)
}

void CTabPageManager::deleteFromStandTable()
{
	CXTPTabManagerItem *pItem = m_wndTabControl.GetItem( m_wndTabControl.GetCurSel() );
	if(pItem)
	{
		CSpeciesView* pSpecView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pItem->GetHandle()));
		if(pSpecView)
		{
			pSpecView->deleteFromStandTable();
		}//if(pSpecView)
	}//if(pItem)
}


BOOL CTabPageManager::isOkToClose()
{
	for(int i=0; i<m_wndTabControl.GetItemCount(); i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.GetItem(i);
		if(pItem)
		{
			CSpeciesView *pView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pItem->GetHandle()));
			if(pView)
			{
				if(pView->isOkToClose() == FALSE)
					return FALSE;
			}//if(pView)
		}//if(pItem)
	}//for(i)

	return TRUE;
}

void CTabPageManager::resetDataChanged()
{
	for(int i=0; i<m_wndTabControl.GetItemCount(); i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.GetItem(i);
		if(pItem)
		{
			CSpeciesView *pView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pItem->GetHandle()));
			if(pView)
			{
				pView->resetDataChanged();
			}//if(pView)
		}//if(pItem)
	}//for(i)
}
