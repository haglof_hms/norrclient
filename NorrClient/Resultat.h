#pragma once
#include "afxwin.h"
#include "MyTabControl.h"


// CResultat dialog

class CResultat : public CPropertyPage
{
	DECLARE_DYNAMIC(CResultat)

public:
	CResultat();
	virtual ~CResultat();
	virtual BOOL OnSetActive();

// Dialog Data
	enum { IDD = IDD_WIZARD_RESULTAT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual LRESULT OnWizardNext();
	virtual LRESULT OnWizardBack();
	virtual int OnWizardFinish();
	virtual BOOL OnQueryCancel();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
	CMyExtEdit m_text;
};
