#pragma once

#include "tinyxml.h"

typedef struct _tagHXLHEADER
{
	// read/write variabler
	CString csBestand;
	int nDelbestand;
	int nBestAlder;
	int nAreal;
	int nLatitud;
	int nAltitud;
	int nInventeringsmetod;	// 1 = Provytetax, 2 = Totaltax/Provtr�d, 3 = Snabbtaxering, 5 = Relaskop, 6 = V�rdering

	// read only variabler
	CString csFilePath;
	CString csDatum;
	CString csSI;
	int nNumPlots;
	int nNumTrees;
	int nNumSampleTrees;
} HXLHEADER;

typedef struct _tagHXLSPECIES
{
	int nIndex;
	CString csName;
} HXLSPECIES;

typedef struct _tagHXLPLOT
{
	CString csName;
	int nAreal;
	int nOHgt;
	int nDBHAge;
	int nBonitet;
	int nSiTree;
	int nSI;
	int nAntal;
} HXLPLOT;

typedef struct _tagHXLTREE
{
	int nSpecNr;
	int nDBH;
	int nHgtNum;
	int nHgt;
	int nHgt2;
	int nAge;
	int nType;
	int nBonitet;
	int nPlot;
} HXLTREE;

typedef struct _tagHXLDATA
{
	CString csName;
	int nVar;
	int nType;
	CString csDef;
} HXLDATA;


class CHXLParser
{
	TiXmlDocument m_doc;	// XML document
	TiXmlPrinter *m_pPrinter;	// XML printer object

	int InsertTag(TiXmlNode* /*pNode*/, char* /*pszTag*/, TiXmlAttributeSet* /*TiXmlAttribute*[]*/ /*pAttributes*/, char* /*pszValue*/);
	int getHxlAttributes(TiXmlElement* /*pElement*/, HXLDATA* /*pHxlData*/);
	int setHxlAttributes(TiXmlElement* /*pElement*/, CStringA /*csName*/, CStringA /*csVar*/, CStringA /*csType*/);

	CString m_csFilePath;

	CString GetNodeValue(CStringA /*csNode*/);
	int SetNodeValue(CStringA /*csNode*/, CStringA /*csName*/, CStringA /*csVar*/, CStringA /*csType*/, int /*nValue*/);
	int SetNodeValue(CStringA /*csNode*/, CStringA /*csName*/, CStringA /*csVar*/, CStringA /*csType*/, LPCTSTR /*lpszValue*/);

	int ReplaceChars(CStringA *pcsBuf, CStringA csTag, char /*szSearch*/, char /*szReplace*/);

public:
	CHXLParser();
	~CHXLParser();

	int Open(CString /*csFilename*/);
	int Save(CString /*csFilename*/);

	int getHeaderData(HXLHEADER* /*pHeader*/);
	int getHeaderData(CString /*csFilename*/, HXLHEADER* /*pHeader*/);

	int setHeaderData(HXLHEADER* /*pHeader*/);
	int setHeaderData(CString /*csFilename*/, HXLHEADER* /*pHeader*/);

	CString getBestand();
	int setBestand(CString csValue);

	int getAreal();
	int setAreal(int /*nValue*/);

	int getLatitud();
	int setLatitud(int /*nValue*/);

	int getInventeringsmetod();
	int setInventeringsmetod(int /*nValue*/);

	CString getSI();
	int setSI(CString /*csValue*/);

	CString getDatum();
	int setDatum(CString /*csValue*/);

	int getAltitud();
	int setAltitud(int /*nValue*/);

	int getBestAlder();
	int setBestAlder(int /*nValue*/);

	int getNumPlots();

	int getPlots(std::vector< PLOT > *pvecPlots);	// load plots from HXL file
	int setPlots(std::vector< PLOT > *pvecPlots);	// save plots to HXL file
	int setSpecies(vecTransactionSpecies *m_vecSpecies);	// save species to HXL file

	int setTrees(std::vector< PLOT > *pvecPlots);	// save trees to HXL file
};
