#include "StdAfx.h"
#include "PricelistParser.h"

CPricelistParser::CPricelistParser()
{
}

CPricelistParser::~CPricelistParser()
{
	m_doc.Clear();
}

BOOL CPricelistParser::ParseXml(LPCSTR xml)
{
	QDESC qd;
	TiXmlNode *pRoot;
	TiXmlElement *pElem, *pChild;

	m_qlst.RemoveAll();
	qd.id = -1;

	// Parse pricelist
	m_doc.Clear();
	m_doc.Parse(xml, NULL, TIXML_ENCODING_LEGACY); // Legacy encoding, this make sure ��� won't break the parsing

	if( !(pRoot = m_doc.FirstChild("Pricelist")) )
		return FALSE;

	// Species
	pElem = pRoot->FirstChildElement("Specie");
	while( pElem )
	{
		qd.id = -1; // Reset id
		pElem->Attribute("id", &qd.spcId);

		// Quality descriptions per specie
		pChild = pElem->FirstChildElement("QualityDesc");
		while( pChild )
		{
			qd.name = pChild->Attribute("name");
			qd.id++; // Increase id
			m_qlst.AddTail(qd);

			pChild = pChild->NextSiblingElement("QualityDesc");
		}

		pElem = pElem->NextSiblingElement("Specie");
	}

	return TRUE;
}
