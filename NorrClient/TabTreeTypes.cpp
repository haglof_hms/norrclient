// TabPageManager.cpp : implementation file
//

#include "stdafx.h"
#include "NorrClient.h"
#include "resource.h"
#include "TabTreeTypes.h"
#include "TabPageManager.h"
#include "SampleTreeTabPageManager.h"
#include "SelectSpeciesDlg.h"
#include "SpeciesView.h"
#include "SampleTreeView.h"
#include "SpeciesViewReportRec.h"


// CTabTreeTypes

IMPLEMENT_DYNCREATE(CTabTreeTypes, CXTResizeFormView)

CTabTreeTypes::CTabTreeTypes()
	: CXTResizeFormView(CTabTreeTypes::IDD)
{
		//AfxMessageBox(_T("CTabTreeTypes::CTabTreeTypes()"));
}

CTabTreeTypes::~CTabTreeTypes()
{
	//AfxMessageBox(_T("CTabTreeTypes::~CTabTreeTypes()"));
}

void CTabTreeTypes::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTabTreeTypes, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CTabTreeTypes diagnostics

#ifdef _DEBUG
void CTabTreeTypes::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CTabTreeTypes::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CTabTreeTypes message handlers


BOOL CTabTreeTypes::PreCreateWindow(CREATESTRUCT& cs)
{
	
	if(!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CTabTreeTypes::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
	{
		//AfxMessageBox(_T("CXTResizeFormView::OnCreate not ok!"));
		return -1;
	}

/*	LOGFONT lf;
	m_fntTab.CreateFont(
		24,                        // nHeight
		0,                         // nWidth
		0,                         // nEscapement
		0,                         // nOrientation
		FW_NORMAL,                 // nWeight
		FALSE,                     // bItalic
		FALSE,                     // bUnderline
		0,                         // cStrikeOut
		ANSI_CHARSET,              // nCharSet
		OUT_DEFAULT_PRECIS,        // nOutPrecision
		CLIP_DEFAULT_PRECIS,       // nClipPrecision
		DEFAULT_QUALITY,           // nQuality
		DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
		_T("Times New Roman"));                 // lpszFacename

	m_fntTab.GetLogFont(&lf);
*/
	//tab control
	//m_wndTabControl = new CXTPTabControl;
	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP, CRect(0, 0, 0, 0), this, IDC_TABCONTROL_1);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
//	m_wndTabControl.GetPaintManager()->m_bBoldSelected = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);
//	m_wndTabControl.GetPaintManager()->SetFontIndirect( &lf );
	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS,NULL,0,CSize(16, 16),xtpImageNormal);
		
	//create tabpages
	AddView(RUNTIME_CLASS(CTabPageManager), theApp.m_XML->str(126), 3);	//cut
	AddView(RUNTIME_CLASS(CTabPageManager), theApp.m_XML->str(127), 3);	//standing
	AddView(RUNTIME_CLASS(CSampleTreeTabPageManager), theApp.m_XML->str(169), 3);	//sample tree, rand tree

	return 0;
}

void CTabTreeTypes::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	// remove scrollbars
	SetScaleToFitSize(CSize(1,1));
}

void CTabTreeTypes::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);
	if(m_wndTabControl.GetSafeHwnd())
	{
		setResize(&m_wndTabControl,1,1,rect.right-1,rect.bottom-1);
	}
}

void CTabTreeTypes::OnDraw(CDC* /*pDC*/)
{
}

BOOL CTabTreeTypes::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pNewViewClass = pViewClass;

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle, rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		// pWnd will be cleaned up by PostNcDestroy
		AfxMessageBox(_T("Failed to create Window!"));
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	pWnd->SetOwner(this);
	pWnd->SendMessage(WM_INITIALUPDATE);

	return TRUE;
}


void CTabTreeTypes::setupDclsTables(double dcls)
{
	int nTabs = m_wndTabControl.GetItemCount();
	for(int i=0; i<nTabs; i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.GetItem(i);
		if(pItem)
		{
			CSpeciesView* pSpecView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pItem->GetHandle()));
			if(pSpecView)
			{
				pSpecView->setupDclsTable(dcls);
			}//if(pSpecView)
		}//if(pItem)
	}//for(int i=0; i<nTabs; i++)
}

void CTabTreeTypes::deleteFromStandTable()
{
/*	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	if(pItem)
	{
		CSpeciesView* pSpecView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pItem->GetHandle()));
		if(pSpecView)
		{
			pSpecView->deleteFromStandTable();
		}//if(pSpecView)
	}//if(pItem)
	*/
}


BOOL CTabTreeTypes::isOkToClose()
{
	for(int i=0; i<m_wndTabControl.GetItemCount(); i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.GetItem(i);
		if(pItem)
		{
			CSpeciesView *pView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pItem->GetHandle()));
			if(pView)
			{
				if(pView->isOkToClose() == FALSE)
					return FALSE;
			}//if(pView)
		}//if(pItem)
	}//for(i)

	return TRUE;
}

void CTabTreeTypes::resetDataChanged()
{
	for(int i=0; i<m_wndTabControl.GetItemCount(); i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.GetItem(i);
		if(pItem)
		{
			CSpeciesView *pView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pItem->GetHandle()));
			if(pView)
			{
				pView->resetDataChanged();
			}//if(pView)
		}//if(pItem)
	}//for(i)
}
