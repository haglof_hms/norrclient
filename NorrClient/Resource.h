//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by NorrClient.rc
//
#define IDD_STANDTABLE                  2000
#define IDD_SPECIESVIEW                 2001
#define IDC_PLACEHOLDER                 2001
#define IDC_BUTTON_SPECIES_ADD          2002
#define IDD_SAMPLETREEVIEW              2003
#define IDC_BUTTON_SPECIES_DEL          2003
#define IDC_BUTTON_REMOVE               2003
#define IDD_NUMOFCOPIES                 2004
#define IDC_BUTTON_SAVE                 2004
#define IDC_BUTTON_EDIT                 2004
#define IDD_SELECTSPECIES               2005
#define IDC_BUTTON_CANCEL               2005
#define IDD_TABPAGEMANAGER              2006
#define IDC_BUTTON_LOAD                 2006
#define IDC_COMBO_INVMETOD              2007
#define IDC_LBL_INVMETOD                2008
#define IDC_BUTTON_NEW                  2008
#define IDD_WIZARD_VALJ_MALL            2009
#define IDC_LIST_FILES                  2009
#define IDD_WIZARD_INDATA               2010
#define IDC_LIST_DESC                   2010
#define IDD_WIZARD_RESULTAT             2011
#define IDC_DATE                        2011
#define IDD_WIZARD_HEADER               2012
#define IDC_STATIC_TOP                  2012
#define IDC_STATIC_INDATAFILER          2013
#define IDD_TABPAGEMANAGER1             2013
#define IDD_BYTMALL                     2013
#define IDD_BYT_MALL                    2013
#define IDC_STATIC_BESKRIVNING          2014
#define IDC_STATIC_BESTAND              2016
#define IDC_STATIC_DELBESTAND           2017
#define IDC_STATIC_DATUM                2018
#define IDC_STATIC_ALTITUD              2019
#define IDC_STATIC_AREAL                2020
#define IDC_STATIC_ALDER                2021
#define IDC_STATIC_BONITET              2022
#define IDC_STATIC_BREDDGRAD            2023
#define IDC_EDIT_ALTITUD                2024
#define IDC_EDIT_AREAL                  2025
#define IDC_EDIT_ALDER                  2026
#define IDC_EDIT_BONITET                2027
#define IDC_COMBO1                      2028
#define IDC_STATIC_BERAKNINGSMALL       2029
#define IDC_BUTTON_PLOT_ADD             2030
#define IDC_BUTTON2                     2031
#define IDC_BUTTON_PLOT_DEL             2031
#define IDD_TABTREETYPES                2032
#define IDC_LIST1                       2033
#define IDC_LIST_REPORT                 2033
#define IDC_LBL_YTAREAL                 2035
#define IDC_EDIT_YTAREAL                2036
#define IDC_BUTTON_ADD                  2038
#define IDC_BUTTON3                     2039
#define IDC_BUTTON_DEL                  2039
#define IDC_STATIC_KOMPLETTERA          2040
#define IDC_EDIT1                       14000
#define IDC_EDIT_BESTAND                14000
#define IDC_EDIT2                       14001
#define IDC_EDIT_DELBESTAND             14001
#define IDC_EDIT3                       14002
#define IDC_EDIT4                       14003
#define IDC_EDIT5                       14004
#define IDC_EDIT6                       14005
#define IDC_LBL1                        14006
#define IDC_EDIT11                      14006
#define IDC_EDIT_BREDDGRAD              14006
#define IDC_LBL2                        14007
#define IDC_LBL3                        14008
#define IDC_LBL4                        14009
#define IDC_LBL5                        14010
#define IDC_LBL6                        14011
#define IDC_GRP1                        14012
#define IDC_EDIT7                       14013
#define IDC_LBL7                        14014
#define IDC_EDIT8                       14015
#define IDC_LBL8                        14016
#define IDC_LBL9                        14017
#define IDC_LIST2                       14018
#define IDC_EDIT9                       14019
#define IDC_LBL_NUMOF                   14025
#define IDC_EDIT_NUMOF                  14026
#define IDC_LBL10                       14027
#define IDC_EDIT10                      14028
#define IDC_BTN_OPENPROP                14029
#define IDB_TAB_ICONS                   14030

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        2014
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         2041
#define _APS_NEXT_SYMED_VALUE           2000
#endif
#endif
