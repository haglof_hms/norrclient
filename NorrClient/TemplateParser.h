#pragma once

#include <tinyxml.h>

struct SPECIE
{
	int id;
	CString name;
	int hgtId, hgtSpcId, hgtIdx;
	int volId, volSpcId, volIdx;
	int barkId, barkSpcId, barkIdx;
	int ubId, ubSpcId, ubIdx;
	int qualityId;
	CString qualityName;
	int grotId;
	double grotPercent, grotPrice, grotCost;
	int dist1, dist2;
	double sk2ub;
	double sk2fub;
	double fub2to;
	double h25;
	int gcrown;
};

typedef CList<SPECIE, SPECIE&> SpecieList;

class CTemplateParser
{
public:
	CTemplateParser();
	virtual ~CTemplateParser();

	BOOL ParseXml(LPCSTR xml);
	BOOL UpdateXml(LPCSTR* pStr);

	SpecieList& GetSpecieList() { return m_species; }

	int GetPricelistId() { return m_nPrlId; }
	int GetCostTemplateId() { return m_nCostId; }
	double GetDcls() { return m_fDcls; }
	int GetAltitude() { return m_nAltitude; }
	int GetLatitude() { return m_nLatitude; }
	CString GetSI() { return m_csSI; }
	int GetAtCoast() { return m_nAtCoast; }
	int GetSouthEast() { return m_nSouthEast; }
	int GetRegion5() { return m_nRegion5; }
	int GetPartOfPlot() { return m_nPartOfPlot; }
	CString GetH100Pine() { return m_csH100Pine; }

protected:
	BOOL _ParseXml(LPCSTR xml = NULL, LPCSTR *out = NULL);

	SpecieList m_species;

	int m_nPrlId;
	int m_nCostId;
	double m_fDcls;
	int m_nAltitude;
	int m_nLatitude;
	CString m_csSI;
	int m_nAtCoast;
	int m_nSouthEast;
	int m_nRegion5;
	int m_nPartOfPlot;
	CString m_csH100Pine;

	TiXmlDocument m_doc;
	TiXmlPrinter *m_pPrinter;
};

double localeStrToDbl(LPCSTR value);
