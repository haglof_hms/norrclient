// NorrClient.h : main header file for the NorrClient DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "ResLangFileReader.h"

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

// CNorrClientApp
// See NorrClient.cpp for the implementation of this class
//

class CNorrClientApp : public CWinApp
{
	CString m_sLangFN;
public:
	CNorrClientApp();
	~CNorrClientApp();
	RLFReader *m_XML;

protected:
	RPC_WSTR m_szStringBinding;

// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

	DECLARE_MESSAGE_MAP()
};

extern CNorrClientApp theApp;